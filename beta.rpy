########################################################################################
#     This file is part of Castle.
#     Copyright (C) 2015  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# This files contains labels for dev_beta, besides credits.
# Please note 3/4 of the original file is now under map/01_linarius
# It also contain pre-definitions for some other bg resources (moved from defs.rpy)

image city l0 = Frame("gfx/bg/castle8.jpg",0,0)

image DKF Hallway = "#000"
image DKF Bedroom = "#000"

# TODO: Do we really use this?
label load_sentences:   # A label to load default sentences. Please note the _DO_NOT_REMOVE__FLAG. They're replaced later on.
    # FLAGS LIST:

    # ALV_DO_NOT_REMOVE__FLAG -> Current level flag
    # GLV_DO_NOT_REMOVE__FLAG -> Maximum level flag
    # PLN_DO_NOT_REMOVE__FLAG -> Player  name  flag
    # PLG_DO_NOT_REMOVE__FLAG -> Player gender flag
    # PHP_DO_NOT_REMOVE__FLAG -> Player HPs    flag
    # PMP_DO_NOT_REMOVE__FLAG -> Player MPs    flag

    $ HUNT_INTRO_SENTENCE=renpy.random.choice([
_("You are on level ALV_DO_NOT_REMOVE__FLAG and eager to hunt!"), 
_("You arrive at hunting camps of level ALV_DO_NOT_REMOVE__FLAG."), 
_("You arrive at level ALV_DO_NOT_REMOVE__FLAG hunting camp eager to hunt!"), 
_("You arrive at hunting camps of level ALV_DO_NOT_REMOVE__FLAG."), 
_("You arrive at level ALV_DO_NOT_REMOVE__FLAG hunting camp eager to hunt!"), 
_("Hungry for XP, items and credits, you arrive at level ALV_DO_NOT_REMOVE__FLAG hunting camps.")])

    $ SHOPS_INTRO_SENTENCE=renpy.random.choice([
_("Welcome to my shop!"),
_("Welcome to my humble shop, PLG_DO_NOT_REMOVE__FLAG"),
_("Hello PLG_DO_NOT_REMOVE__FLAG! What do you want to buy?"),
_("Hi there. Care to buy anything?")])

    return


label dev_beta__shops:

    #hide city with Dissolve(0.4)
    #show shop with Fade(0.0, 0.0, 0.4)

    menu:
        "In which store will you enter?"
        "Weapon Store":
            python:
                try:
                    renpy.jump("dev_beta__wstore_l" + str(g_cur_level))
                except:
                    show_money=True
                    renpy.call_in_new_context("load_sentences")
                    # Process gender (sir or madam)
                    if ymale:
                        SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", _("sir"))
                    else:
                        SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", _("madam"))
                    (player("[SHOPS_INTRO_SENTENCE!t]"))
                    renpy.jump("weapon_shop")
        "Armor Store":
            python:
                try:
                    renpy.jump("dev_beta__astore_l" + str(g_cur_level))
                except:
                    show_money=True
                    renpy.call_in_new_context("load_sentences")
                    # Process gender (sir or madam)
                    if ymale:
                        SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", _("sir"))
                    else:
                        SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", _("madam"))
                    (player("[SHOPS_INTRO_SENTENCE!t]"))
                    renpy.jump("armor_shop")
        "Food Store":
            python:
                try:
                    renpy.jump("dev_beta__hstore_l" + str(g_cur_level))
                except:
                    show_money=True
                    renpy.call_in_new_context("load_sentences")
                    # Process gender (sir or madam)
                    if ymale:
                        SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", _("sir"))
                    else:
                        SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", _("madam"))
                    (player("[SHOPS_INTRO_SENTENCE!t]"))
                    renpy.jump("healitem_shop")
        "Miscellaneous Stuff Store":
            python:
                try:
                    renpy.jump("dev_beta__wstore_l" + str(g_cur_level))
                except:
                    show_money=True
                    renpy.call_in_new_context("load_sentences")
                    # Process gender (sir or madam)
                    if ymale:
                        SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", _("sir"))
                    else:
                        SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", _("madam"))
                    (player("[SHOPS_INTRO_SENTENCE!t]"))
                    renpy.jump("generic_shop")
        "Sell items":
            jump dev_beta__sellitem
        "Exit":
            $ renpy.jump("dev_beta__available_quests_on_l" + str(g_cur_level))

    jump dev_beta

label dev_beta__sellmult(icxxtem, price=1):
    # VARIABLES: i=How_Many_Have price=Individual_Price icxxtem=Item_Variable cgdr=Item_Name cgdi=How_Many_Left outcome=How_Much_You_Got cgd=How_Many_Sell
    $i=0
    $ cgdr=icxxtem.name
    $ cgi=hero.items
    python:
        for item_gn in hero.items:
          if item_gn is icxxtem:
            i+=1
    $ cgd=renpy.input(_("How many [cgdr!t] you wish to sell? 0-[i]"), allow="0123456789") or "1"
    $ cgd = int(cgd)
    if cgd > i:
        "Invalid value."
        call dev_beta__sellmult(icxxtem, price)
        return
    $ outcome=price*cgd
    #$ print "Amount: " + str(cgd)
    #$ print "Price: " + str(price)
    #$ print "Total: " + str(outcome)
    $ renpy.block_rollback()
    $ gp+=outcome
    $ rm_mult_items(cgd, icxxtem)
    $ cgdi=i-cgd
    "Sold [cgd] [cgdr] at [price] Col each, for a total outcome of {b}[outcome]{/b} Col.\nYou now have: [cgdi] [cgdr]."

    return

label dev_beta__sellitem:
    $ show_money=True
    menu:
        "It's ok, but what do you want to sell?"
        "Nothing":
            $ show_money=False
            jump dev_beta__shops   # Return to city shops.
        "Sell Miscellaneous item":
            call dev_beta__sellmitem
        "Sell Weapons":
            call dev_beta__sellwequip
        "Sell Armor and Shields":
            call dev_beta__sellaoequip
    jump dev_beta__sellitem

label dev_beta__sellmitem:
    $ show_money=True
    "Bonjour! What do you want to sell?"
    $ sellable=[["Nothing, I'm done", "SIGABRT"]]
    $ sl_ctrl=[]
    python:
        for item in hero.items:
            if item.type=="misc":
                if not item.name in sl_ctrl:
                    sellable.append(["Sell " + item.name + " for " + str(item.sell) + " credits", item])
                    sl_ctrl.append(item.name)

    $_return=renpy.display_menu(sellable)

    if _return == "SIGABRT":
            $ show_money=False
            return
    else:
            call dev_beta__sellmult(_return, _return.sell)

    jump dev_beta__sellmitem


label dev_beta__buymult(icxxtem, price=1):
    # A lame copy from sellmult.
    $i=int(gp/price)
    $ cgdr=icxxtem.name
    $ cgi=hero.items
    $ cgd=renpy.input(_("How many [cgdr!t] you wish to buy? 0-[i]"), allow="0123456789") or "1"
    $ cgd = int(cgd)
    if cgd > i:
        "Invalid value."
        call dev_beta__buymult(icxxtem, price)
        return
    $ outcome=price*cgd
    #$ print "Amount: " + str(cgd)
    #$ print "Price: " + str(price)
    #$ print "Total: " + str(outcome)
    $ gp-=outcome
    $ add_mult_items(cgd, icxxtem)
    $ renpy.block_rollback()
    "Bought [cgd] [cgdr] at [price] Col each, for a total of {b}[outcome]{/b} Col.\nYou now have: [gp] Col."

    return









































image credits0 = Text(_("PARADIGM"), size=45, font="f/Imperator.ttf", text_align=0.5)
image credits1 = Text(_("Project Creator {p}         Jesusaves (jesusalva)"), size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits2 = Text(_("Coders {p}Jesusaves (jesusalva)"), size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits3 = Text("Story Writter {p}There's no Story", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits4 = Text(_("Fonts {p}All licensed under GPL"), size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits4a = Text(_("ShadowedBlack, by Freecol"), size=30, font="f/ShadowedBlack.ttf", text_align=0.5)
image credits4b = Text(_("Jura Regular, by Unknown"), size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits4c = Text(_("FreeMono, by Freefont"), size=30, font="f/FreeMono.ttf", text_align=0.5)
image credits4d = Text(_("unifont, by Unknown"), size=30, font="f/unifont.ttf", text_align=0.5)
image credits4e = Text(_("Imperator, by Unknown"), size=30, font="f/Imperator.ttf", text_align=0.5)

image credits_art    = Text("{i}Featuring Art From:{/i}", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_art_01 = Text("(CC-BY) Zylinder", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_art_02 = Text("(GPL) Santiago Iborra", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_art_03 = Text("(GPL) Battle For Wesnoth (and addons)", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_art_04 = Text("(GPL) Super Tux", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_art_05 = Text("(GPL) Freecol", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_art_06 = Text("(CC0) Jon Sullivan", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_art_07 = Text("(CC0) Jesusalva", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_art_08 = Text("(-) Sapiboong", size=30, font="f/Jura-Regular.otf", text_align=0.5)

image credits_music    = Text("{i}Featuring Music From:{/i}", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_music_01 = Text("(CC0) Daniel Stephens", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_music_02 = Text("(CC0) Thomas Bruno", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits_music_03 = Text("(GPL) The Battle For Wesnoth (sfx)", size=30, font="f/Jura-Regular.otf", text_align=0.5)


image credits6x = Text("Additional Information check files:{p}gfx/ART_LICENSE{p}music/MUSIC_LICENSE", size=30, font="f/FreeMono.ttf", text_align=0.5)

image credits7 = Text(_("Special Thanks to: "), size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits8 = Text("Jonatas Nogueira et al.", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits9 = Text("Michel Glenn", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits9b = Text("Some Special Someone", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits10 = Text("Jesus Christ", size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits10b = Text(_("YOU!!"), size=30, font="f/Jura-Regular.otf", text_align=0.5)

image credits11 = Text(_("Thanks for testing.\n"), size=30, font="f/Jura-Regular.otf", text_align=0.5)
image credits12 = Text("Paradigm " + config.version + " " + persistent.release_name + "{p}Renpy 6.99.10", size=30, font="f/Jura-Regular.otf", text_align=0.5)

image creditsXE1 = Text("???", size=30, font="f/Jura-Regular.otf", text_align=0.5, color="#FF5555")
image creditsXE2 = Text("???", size=30, font="f/Jura-Regular.otf", text_align=0.5, color="#FF5555")
image creditsXE3 = Text("???", size=30, font="f/Jura-Regular.otf", text_align=0.5, color="#FF5555")

image credits_postwon = Text(_("You can seek for True End reloading your last save.\nDon't do that."), size=30, font="f/Imperator.ttf", text_align=0.5, color="#F00")
image credits_postwon2 = Text(_("- Jesusalva Jesusaves,\nEmperor of Linarius"), size=30, font="f/ShadowedBlack.ttf", text_align=0.5)



transform ATL_Credit:
    xalign 0.5
    ypos 1.1
    linear 15 ypos -0.25

label end_beta:
        show Hits Final with vpunch
        pause 0.4
        play music "music/The_Rush.mp3"
        pause 0.2
        scene black with Fade(2.0, 0.0, 0.0)
        $ show_hpbar=False
        centered "{cps=10}THE END.{/cps}"
        centered "The game has finished.\n\nThanks for playing."


        pause 0.1
        centered " {nw}"
        scene black with None
        window hide

        show IC sided at left with dissolve

        show credits0:
            xalign 0.5
            ypos 1.0
            linear 15 ypos -0.25

        pause 2.5

        show credits1 at ATL_Credit
        pause 2.5

        show credits2 at ATL_Credit
        pause 2.5

        show credits3 at ATL_Credit
        pause 2.5

        show credits4 at ATL_Credit
        pause 0.85
        show credits4a at ATL_Credit
        pause 0.67
        show credits4b at ATL_Credit
        pause 0.67
        show credits4c at ATL_Credit
        pause 0.67
        show credits4d at ATL_Credit
        pause 0.67
        show credits4e at ATL_Credit
        pause 2.5

        #show credits3 at ATL_Credit
        #pause 2.5

        # Credit Art

        show credits_art at ATL_Credit
        pause 1.5

        show credits_art_01 at ATL_Credit
        pause 0.5
        show credits_art_02 at ATL_Credit
        pause 0.5
        show credits_art_03 at ATL_Credit
        pause 0.5
        show credits_art_04 at ATL_Credit
        pause 0.5
        show credits_art_05 at ATL_Credit
        pause 0.5
        show credits_art_06 at ATL_Credit
        pause 0.5
        show credits_art_07 at ATL_Credit
        pause 0.5
        show credits_art_08 at ATL_Credit
        pause 2.5

        # Credit Musics

        show credits_music at ATL_Credit
        pause 1.5

        show credits_music_01 at ATL_Credit
        pause 0.5
        show credits_music_02 at ATL_Credit
        pause 0.5
        show credits_music_03 at ATL_Credit
        pause 0.5

        show credits6x at ATL_Credit
        pause 3.0

        show credits7 at ATL_Credit
        pause 1.0

        show credits8 at ATL_Credit
        pause 1.0

        show credits9 at ATL_Credit
        pause 1.0

        show credits9b at ATL_Credit
        pause 1.0

        show credits10 at ATL_Credit
        pause 1.0

        show credits10b at ATL_Credit
        pause 2.5

        show credits11 at ATL_Credit
        pause 2.5


        show credits12:
            xalign 0.5
            ypos 1.1
            linear 6.666666666666667 ypos 0.5
        pause 6.0
        hide IC sided with dissolve
        pause 9.0
        hide credits12 with dissolve

        if not persistent.won:
            show credits_postwon:
                xalign 0.5
                ypos 1.1
                linear 7.666666666666667 ypos 0.4
            pause 2.5
            show credits_postwon2:
                xalign 0.5
                ypos 1.1
                linear 5.0 ypos 0.6
            pause 7.0


        pause 0.5
        window show
        centered " {nw}"

        $ persistent.won=True
        $ config.main_menu_music = "music/The_Rush.mp3"

        $renpy.full_restart()





































init python:
  def assign_city_name(value):
    
    # switch value.

    # ACT 0
    if value == 0:
        return _("Weisheit Academy")

    # ACT 1 - 
    elif value == 1:
        return _("Linarius Prime")
    elif value == 2:
        return _("Lumnia Town")
    elif value == 3:
        return _("Forsaken Village")
    elif value == 4:
        return _("NE-1 Fortress")
    elif value == 5:
        return _("Dark Fortress")

    # ACT 2 - 
    else:
        print "[WARNING] Tried to get name for floor " + str(value) + ", but this is an invalid value."
        return _("Bugtest Area")

    return _("ERROR")

