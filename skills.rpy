########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Skills main file
# Skills are subdivided in five classes, as follow:
# MAGE      - Offensive skills. High damage, acc, and cost.
# WARRIOR   - Offensive skills. Moderate damage, acc and cost.
# TANKER    - Defensive skills. Buffs, Debuffs and Healing.
# RANGER    - Low damage and acc, high status inflicting chance, mod. cost.
# PASSIVE   - Grants permanent bonuses to the hero or the party.
#
# The hero can learn skills from all five classes, but PASSIVE skills are granted automagically.
# Therefore, you cannot add points to unlock passive skills, only to expensively upgrade them.
# 
# Each party member contains one skill. Skills (except PASSIVE) are cast at random.
# When not using AUTO mode, you may be able to use their skill, maybe?
#
###################################################################################################

# setup label moved to database.rpy

# Helper functions (ie. check if you have a skill)
init python:
    def haz_skill(en, skillname):
        for skill in en.skill:
            if skill.name == skillname:
                return True
        return False

    def get_skill(en, skillname):
        for skill in en.skill:
            if skill.name == skillname:
                return skill
        (player("Hm, %s does not know skill: %s.\nIs this a bug of some sort? (hint hint)" % (en.name, skillname)))
        # TODO REMOVE ME
        if config.developer or persistent.premium:
            for skill in skilist:
                if skill.name == skillname:
                    return skill

        return Skill()


    def retrieve_skill_from_db(id):
        for skill in skilist:
            if id == skill.name:
                return skill
        (player("{color=#F00}{b}CRITICAL ERROR{/b}{/color}\n\nRequested skill %s, that DOES NOT EXIST.\nGame databases seems to have been compromised. This game shall now shutdown to prevent CRAZY bugs. Sorry.\n\nIf you believe this is a mistake, contact developers or try rollback." % (id)))
        bug('Requested skill %s, that DOES NOT EXIST' % (id), fatal=True)
        raise KeyboardInterrupt("Segmentation Fault: retrieve_skill_from_db(%s).\n\nDatabase compromised, GAME ABORTED." % (id))
        renpy.quit()
        return Skill()

# This menu is always cast by the skcaster
label select_skill(skcaster):
    # setup
    $ selection=Skill()
    $ selected=False
    # Extensive menu
    # or config.developer -- All skills unlocked for devs.
    menu:
        "{image=ui/SA/Warrior.png}Warrior Skills" if "W" in skcaster.ordem:
            call warskill(skcaster)
        "{image=ui/SA/Ranger.png}Ranger Skills" if "R" in skcaster.ordem:
            call ranskill(skcaster)
        "{image=ui/SA/Mage.png}Mage Skills" if "M" in skcaster.ordem:
            call magskill(skcaster)
        "{image=ui/SA/Tanker.png}Tanker Skills" if "T" in skcaster.ordem:
            call tanskill(skcaster)
        "{image=gfx/mg/tray.png}Sanctums" if "S" in skcaster.ordem: # TODO
            call sctskill(skcaster)
        "Semi auto ([skcaster.skauto.name])" if skcaster.skauto.type != 'P':
            $ selection=get_skill(skcaster, skcaster.skauto.name) # Skill might have been updated
            $ selected=True

        "Cancel":
            $ done=False
            return

    # Have you picked a skill?
    if not selected:
        $ print "\033[1;31mBUG, REPORT ME!\n\033[0;31mPossible stack overflow detected.\nat: Skills.rpyc:select:notselected::call()\n\033[0m\n"
        call select_skill(skcaster)
        return
    $ renpy.block_rollback()    # Must be sure.

    ###################################
    # Now to use the selected skill
    # 

    # Do we have suffice MP?
    if skcaster.mp < selection.cost:
        player "[skcaster.name] do not have suffice MP to cast [selection.name].\nCurrent mana: [skcaster.mp]/[selection.cost]"
        $ print "\033[1;31mBUG, REPORT ME!\n\033[0;31mPossible stack overflow detected.\nat: Skills.rpyc:select:notselected::call()\n\033[0m\n"
        call select_skill(skcaster)
        return

    # XXX Target Set
    window hide
    $ set_combat_target()
    $ target=ui.interact()
    window show
    if target=="abort": # Abort button
        $ done=False
        return # Yes this works
    # XXX Target Set

    $skcaster.mp-=selection.cost

    if selection.type == 'DBG' or selection.type == 'P':
        pass # TODO: Hmmmmm...
    else:
        call auto_skill(target, selection, skcaster)

    # Save semiauto skill (eh. I hope it works?)
    $ skcaster.skauto=selection
    return












#######################################################
########## Class Specific Skills - Cast Menu

# Warrior skills
label warskill(skcaster):
    menu:
        "{a=sk_w1}{image=gfx/mg/w1.png}{/a}T1: Heavy Slash" if haz_skill(skcaster, "Heavy Slash"):
            $ selection=get_skill(skcaster, "Heavy Slash")
            $ selected=True
        "{a=sk_w2}{image=gfx/mg/w2.png}{/a}T2: Light Slash" if haz_skill(skcaster, "Light Slash"):
            $ selection=get_skill(skcaster, "Light Slash")
            $ selected=True
        "{a=sk_w3}{image=gfx/mg/w3.png}{/a}T2: Stab" if haz_skill(skcaster, "Stab"):
            $ selection=get_skill(skcaster, "Stab")
            $ selected=True
        "Cancel":
            pass

    return



# Tanker skills
label tanskill(skcaster):
    menu:
        "{a=sk_t1}{image=gfx/mg/t1.png}{/a}T1: Heal" if haz_skill(skcaster, "Heal"):
            $ selection=get_skill(skcaster, "Heal")
            $ selected=True
        "{a=sk_t2}{image=gfx/mg/t2.png}{/a}T2: Buff" if haz_skill(skcaster, "Buff"):
            $ selection=get_skill(skcaster, "Buff")
            $ selected=True
        "{a=sk_t3}{image=gfx/mg/t3.png}{/a}T2: Debuff" if haz_skill(skcaster, "Debuff"):
            $ selection=get_skill(skcaster, "Debuff")
            $ selected=True
        "Cancel":
            pass

    return



# Mage skills
label magskill(skcaster):
    menu:
        "{a=sk_m1}{image=gfx/mg/m1.png}{/a}T1: Magic Arrow" if  haz_skill(skcaster, "Magic Arrow"):
            $ selection=get_skill(skcaster, "Magic Arrow")
            $ selected=True
        "{a=sk_m2}{image=gfx/mg/m2.png}{/a}T2: Fireball" if haz_skill(skcaster, "Fireball"):
            $ selection=get_skill(skcaster, "Fireball")
            $ selected=True
        "{a=sk_m3}{image=gfx/mg/m3.png}{/a}T2: Ultima CRUSH" if haz_skill(skcaster, "Ultima CRUSH"):
            $ selection=get_skill(skcaster, "Ultima CRUSH")
            $ selected=True
        "Cancel":
            pass

    return



# Ranger skills
label ranskill(skcaster):
    menu:
        "{a=sk_r1}{image=gfx/mg/r1.png}{/a}T1: Paralizy Dart" if haz_skill(skcaster, "Paralizy Dart"):
            $ selection=get_skill(skcaster, "Paralizy Dart")
            $ selected=True
        "{a=sk_r2}{image=gfx/mg/r2.png}{/a}T2: Poison Dart" if haz_skill(skcaster, "Poison Dart"):
            $ selection=get_skill(skcaster, "Poison Dart")
            $ selected=True
        "{a=sk_r3}{image=gfx/mg/r3.png}{/a}T2: Draining Dart" if haz_skill(skcaster, "Draining Dart"):
            $ selection=get_skill(skcaster, "Draining Dart")
            $ selected=True
        "Cancel":
            pass

    return

# Sanctum skills
label sctskill(skcaster):
    menu:
        "{a=sk_s1}{image=gfx/mg/s1.png}{/a}Precise Shoot Sanctum" if haz_skill(skcaster, "Precise Shoot Sanctum"):
            $ selection=get_skill(skcaster, "Precise Shoot Sanctum")
            $ selected=True
        "{a=sk_s2}{image=gfx/mg/s2.png}{/a}Explosion Sanctum" if haz_skill(skcaster, "Explosion Sanctum"):
            $ selection=get_skill(skcaster, "Explosion Sanctum")
            $ selected=True
        "{a=sk_s3}{image=gfx/mg/s3.png}{/a}Protector Sanctum" if haz_skill(skcaster, "Protector Sanctum"):
            $ selection=get_skill(skcaster, "Protector Sanctum")
            $ selected=True
        "{a=sk_s4}{image=gfx/mg/s4.png}{/a}Be Quiet!" if haz_skill(skcaster, "Be Quiet!"):
            $ selection=get_skill(skcaster, "Be Quiet!")
            $ selected=True
        "{a=sk_s5}{image=gfx/mg/s5.png}{/a}Blue Mist" if haz_skill(skcaster, "Blue Mist"):
            $ selection=get_skill(skcaster, "Blue Mist")
            $ selected=True
        "{a=sk_tray}{image=gfx/mg/tray.png}{/a}Sanctum! (10 MP)" if haz_skill(skcaster, "Sanctum!"):
            $ selection=get_skill(skcaster, "Sanctum!")
            $ selected=True
        "Cancel":
            pass

    return

















#######################################################
########## Skill Evolution tree

# Create helpers
init python:
    def learned_skill(id, name, desc):
        ui.window(xalign=0.5, yalign=0.5, xmaximum=0.5, xminimum=0.5, ymaximum=0.5, yminimum=0.5)
        ui.hbox()
        ui.image("gfx/mg/"+str(id)+".png")
        ui.vbox()
        ui.text(name, size=24)
        ui.null(height=20)
        ui.text(desc, size=15)
        ui.close()
        ui.close()

    def remember_skill(id, name):
        tmprs=retrieve_skill_from_db(name)
        ui.window(xalign=0.5, yalign=0.5, xmaximum=0.5, xminimum=0.5, ymaximum=0.5, yminimum=0.5)
        ui.hbox()
        ui.image("gfx/mg/"+str(id)+".png")
        ui.vbox()
        ui.text(name, size=24)
        ui.null(height=20)
        ui.text(tmprs.desc, size=15)
        ui.close()
        ui.close()

    def skimg(en, id, name, button=False):
        if haz_skill(en, name):
            if not button:
                ui.image("gfx/mg/"+str(id)+".png")
            else:
                ui.imagebutton("gfx/mg/"+str(id)+".png", "gfx/mg/"+str(id)+".png", clicked=ui.returns(str(id) + "_@_" + str(name)))

    def append_sk(id, name, desc, type, cost, int=0, float=0.0, array=[], str="", lvl=1, halo="NONE"):
        skilist.append(Skill(name, type, cost, desc=desc, halo=halo))
        for sk in skilist:
            if sk.name == name:
                sk.int=int
                sk.float=float
                sk.array=array
                sk.string=str

    def learn_sk(id, name, en):
        #raise Exception("bug")
        if not haz_skill(en, name):
            tmpsk=retrieve_skill_from_db(name)
            en.skill.append(tmpsk)
            learned_skill(id, tmpsk.name, tmpsk.desc)
            (player("%s Learned %s!" % (en.name, tmpsk.name)))

    # ui.returns('toog')
    def skill_screen(who, inline=False):
        ui.window(xalign=0.5, yalign=0.5, xmaximum=0.5, xminimum=0.5, ymaximum=0.5, yminimum=0.5)
        # if len(who.ordem) < 5:
        ui.vbox()
        # else:
        #    ui.hbox()

        if not inline:
            if 'W' in who.ordem:
                ui.hbox()
                ui.image("ui/SA/Warrior.png")
                ui.text("WARRIOR    ", size=12)
                skimg(who, "w1", "Heavy Slash", True)
                skimg(who, "w2", "Light Slash", True)
                skimg(who, "w3", "Stab", True)
                ui.close()

            if 'R' in who.ordem:
                ui.hbox()
                ui.image("ui/SA/Ranger.png")
                ui.text("RANGER    ", size=12)
                skimg(who, "r1", "Paralizy Dart", True)
                skimg(who, "r2", "", True)
                skimg(who, "r3", "", True)
                ui.close()

            if 'T' in who.ordem:
                ui.hbox()
                ui.image("ui/SA/Tanker.png")
                ui.text("TANKER    ", size=12)
                skimg(who, "t1", "Heal", True)
                skimg(who, "t2", "Buff", True)
                skimg(who, "t3", "Debuff", True)
                ui.close()

            if 'M' in who.ordem:
                ui.hbox()
                ui.image("ui/SA/Mage.png")
                ui.text("MAGE    ", size=12)
                skimg(who, "m1", "Magic Arrow", True)
                skimg(who, "m2", "Fireball", True)
                skimg(who, "m3", "Ultima CRUSH", True)
                ui.close()

            if 'S' in who.ordem and len(who.ordem) <= 2:
                ui.hbox()
                ui.image("ui/SA/Sanctum.png")
                ui.text("Sanctums    ", size=12)
                skimg(who, "s1", "Precise Shoot Sanctum", True)
                skimg(who, "s2", "Explosion Sanctum", True)
                skimg(who, "s3", "Protector Sanctum", True)
                skimg(who, "s4", "Be Quiet!", True)
                skimg(who, "s5", "Blue Mist", True)
                ui.close()
        else:
                ui.hbox()
                skimg(who, "s1", "Precise Shoot Sanctum")
                skimg(who, "s2", "Explosion Sanctum")
                skimg(who, "s3", "Protector Sanctum")
                skimg(who, "s4", "Be Quiet!")
                skimg(who, "s5", "Blue Mist")

                skimg(who, "w1", "Heavy Slash")
                skimg(who, "r1", "Paralizy Dart")
                skimg(who, "t1", "Heal")
                skimg(who, "m1", "Magic Arrow")

                skimg(who, "w2", "Light Slash")
                skimg(who, "r2", "Poison Dart")
                skimg(who, "t2", "Buff")
                skimg(who, "m2", "Fireball")

                skimg(who, "w3", "Stab")
                skimg(who, "r3", "Draining Dart")
                skimg(who, "t3", "Debuff")
                skimg(who, "m3", "Ultima CRUSH")
                ui.close()

        ui.close()


# XXX HERO-ONLY FUNCTION
label skill_check:
    $ renpy.block_rollback()
    # Moved from dev_beta_setup
    if show_loading_bar:
        pause 0.1
        $ show_loading_bar=False

    # Ensure you have your class skill (id, en, name)
    if classe == C_MAGE:
        $ learn_sk("m1", "Magic Arrow", hero)

    if classe == C_TANK:
        $ learn_sk("t1", "Heal", hero)

    if classe == C_WARR:
        $ learn_sk("w1", "Heavy Slash", hero)

    if classe == C_RANG:
        $ learn_sk("r1", "Paralizy Dart", hero)

    # Grant hero some Sanctums (could be limited by difficulty)
    if not haz_skill(hero, "Explosion Sanctum"):
        $ learn_sk("s2", "Explosion Sanctum", hero)


    return

# SKILL EXPLANATION LABELS
label sk_r1:
    $ remember_skill('r1', "Paralizy Dart")
    pause
    return

label sk_r2:
    $ remember_skill('r2', "Poison Dart")
    pause
    return

label sk_r3:
    $ remember_skill('r3', "Draining Dart")
    pause
    return

label sk_w1:
    $ remember_skill('w1', "Heavy Slash")
    pause
    return

label sk_w2:
    $ remember_skill('w2', "Light Slash")
    pause
    return

label sk_w3:
    $ remember_skill('w3', "Stab")
    pause
    return

label sk_t1:
    $ remember_skill('t1', "Heal")
    pause
    return

label sk_t2:
    $ remember_skill('t2', "Buff")
    pause
    return

label sk_t3:
    $ remember_skill('t3', "Debuff")
    pause
    return

label sk_m1:
    $ remember_skill('m1', "Magic Arrow")
    pause
    return

label sk_m2:
    $ remember_skill('m2', "Fireball")
    pause
    return

label sk_m3:
    $ remember_skill('m3', "Ultima CRUSH")
    pause
    return

label sk_s1:
    $ remember_skill('s1', "Precise Shoot Sanctum")
    pause
    return

label sk_s2:
    $ remember_skill('s2', "Explosion Sanctum")
    pause
    return

label sk_s3:
    $ remember_skill('s3', "Protector Sanctum")
    pause
    return

label sk_s4:
    $ remember_skill('s4', "Be Quiet!")
    pause
    return

label sk_s5:
    $ remember_skill('s5', "Blue Mist")
    pause
    return

label sk_tray:
    $ remember_skill('tray', "Sanctum!")
    pause
    return

# This is not enough - I need a screen to manage skill updates (and an auto button, maybe)
# Maybe I should use GLOBAL skill points, you can invest on yourself, on your allies, on Passive Skills, or on making skilist[] upgrades (affects all). Sounds neat. TODO This label should be called upon adding member to battling.

# This label should NOT need to be casted directly, as add_member(str*) casts.
label skupd(born):
    if "M" in born.ordem:
        $ learn_sk("m1", "Magic Arrow", born)

    if "T" in born.ordem:
        $ learn_sk("t1", "Heal", born)

    if "W" in born.ordem:
        $ learn_sk("w1", "Heavy Slash", born)

    if "R" in born.ordem:
        $ learn_sk("r1", "Paralizy Dart", born)

    return



init python:
    skw=None
label skmanage:
    $ skwindow="P"
    $ show_hpbar=False
    jump skmng

label skmng:
  window hide
  python:
    #global skwindow
    #print "SKWINDOW: " + str(skwindow)
    if skw is None:
        skw="W"

    ui.window(xalign=.5, yalign=.5, xfill=False, yfill=False, xmaximum=0.8, ymaximum=0.8)
    ui.vbox()
    ui.text("{size=12}" + str(skp) + " Skill Points{/size}")

    #if skwindow == "P":
    #    ui.text("Permanent Skills")
    #    ui.hbox()
    #    ui.textbutton("HP +10", clicked=ui.returns("p_hp10"))
    #    ui.text("100 SKP")
    #    ui.close()

    if skw == "W":
        ui.text("Warrior Skills")
        jrncslvp=ui.viewport(xmaxium=190, xminimum=190, xfill=False, ymaximum=600, mousewheel=True, draggable=False)
        ui.hbox()

        ui.vbox() # DUMMY
        for dummy in party:
          if "W" in dummy.ordem:
            ui.hbox()
            ui.vbox()
            ui.text(dummy.name, size=14)
            ui.image("gfx/enemies/" + dummy.pic + ".png")
            ui.close()

            # --- Heavy Slash
            if not haz_skill(dummy, "Heavy Slash"):
                ui.vbox()
                ui.text("1 SKP", size=13)
                ui.imagebutton("gfx/mg/w1.png", clicked=ui.returns("w1_@_1_@_Heavy Slash_@_" + dummy.name))
                ui.close()
            else:
                ui.vbox()
                ui.text("{i}Onwed{/i}", size=13)
                ui.imagebutton("gfx/xm/w1.png", clicked=None)
                ui.close()


                # --- Light Slash
                if not haz_skill(dummy, "Light Slash"):
                    ui.vbox()
                    ui.text("1 SKP", size=13)
                    ui.imagebutton("gfx/mg/w2.png", clicked=ui.returns("w2_@_1_@_Light Slash_@_" + dummy.name))
                    ui.close()
                else:
                    ui.vbox()
                    ui.text("{i}Onwed{/i}", size=13)
                    ui.imagebutton("gfx/xm/w2.png", clicked=None)
                    ui.close()



                # --- Stab
                if not haz_skill(dummy, "Stab"):
                    ui.vbox()
                    ui.text("2 SKP", size=13)
                    ui.imagebutton("gfx/mg/w3.png", clicked=ui.returns("w3_@_2_@_Stab_@_" + dummy.name))
                    ui.close()
                else:
                    ui.vbox()
                    ui.text("{i}Onwed{/i}", size=13)
                    ui.imagebutton("gfx/xm/w3.png", clicked=None)
                    ui.close()
            ui.close()

        ui.close() # --DUMMY

        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar')
        ui.close()












    if skw == "M":
        ui.text("Mage Skills")
        jrncslvp=ui.viewport(xmaxium=190, xminimum=190, xfill=False, ymaximum=600, mousewheel=True, draggable=False)
        ui.hbox()

        ui.vbox() # DUMMY
        for dummy in party:
          if "M" in dummy.ordem:
            ui.hbox()
            ui.vbox()
            ui.text(dummy.name, size=14)
            ui.image("gfx/enemies/" + dummy.pic + ".png")
            ui.close()

            # --- Magic Arrow
            if not haz_skill(dummy, "Magic Arrow"):
                ui.vbox()
                ui.text("1 SKP", size=13)
                ui.imagebutton("gfx/mg/m1.png", clicked=ui.returns("m1_@_1_@_Magic Arrow_@_" + dummy.name))
                ui.close()
            else:
                ui.vbox()
                ui.text("{i}Onwed{/i}", size=13)
                ui.imagebutton("gfx/xm/m1.png", clicked=None)
                ui.close()


                # --- Fireball
                if not haz_skill(dummy, "Fireball"):
                    ui.vbox()
                    ui.text("1 SKP", size=13)
                    ui.imagebutton("gfx/mg/m2.png", clicked=ui.returns("m2_@_1_@_Fireball_@_" + dummy.name))
                    ui.close()
                else:
                    ui.vbox()
                    ui.text("{i}Onwed{/i}", size=13)
                    ui.imagebutton("gfx/xm/m2.png", clicked=None)
                    ui.close()



                # --- Ultima CRUSH
                if not haz_skill(dummy, "Ultima CRUSH"):
                    ui.vbox()
                    ui.text("2 SKP", size=13)
                    ui.imagebutton("gfx/mg/m3.png", clicked=ui.returns("m3_@_2_@_Ultima CRUSH_@_" + dummy.name))
                    ui.close()
                else:
                    ui.vbox()
                    ui.text("{i}Onwed{/i}", size=13)
                    ui.imagebutton("gfx/xm/m3.png", clicked=None)
                    ui.close()
            ui.close()







        ui.close() # --DUMMY

        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar')
        ui.close()

    if skw == "T":
        ui.text("Tanker Skills")
        jrncslvp=ui.viewport(xmaxium=190, xminimum=190, xfill=False, ymaximum=600, mousewheel=True, draggable=False)
        ui.hbox()

        ui.vbox() # DUMMY
        for dummy in party:
          if "T" in dummy.ordem:
            ui.hbox()
            ui.vbox()
            ui.text(dummy.name, size=14)
            ui.image("gfx/enemies/" + dummy.pic + ".png")
            ui.close()

            # --- Heal
            if not haz_skill(dummy, "Heal"):
                ui.vbox()
                ui.text("1 SKP", size=13)
                ui.imagebutton("gfx/mg/t1.png", clicked=ui.returns("t1_@_1_@_Heal_@_" + dummy.name))
                ui.close()
            else:
                ui.vbox()
                ui.text("{i}Onwed{/i}", size=13)
                ui.imagebutton("gfx/xm/t1.png", clicked=None)
                ui.close()


                # --- Buff
                if not haz_skill(dummy, "Buff"):
                    ui.vbox()
                    ui.text("1 SKP", size=13)
                    ui.imagebutton("gfx/mg/t2.png", clicked=ui.returns("t2_@_1_@_Buff_@_" + dummy.name))
                    ui.close()
                else:
                    ui.vbox()
                    ui.text("{i}Onwed{/i}", size=13)
                    ui.imagebutton("gfx/xm/t2.png", clicked=None)
                    ui.close()



                # --- Debuff
                if not haz_skill(dummy, "Debuff"):
                    ui.vbox()
                    ui.text("2 SKP", size=13)
                    ui.imagebutton("gfx/mg/t3.png", clicked=ui.returns("t3_@_2_@_Debuff_@_" + dummy.name))
                    ui.close()
                else:
                    ui.vbox()
                    ui.text("{i}Onwed{/i}", size=13)
                    ui.imagebutton("gfx/xm/t3.png", clicked=None)
                    ui.close()
            ui.close()







        ui.close() # --DUMMY

        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar')
        ui.close()

    if skw == "R":
        ui.text("Ranger Skills")
        jrncslvp=ui.viewport(xmaxium=190, xminimum=190, xfill=False, ymaximum=600, mousewheel=True, draggable=False)
        ui.hbox()

        ui.vbox() # DUMMY
        for dummy in party:
          if "R" in dummy.ordem:
            ui.hbox()
            ui.vbox()
            ui.text(dummy.name, size=14)
            ui.image("gfx/enemies/" + dummy.pic + ".png")
            ui.close()

            # --- Paralizy Dart
            if not haz_skill(dummy, "Paralizy Dart"):
                ui.vbox()
                ui.text("1 SKP", size=13)
                ui.imagebutton("gfx/mg/r1.png", clicked=ui.returns("r1_@_1_@_Paralizy Dart_@_" + dummy.name))
                ui.close()
            else:
                ui.vbox()
                ui.text("{i}Onwed{/i}", size=13)
                ui.imagebutton("gfx/xm/r1.png", clicked=None)
                ui.close()


                # --- ????
                if not haz_skill(dummy, "Poison Dart"):
                    ui.vbox()
                    #ui.text("(Coming", size=13, color="#F00")
                    ui.text("1 SKP", size=13)
                    ui.imagebutton("gfx/mg/r2.png", clicked=ui.returns("r2_@_1_@_Poison Dart_@_" + dummy.name)) # TODO: xm → mg
                    ui.close()
                else:
                    ui.vbox()
                    ui.text("{i}Onwed{/i}", size=13)
                    ui.imagebutton("gfx/xm/r2.png", clicked=None)
                    ui.close()



                # --- ????
                if not haz_skill(dummy, "Draining Dart"):
                    ui.vbox()
                    #ui.text("Soon)", size=13, color="#F00")
                    ui.text("2 SKP", size=13)
                    ui.imagebutton("gfx/mg/r3.png", clicked=ui.returns("r3_@_2_@_Draining Dart_@_" + dummy.name))
                    ui.close()
                else:
                    ui.vbox()
                    ui.text("{i}Onwed{/i}", size=13)
                    ui.imagebutton("gfx/xm/r3.png", clicked=None)
                    ui.close()
            ui.close()

        ui.close() # --DUMMY

        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar')
        ui.close()












    ui.vbox()
    ui.hbox()
    ui.textbutton("Warrior", clicked=ui.returns("W"))
    ui.textbutton("Tanker", clicked=ui.returns("T"))
    ui.close()
    ui.hbox()
    ui.textbutton("Ranger", clicked=ui.returns("R"))
    ui.textbutton("Mage", clicked=ui.returns("M"))
    ui.textbutton("Quit", clicked=ui.returns("X"))
    ui.close()
    ui.close()


    ui.close()
    _action=ui.interact(suppress_overlay=True)
    #print str(_action)

  window show
  python:
    if not "_@_" in _action:
        pass # Will be handled by Ren'Python
    else:
        # Now we do python processing
        skact=_action.split("_@_")
        # skact is [type, cost, skid, pid] - debug marker below
        #print str(skact)
        if skp < int(skact[1]):
            _(e("Insufficient Skill Points!"))
        else:
            learn_sk(skact[0], skact[2], get_player_by_name(skact[3]))
            skp-=int(skact[1])

  if _action == "W":
    $ skw="W"

  if _action == "M":
    $ skw="M"

  if _action == "T":
    $ skw="T"

  if _action == "R":
    $ skw="R"

  if _action == "X":
    $ show_hpbar=True
    return


  jump skmanage

























label auto_skill(target, selection, skcaster):
    # Attack Skills
    if selection.type == 'A':
        if CRandom() <= (selection.float*skcaster.luk):
            play sound "sfx/"+renpy.random.choice(['M2', 'M3', 'M4', 'M5', 'M6'])+".ogg"
            #$ui.at(Position(xpos=750, ypos=295, xanchor="center", yanchor=0.0))
            $renpy.show('skhalo ' + selection.halo, at_list=[truecenter])
            $dmg=renpy.random.randint(selection.array[0], selection.array[1])
            $dmg=recalculate_damage(target, dmg, skcaster, False)
            if not skcaster in EnemyParty:
                $dmg=int(dmg*skcaster.luk)
            $hit_someone_verbose(target, dmg)
            $announce_dmg(dmg, target)
            "[selection.name] causes [dmg] damage to [target.name!t]!"
            hide skhalo
        else:
            "Skill fails!"

    # Physical Attack Skills (use weapon as base)
    elif selection.type == 'PA':
        if selection.float == 0.0:
            $ selection.float=skcaster.weapon.acc
        if CRandom() <= selection.float:
            $renpy.show('skhalo '+selection.halo, at_list=[truecenter])
            if selection.int < 0:
                $ i=renpy.random.randint(selection.array[0], selection.array[1])
            else:
                $ i=selection.int
            $dmg=renpy.random.randint(skcaster.weapon.mindmg+i, skcaster.weapon.maxdmg+i)
            $dmg=recalculate_damage(target, dmg, skcaster, False)
            $hit_someone_verbose(target, dmg)
            $announce_dmg(dmg, target)
            "[selection.name] causes [dmg] damage to [target.name!t]!"
        else:
            "Skill fails!"

    # AoE Skills
    elif selection.type == 'E':
        if CRandom() <= (selection.float*skcaster.luk):
            $dmg=renpy.random.randint(selection.array[0], selection.array[1])
            $dmg=int(dmg*skcaster.luk)
            python:
                if target in EnemyParty:
                    for vict in EnemyParty:
                        if vict.hp > 0:
                            renpy.show('skhalo '+selection.halo, at_list=[Position(xpos=get_en_xypos(vict)[0], ypos=get_en_xypos(vict)[1]+60, xanchor="center", yanchor=0.0)])
                            #renpy.show('skhalo '+selection.halo, at_list=[truecenter])
                            renpy.play("sfx/"+renpy.random.choice(['M', 'MA'])+".ogg", channel="sound")
                            #renpy.show("flameburst", xpos=0.5, ypos=0.5)
                            hit_someone_verbose(vict, recalculate_damage(target, dmg, skcaster, False))
                elif target in battling:
                    for vict in battling:
                        if vict.hp > 0:
                            renpy.show('skhalo '+selection.halo, at_list=[Position(xpos=get_en_xypos(vict)[0], ypos=get_en_xypos(vict)[1]+60, xanchor="center", yanchor=0.0)])
                            hit_someone_verbose(vict, recalculate_damage(target, dmg, skcaster, False))
                else:
                    dmg=0
                    skcaster.mp+=selection.cost
                    print "\033[31mSomething went wrong: AoE (E) skill: Invalid target, expected EP or BT, got nil: " + str(target.name)
            $announce_dmg(dmg, "AoE Skill")
            "[selection.name] splashes and causes [dmg] damage!"
            hide skhalo
        else:
            "Skill fails!"

    # Limited AoE Skills (untested)
    elif selection.type == 'AoE':
        if CRandom() <= (selection.float*skcaster.luk):
            $dmg=renpy.random.randint(selection.array[0], selection.array[1])
            $dmg=int(dmg*skcaster.luk)
            if target in EnemyParty:
                $tmp_sk_id=EnemyParty.index(target)
                $tmp_scope=EnemyParty
            elif target in battling:
                $tmp_sk_id=battling.index(target)
                $tmp_scope=battling
            else:
                $print "\033[31mSomething went wrong: AoE (L) skill: Invalid target, got nil: " + str(target.name)
                $raise Exception("WHAT IS THIS SKILL SUPPOSED TO DO?! Error on limited AoE skill: Invalid Target!\n\nYou must rollback!")
                # We only force a fix if an enemy tried the skill. Otherwise, we let the game crash.
                if skcaster in EnemyParty:
                    $tmp_sk_id=0
                    $tmp_scope=[]
            python:
                # Hit target on positive side if there is any
                if tmp_sk_id+1 < len(tmp_scope):
                    renpy.show('skhalo '+selection.halo, at_list=[truecenter])
                    renpy.play("sfx/"+renpy.random.choice(['M', 'MA'])+".ogg", channel="sound")
                    hit_someone_verbose(tmp_scope[tmp_sk_id+1], recalculate_damage(tmp_scope[tmp_sk_id+1], dmg, skcaster, False))
                    announce_dmg(dmg, tmp_scope[tmp_sk_id+1])
                # Hit target on negative side if there is any
                if tmp_sk_id-1 >= 0:
                    renpy.show('skhalo '+selection.halo, at_list=[truecenter])
                    renpy.play("sfx/"+renpy.random.choice(['M', 'MA'])+".ogg", channel="sound")
                    hit_someone_verbose(tmp_scope[tmp_sk_id-1], recalculate_damage(tmp_scope[tmp_sk_id-1], dmg, skcaster, False))
                    announce_dmg(dmg, tmp_scope[tmp_sk_id-1])
                # Hit target
                renpy.show('skhalo '+selection.halo, at_list=[truecenter])
                renpy.play("sfx/"+renpy.random.choice(['M', 'MA'])+".ogg", channel="sound")
                hit_someone_verbose(target, recalculate_damage(tmp_scope[tmp_sk_id], dmg, skcaster, False))
                announce_dmg(dmg, target)
            hide skhalo
            "[selection.name] causes [dmg] damage to [target.name!t] and surroundings!"
        else:
            "Skill fails!"

    # Paralyze+AoE skill
    elif selection.type == 'ICEBREATH':
        $dmg=renpy.random.randint(selection.array[0], selection.array[1])
        python:
            if target in battling:
                for vict in battling:
                    if vict.hp > 0:
                        if CRandom() <= selection.float:
                            renpy.show("icebubble", at_list=[truecenter]) # TODO
                            renpy.play("sfx/M.ogg", channel="sound")
                            #renpy.show("flameburst", xpos=0.5, ypos=0.5)
                            hit_someone_verbose(vict, recalculate_damage(target, dmg, skcaster, False))
                            if CRandom() <= selection.float: # Roll the dice again, to paralyze. It's 64% to paralyze with 80% acc.
                                vict.frozen+=selection.int
            else:
                dmg=0
                #skcaster.mp+=selection.cost
                raise Exception("\033[31mSomething went wrong: AoE skill: Invalid target, expected EP or BT, got nil: " + str(target.name))
        $announce_dmg(dmg, 'AoE')
        "[selection.name] splashes and causes [dmg] damage!"
        hide skhalo

    # Healing Skills
    elif selection.type == 'H' or selection.type == 'MH':
        $ renpy.play("sfx/heal.ogg", channel="sound")
        $ renpy.show('skhalo '+selection.halo, at_list=[truecenter])
        if selection.type == "MH":
            python:
                if target in battling:
                    for victet in battling:
                        dmg=renpy.random.randint(selection.array[0], selection.array[1])
                        dmg=int(dmg*skcaster.luk)
                        hit_someone_verbose(target, -dmg)
                elif target in EnemyParty:
                    for victet in EnemyParty:
                        dmg=renpy.random.randint(selection.array[0], selection.array[1])
                        dmg=int(dmg*skcaster.luk)
                        hit_someone_verbose(target, -dmg)
                else:
                    print "Something went wrong, invalid MultiHeal target! (not in BT nor EP)"
            $announce_dmg(dmg, 'AoE')
        else:
            $dmg=renpy.random.randint(selection.array[0], selection.array[1])
            $dmg=int(dmg*skcaster.luk)
            $hit_someone_verbose(target, -dmg)
            $announce_dmg(dmg, target)
        "Thanks to [selection.name], [target.name!t] recovered [dmg] hitpoints!"
        hide skhalo

    # Summoning Skills
    elif selection.type == 'SUMMON':
        #$i=0
        if CRandom() <= selection.float:
            $ renpy.show('skhalo '+selection.halo, at_list=[Position(xpos=get_en_xypos(EnemyParty[len(EnemyParty)-1])[0], ypos=get_en_xypos(EnemyParty[len(EnemyParty)-1])[1]+60, xanchor="center", yanchor=0.0)])
            #$ renpy.pause(0.3)
            if skcaster in EnemyParty and len(EnemyParty) < 9:
                $EnemyParty.append(reg("Summoned Monster #%02d" % (len(EnemyParty)), selection.int, pic=selection.string))
            elif skcaster in battling and len(battling) < 9:
                $battling.append(reg("Summoned Monster #%02d" % (len(battling)), selection.int, pic=selection.string))
            else:
                "Ops, summon failed. Please rollback."
        "Thanks to [selection.name], Summon!"
        hide skhalo

    # Buffing and Debuffing Skills
    elif selection.type == 'B' or selection.type == 'MB':
        $ renpy.play("sfx/heal.ogg", channel="sound")
        $ renpy.show('skhalo '+selection.halo, at_list=[truecenter])
        if selection.type == "MB":
            python:
                if target in battling:
                    for victet in battling:
                        if victet.hp > 0:
                            victet.add_dmg+=selection.float
                            victet.add_def_rt+=selection.float
                            victet.add_dmg_dur+=selection.int
                            victet.add_def_dur+=selection.int
                    target=Enemy("Party", 0)
                elif target in EnemyParty:
                    for victet in EnemyParty:
                        if victet.hp > 0:
                            victet.add_dmg+=selection.float
                            victet.add_def_rt+=selection.float
                            victet.add_dmg_dur+=selection.int
                            victet.add_def_dur+=selection.int
                    target=Enemy("Party", 0)
                else:
                    print "Something went wrong, invalid MultiBuff target! (not in BT nor EP)"

        $target.add_dmg+=selection.float
        $target.add_def_rt+=selection.float
        $target.add_dmg_dur+=selection.int
        $target.add_def_dur+=selection.int

        $tmp_sk_dmg=target.add_dmg*100
        $tmp_sk_def=target.add_def_rt*100
        #$hit_player_verbose(-dmg)
        "Thanks to [selection.name], [target.name!t] power changed!\n\n\
        Attack Bonus: [tmp_sk_dmg]%% for [target.add_dmg_dur] hits\n\
        Defense Bonus: [tmp_sk_def]%% for [target.add_def_dur] hits"
        hide skhalo

    # Ranger Skills
    elif selection.type == 'R':
        if renpy.random.random() <= (selection.float*skcaster.luk): # TODO: CRandom() was not used for MAJOR balance purposes
            play sound "sfx/"+renpy.random.choice(['R', 'R2'])+".ogg"
            $ renpy.show('skhalo '+selection.halo, at_list=[truecenter])
            $dmg=renpy.random.randint(selection.array[0], selection.array[1])
            $dmg=recalculate_damage(target, dmg, skcaster, False)
            if not skcaster in EnemyParty:
                $dmg=int(dmg*skcaster.luk)
            $hit_someone_verbose(target, dmg)
            $calc_weapon_skill(target, Weapon(sk_pc=1, sk_cmd=selection.string), skcaster)
            $announce_dmg(dmg, target)
            "[selection.name] causes [dmg] damage to [target.name!t]! Effects unsleashed!"
            hide skhalo
        else:
            "Skill fails!"

    else:
        $ bug("Skill Type: %s is Invalid!\n\nName: %s" % (str(selection.type), str(selection.name)) )
        $ print "\033[1;31mBUG, REPORT ME!\n\033[0;31mPossible stack overflow detected.\nat: Skills.rpyc:select:invalidtype::call()\n\033[0m\n"
        if skcaster in battling:
            call select_skill(skcaster)

    return












#begin atl_image1
image skhalo flameburst:
    "halo flame burst 1"
    pause .1
    "halo flame burst 2"
    pause .1
    "halo flame burst 3"
    pause .1
    "halo flame burst 4"
    pause .1
    "halo flame burst 5"
    pause .1
    "halo flame burst 6"
    pause .1
    "halo flame burst 7"
    pause .1
    "halo flame burst 8"
    pause .2
    "NONE full"
    #hide flameburst
    #repeat 2
#end atl_image1

image skhalo icebubble:
    "halo bubble1"
    pause .1
    "halo bubble2"
    pause .1
    "halo bubble3"
    pause .1
    "halo bubble4"
    pause .1
    "halo bubble a"
    pause .1
    "halo bubble b"
    pause .1
    "halo bubble c"
    pause .1
    "halo bubble d"
    pause .1
    "halo bubble e"
    pause .1
    "halo bubble f"
    pause .1
    "halo bubble g"
    pause .1
    "halo bubble land1"
    pause .1
    "halo bubble land2"
    pause .1
    "halo bubble land3"
    pause .1
    "halo bubble land4"
    pause .1
    "halo bubble land5"
    pause .1
    "halo bubble land6"
    pause .1
    "halo bubble land7"
    pause .1
    "halo bubble land8"
    pause .2
    "NONE full"
    #hide flameburst
    #repeat 2

image skhalo implosion:
    "halo implosion 1 1"
    pause .1
    "halo implosion 1 2"
    pause .1
    "halo implosion 1 3"
    pause .1
    "halo implosion 1 4"
    pause .1
    "halo implosion 1 5"
    pause .1
    "halo implosion 1 6"
    pause .1
    "halo implosion 1 7"
    pause .1
    "halo implosion 1 8"
    pause .1
    "halo implosion 1 9"
    pause .1
    "halo implosion 1 10"
    pause .2
    "NONE full"

image skhalo flamedrown:
    "halo inf1"
    xalign .5
    pause .1
    "halo inf2"
    xalign .5
    pause .1
    "halo inf3"
    xalign .5
    pause .1
    "halo inf4"
    xalign .5
    pause .1
    "halo inf5"
    xalign .5
    pause .2
    "halo inf6"
    xalign .5
    pause .2
    "halo inf5"
    xalign .5
    pause .1
    "halo inf4"
    xalign .5
    pause .1
    "halo inf3"
    xalign .5
    pause .1
    "halo inf2"
    xalign .5
    pause .1
    "halo inf1"
    xalign .5
    pause .1
    "NONE full"

image skhalo firewheel:
    "halo firewheel s1"
    pause .1
    "halo firewheel s2"
    pause .1
    "halo firewheel s3"
    pause .1
    "halo firewheel s4"
    pause .1
    "halo firewheel s5"
    pause .1
    "halo firewheel s6"
    pause .1
    "halo firewheel s1"
    pause .1
    "halo firewheel s2"
    pause .1
    "halo firewheel s3"
    pause .1
    "halo firewheel s4"
    pause .1
    "halo firewheel s5"
    pause .1
    "halo firewheel s6"
    pause .2
    "NONE full"

image skhalo lightbeam:
    "halo light beam 1"
    pause .15
    "halo light beam 2"
    pause .15
    "halo light beam 3"
    pause .15
    "halo light beam 4"
    pause .15
    "halo light beam 5"
    pause .15
    "halo light beam 6"
    pause .15
    "halo light beam 7"
    pause .25
    "NONE full"

image skhalo lightning:
    #play sound "sfx/lightning.ogg"
    "halo lightning bolt 2 1"
    pause .1
    "halo lightning bolt 2 2"
    pause .1
    "halo lightning bolt 2 3"
    pause .1
    "halo lightning bolt 2 4"
    pause .2
    "NONE full"

image skhalo teleport:
    "halo teleport 1"
    pause .1
    "halo teleport 2"
    pause .1
    "halo teleport 3"
    pause .1
    "halo teleport 4"
    pause .1
    "halo teleport 5"
    pause .1
    "halo teleport 6"
    pause .1
    "halo teleport 7"
    pause .1
    "halo teleport 8"
    pause .1
    "halo teleport 9"
    pause .2
    "NONE full"

image skhalo aerowheel:
    "halo aerowheel s1"
    pause .1
    "halo aerowheel s2"
    pause .1
    "halo aerowheel s3"
    pause .1
    "halo aerowheel s1"
    pause .1
    "halo aerowheel s2"
    pause .1
    "halo aerowheel s3"
    pause .2
    "NONE full"

image skhalo firemagic:
    "halo magic torture halo1"
    pause .1
    "halo magic torture halo2"
    pause .1
    "halo magic torture halo3"
    pause .1
    "halo magic torture halo4"
    pause .1
    "halo magic torture halo5"
    pause .2
    "NONE full"

image skhalo lightning-small:
    "halo small lightning1"
    pause .1
    "halo small lightning2"
    pause .1
    "halo small lightning3"
    pause .1
    "halo small lightning4"
    pause .1
    "halo small lightning5"
    pause .1
    "halo small lightning6"
    pause .1
    "halo small lightning7"
    pause .1
    "halo small lightning8"
    pause .2
    "NONE full"

image skhalo bluebeam:
    "halo zamage halo1"
    pause .1
    "halo zamage halo2"
    pause .1
    "halo zamage halo3"
    pause .1
    "halo zamage halo2"
    pause .1
    "halo zamage halo1"
    pause .1
    "halo zamage halo3"
    pause .2
    "NONE full"

image skhalo summonbugs:
    "halo bugs"
    pause .1
    "halo bugs2"
    pause .1
    "halo bugs3"
    pause .1
    "halo bugs4"
    pause .1
    "halo bugs5"
    pause .1
    "halo bugs6"
    pause .1
    "halo bugs7"
    pause .1
    "halo bugs8"
    pause .1
    "halo bugs9"
    pause .2
    "halo bugs5"
    pause .1
    "halo bugs4"
    pause .1
    "halo bugs2"
    pause .1
    "halo bugs2"
    pause .1
    "halo bugs"
    pause .1
    "NONE full"

image skhalo recruit:
    "halo recruit a 1"
    pause .1
    "halo recruit a 2"
    pause .1
    "halo recruit a 3"
    pause .1
    "halo recruit a 4"
    pause .1
    "halo recruit a 5"
    pause .1
    "halo recruit a 6"
    pause .1
    "halo recruit a 7"
    pause .1
    "halo recruit a 8"
    pause .1
    "halo recruit a 9"
    pause .1
    "halo recruit a 10"
    pause .1
    "halo recruit a 11"
    pause .2
    "halo recruit a 12"
    pause .1
    "halo recruit a 13"
    pause .1
    "halo recruit a 12"
    pause .1
    "halo recruit a 13"
    pause .1
    "NONE full"

image skhalo NONE:
    "NONE full"
#transform enpos:
#   xalign 0.5 yalign 0.5
