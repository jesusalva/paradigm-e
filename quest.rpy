########################################################################################
#     This file is part of Project-E.
#     Copyright (C) 2015-2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Quest log

init python:
 class QuestEntry:
  def __init__(self, name, text):
    self.name=name # Capitalization
    self.text=text

 class QuestLog:
  def __init__(self):
    self.entries=[]
    self.status="Open"

  def AddEntry(self, title, text):
    self.entries.append(QuestEntry(title, text))

  def exist_test(self):
    pass

 # Main functionality of quest log. All others are core functions used by this one.
 class Journal:
  def __init__(self):
    self.quests={}

  def listall(self):
    tmp_jrn=[]
    for i in self.quests:
        tmp_jrn.append(i)
    return sorted(tmp_jrn)

  def add_quest(self, name):
        self.quests[name]=QuestLog()

  def add_quest_update(self, name, title, desc):
        self.quests[name]=QuestLog()
        self.quests[name].AddEntry(title, desc)

  def haz_quest(self, name):
    try:
        self.quests[name].exist_test()
        return True
    except:
        return False

  def update_quest(self, name, title, desc):
    if self.haz_quest(name):
        self.quests[name].AddEntry(title, desc)
    else:
        self.add_quest_update(name, title, desc)
    renpy.notify("Quest log updated.")
    renpy.block_rollback() # Double-safe

  def close(self, name, status="Closed"):
    if self.haz_quest(name):
        self.quests[name].status=status
    else:
        self.add_quest(name)
        self.quests[name].status=status

label quest_screen:
    $qs_id=""
    $qstmp_len=-1
    $qs_current=QuestLog()
    $qs_entry=QuestEntry("Capitalized Text Title", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed luctus placerat tristique. Vivamus fermentum erat ipsum, et pulvinar orci sagittis et. Proin imperdiet ultrices nulla, a pharetra nunc. Donec facilisis vel lectus sed facilisis. Maecenas dui justo, tempus sed gravida at, dignissim id odio. Praesent viverra in est et commodo. Vivamus sem enim, cursus tempor dolor vel, bibendum vestibulum nibh. Praesent molestie odio enim, id pellentesque neque eleifend in. Maecenas sem nibh, hendrerit at consectetur vitae, pulvinar et turpis.\n\
\n\
Ut tempus fringilla efficitur. Etiam auctor neque ex, sed mollis lorem placerat quis. Duis laoreet condimentum orci sed faucibus. Quisque et blandit arcu, ac vestibulum eros. Nam a tincidunt nisl. Sed semper facilisis augue, quis scelerisque urna. Pellentesque at felis aliquet urna mattis molestie et non libero. Aenean maximus interdum ante hendrerit lacinia. Ut fermentum posuere mauris ac fermentum. Quisque pellentesque est et iaculis iaculis turpis duis.")
    jump qs_core

label qs_core:
  window hide
  python:
    ui.window(xalign=.5, yalign=.5, xmaximum=.9, ymaximum=.9)
    ui.hbox() # HBOX
    vp_qscroll=ui.viewport(xfill=False, mousewheel=True, draggable=False)
    ui.vbox(xmaximum=.4) # List of items

    for i in ql.listall():
        if ql.quests[i].status == "Open":
            ui.textbutton(str(i), clicked=ui.returns('SWITCH_@_' + str(i)))
        else:
            ui.textbutton("{color=#003c78}"+str(i)+"{/color}", clicked=ui.returns('SWITCH_@_' + str(i)), background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#c8ffff"),0,12))

    ui.textbutton("{image=gfx/abort.png}Quit", clicked=ui.returns('ABORT'))

    ui.close() # -- List of items
    ui.bar(adjustment=vp_qscroll.yadjustment, style='vscrollbar')

    ui.hbox() # Quest Details

    ui.imagebutton("gfx/arrow2.png", clicked=ui.returns('PREV'), yalign=.5)
    ui.vbox()
    ui.text("Quest: "+str(qs_id), size=24, font="f/unifont.ttf")
    ui.text("Status: "+str(qs_current.status), size=24, font="f/unifont.ttf")
    ui.null(height=18)
    ui.text(qs_entry.name, size=32, font="f/ShadowedBlack.ttf")
    ui.null(height=18)
    ui.text(qs_entry.text, size=18, font="f/Imperator.ttf")
    ui.null(height=8)
    if len(qs_current.entries):
        ui.text("Page %d/%d" % (qstmp_len+1, len(qs_current.entries)), size=16, font="f/unifont.ttf")
    else:
        ui.text("Page 1/1", size=16, font="f/unifont.ttf")
    ui.close()
    ui.imagebutton("gfx/arrow.png", clicked=ui.returns('NEXT'), yalign=.5)

    ui.close() # -- Quest Details

    ui.close() # -- HBOX
    qs_act=ui.interact(suppress_overlay=True)

  if "SWITCH_@_" in qs_act:
    $ qs_id=qs_act.replace("SWITCH_@_", '')
    $ qs_current=ql.quests[qs_id]
    $ qstmp_len=len(qs_current.entries)-1
    #$ print qs_id +": " + str(qstmp_len)
    if qstmp_len >= 0:
        $ qs_entry=qs_current.entries[qstmp_len]
    else:
        $ qs_entry=QuestEntry('', 'This quest does not have a text.')
    jump qs_core
  elif qs_act == "PREV":
    if qstmp_len > 0:
        $ qstmp_len-=1
        $ qs_entry=qs_current.entries[qstmp_len]
    elif len(qs_current.entries) > 0:
        $ qstmp_len=len(qs_current.entries)-1
        $ qs_entry=qs_current.entries[qstmp_len]
    jump qs_core
  elif qs_act == "NEXT":
    if qstmp_len < len(qs_current.entries)-1:
        $ qstmp_len+=1
        $ qs_entry=qs_current.entries[qstmp_len]
    elif len(qs_current.entries) > 0:
        $ qstmp_len=0
        $ qs_entry=qs_current.entries[qstmp_len]

    jump qs_core


  window show
  return





