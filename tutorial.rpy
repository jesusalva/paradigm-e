﻿########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
image TutoBG Sanctuary = Frame("gfx/bg/sanctuary.jpg",0,0)
image UpArrow          = "gfx/system_up_arrow.png"
define eSophia         = Character('Sophia Wisdom'  , color="#ff5e00") # Sophia Wisdom, Academic Expert


label tutorial:
    if playing:
        "{b}{color=#F00}ERROR{/color}{/b}" "You CANNOT start tutorial when the game is started!"
        return

    $ sophia_mood=0
    $ tutorial_adm=False
    $ save_name=_("Area ") + str(g_cur_level) + ': ' + str(gcl_name) + "\n"   # Set save name.
    play music MUSIC_SOCIAL08 fadein 2.0
    scene TutoBG Sanctuary
    show WISDOM full
    with Dissolve(1.0)
    #"???" "There's wisdom."
    eSophia "My name is Wisdom. Sophia Wisdom."
    eSophia "Welcome to Project E. This is the Academy of Weisheit, and I am here to guide you. Let's start with the basics, shall we?"
    $ playing=True
    $ show_hpbar=True
    menu:
        "Please teach me everything in detail (Complete Tutorial)":
            pass
        "_@_NODISPLAY_@_I'm in a hurry, please... (Quick Tutorial)":
            pass
        "Jump to a later section from tutorial":
            $ tutorial_adm=True
            $ sophia_mood+=1 # Sorry. Mood is for the ones who bear everything.
            menu TutorialAdm:
                "Basics":
                    pass
                "The Ingame Menu":
                    jump tutorial_menu
                "The City Menu":
                    jump tutorial_city
                "Battle":
                    jump tutorial_battle
                "I already know everything!":
                    jump tutorial_end

    show UpArrow with dissolve:
        xpos 0.1 ypos 0.25
    eSophia "This panel will accompain you for most of your adventure. It contains the most basic information you need.\nIt'll usually be talking about yourself, or any ally that be attacking at the moment."
    eSophia "It takes about 1/4 from your upper screen, and contains various crucial info about you."
    show UpArrow as HPMASTER with dissolve:
        xpos 0.4 ypos 0.05
        rotate -90.00
    eSophia "Here your name, life (green bar) and mana (blue bar) is displayed."
    $ hit_player_verbose(150)
    eSophia "The life bar will change color if it starts getting in critical conditions."
    $ hit_player_verbose(49)
    eSophia "Remember, if your HP drops to zero or below, it'll be game over (except on rare ocasions). So take care with it."
    $hit_player_verbose(-199)
    show UpArrow as HPMASTER with move:
        xpos 0.4 ypos 0.10
        rotate -90.00
    eSophia "In numbers it'll also display your level and HP."
    show UpArrow as HPMASTER with move:
        xpos 0.3 ypos 0.15
        rotate -90.00
    eSophia "On this area it'll display additional information, like how many followers you have, which map you're on, etc."
    eSophia "Here is also the Menu button. It contains most critical functions, but on battle, you can only change party. Click here and I'll show you how this works."
    hide HPMASTER with None
    hide UpArrow with None
    if tutorial_adm:
        jump TutorialAdm

label tutorial_menu:
    if not renpy.showing("IC default"):
        show IC default with Dissolve(0.2)
    menu:
        eSophia "Char Info contains the most basic information about you. Here you can also check your friends during battle, but you're not allowed to do changes. Click on it!"
        "Char Info":
            $ show_money=True
            # Experimental
            $ ui.window(xfill=False, yfill=False, xalign=0.91, yalign=0.25, ymaximum=80)
            $ ui.vbox()
            $ ui.text("%s, Lv %d, Area #%02d\n{size=12}%d XP to level up{/size}" % (yname, hero.level, g_cur_level, int(hero.mxp-hero.xp)  ))
            $ ui.text("STR: %02d DEX: %02d VIT: %02d LUK: %02d" % ((hero.str-1)*100, (hero.dex-1)*100, (hero.vit-1)*100, (hero.luk-1)*100 ) )
            $ skill_screen(hero, inline=True)
            $ ui.close()
            $ herX=_("hero")
            if not ymale:
                $herX=_("heroine")
            "Char sheet" "Player [yname], the [HClass!t] [herX!t].\n\
Primary weapon: [hero.weapon.name!t]   ,   Damage: [hero.weapon.mindmg] - [hero.weapon.maxdmg]   ,   Precision: [hero.weapon.acc]\n\
Off-hand: [hero.offhand.name!t]\n\
Armor: [hero.armor.name!t]   ,   Absorbs util [hero.armor.abdg] damage in a rate of [hero.armor.rate] : 1.\n\
Current area: [g_cur_level]\n\
Current Level: [hero.level]\n\
XP: [hero.xp]/ [hero.mxp]{fast}\n\
\n\
Project E - v[config.version] \"[persistent.release_name]\""

            $ show_money=False
        "_@_NODISPLAY_@_Inventary" if not fighting: # During fights use standard useitem command
            pass
        "_@_NODISPLAY_@_Skills" if not fighting:
            pass
        "_@_NODISPLAY_@_Party Settings" if not fighting:
            pass
        "_@_NODISPLAY_@_   ":#Quit": # Quit is... odd.
            pass

    call setup_weapons
    $ hero.items.append(Bread)
    menu:
        eSophia "Inventary displays your inventory. Here you can see all your items, and use them. I've given you a bread. Try eating it, by opening the inventary and clicking on \"Heal\" button. Do not forget to select your hero!"
        "_@_NODISPLAY_@_Char Info":
            pass
        "Inventary": # During fights use standard useitem command
            call inv_screen
        "_@_NODISPLAY_@_Skills" if not fighting:
            pass
        "_@_NODISPLAY_@_Party Settings" if not fighting:
            pass
        "_@_NODISPLAY_@_   ":#Quit": # Quit is... odd.
            pass

    if Bread in hero.items:
        $ hero.items.remove(Bread)
        $ sophia_mood+=1
        eSophia "You're not hungry? Then I'm removing the Bread from your bag."
    call setup_skill #skills.rpy file
    $ hero.ordem="WMTR"
    $ skp=1
    menu:
        eSophia "Skills is one of the most complex here. It allows you to learn new skills. I'm giving you ONE skill point, to learn a basic skill with your hero."
        "_@_NODISPLAY_@_Char Info":
            pass
        "_@_NODISPLAY_@_Inventary": # During fights use standard useitem command
            pass
        "Skills":
            call skmanage
        "_@_NODISPLAY_@_Party Settings" if not fighting:
            pass
        "_@_NODISPLAY_@_   ":#Quit": # Quit is... odd.
            pass

    if skp:
        $ sophia_mood+=1
        if sophia_mood==1:
            eSophia "It's okay if you don't want a skill {i}now{/i}, but you'll need it later, or I won't allow you to continue on the tutorial."
        else:
            eSophia "Listen, if you don't get a skill later, I won't allow you to continue the tutorial!"

    menu:
        eSophia "By last, party settings allows you to manage your party. They're divided in active and inactive members. Active are the ones who will fight alongside you, and you can have up to 9. If you click on their names, except for the hero, you can switch from active to inactive.\nAlso, if you click on a skill icon, it'll remember you about what a skill does!"
        "_@_NODISPLAY_@_Char Info":
            pass
        "_@_NODISPLAY_@_Inventary": # During fights use standard useitem command
            pass
        "_@_NODISPLAY_@_Skills":
            pass
        "Party Settings" if not fighting:
            call party_screen
        "_@_NODISPLAY_@_   ":#Quit": # Quit is... odd.
            pass

    hide IC with Dissolve(0.3)
    eSophia "Also, in rare ocasions a guest may join you in a fight. That is a temporary companion marked as “Guest”. Removing them from active member list have the effect of PERMANENTLY REMOVING THEM."
    eSophia "The label will then change to \"Kick Member\", in red. As long you're not blindly clicking everywhere this will be suffice to prevent you from doing mistakes."
    eSophia "Alas, the last button is to close. That's all for the Hero Menu (or ingame menu)!"

    if tutorial_adm:
        jump TutorialAdm

label tutorial_city:
    eSophia "Cities are large, so they have a lot of context menus. We'll explore each menu and most of their submenus."
    menu:
        eSophia "This is the town basic menu. It haves a convenient character menu access button too. Click on it to continue."
        "_@_NODISPLAY_@_Travel":
            pass
        "_@_NODISPLAY_@_Explore city streets (quests, etc.)":
            pass
        "_@_NODISPLAY_@_Explore city outskirts":
            pass
        "Access character menu":
            pass

    menu:
        eSophia "The Travel button is, by far, the most important one. It will advance main story. Due consecutive fights, it's advised to stock as many healing items as you can."
        "Travel":
            pass
        "_@_NODISPLAY_@_Explore city streets (quests, etc.)":
            pass
        "_@_NODISPLAY_@_Explore city outskirts":
            pass
        "_@_NODISPLAY_@_Access character menu":
            pass

    menu:
        eSophia "City outskirts allow you to do some grinding, or to rest your whole party for free. In some rare cases, there may be other options and events, but I'm not allowed to say anything about those."
        "_@_NODISPLAY_@_Travel":
            pass
        "_@_NODISPLAY_@_Explore city streets (quests, etc.)":
            pass
        "Explore city outskirts":
            pass
        "_@_NODISPLAY_@_Access character menu":
            pass

    menu:
        eSophia "The city streets will change even more from city to city! It is the most important part of the city. Click on it so we can visit."
        "_@_NODISPLAY_@_Travel":
            pass
        "Explore city streets (quests, etc.)":
            pass
        "_@_NODISPLAY_@_Explore city outskirts":
            pass
        "_@_NODISPLAY_@_Access character menu":
            pass


    menu:
        eSophia "Usually you'll find a huge list, so let's cover them quickly. The {b}TELETRANSPORT{/b} button allows you to visit already visited cities."

        "{b}TELETRANSPORT{/b}":
            pass
        "_@_NODISPLAY_@_{b}Sleep at Inn{/b}":
            pass
        "_@_NODISPLAY_@_{image=gfx/adventure.png}The Guild Master Room":
            pass
        "_@_NODISPLAY_@_{image=gfx/map.png}Visit The Academy on city":
            pass
        "_@_NODISPLAY_@_{image=gfx/quest.png}Talk to the Guild Courrier":
            pass
        "_@_NODISPLAY_@_{image=gfx/commercial.png}Visit shops on city":
            pass
        "_@_NODISPLAY_@_Talk to no one":
            pass

    menu:
        eSophia "{b}Sleep at Inn{/b} will allow you to heal fully your whole party for some amount of money. Note that sometimes you can have a free night, if someone else reserves your party a room."

        "_@_NODISPLAY_@_{b}TELETRANSPORT{/b}":
            pass
        "{b}Sleep at Inn{/b}":
            pass
        "_@_NODISPLAY_@_{image=gfx/adventure.png}The Guild Master Room":
            pass
        "_@_NODISPLAY_@_{image=gfx/map.png}Visit The Academy on city":
            pass
        "_@_NODISPLAY_@_{image=gfx/quest.png}Talk to the Guild Courrier":
            pass
        "_@_NODISPLAY_@_{image=gfx/commercial.png}Visit shops on city":
            pass
        "_@_NODISPLAY_@_Talk to no one":
            pass

    menu:
        eSophia "{image=gfx/adventure.png}{b}Adventures{/b} are buttons with events. They usually advance main story, and most times vanishes after use.\nOn this example, you would probably have a chat with the Guild Master and update your main adventure."

        "_@_NODISPLAY_@_{b}TELETRANSPORT{/b}":
            pass
        "_@_NODISPLAY_@_{b}Sleep at Inn{/b}":
            pass
        "{image=gfx/adventure.png}The Guild Master Room":
            pass
        "_@_NODISPLAY_@_{image=gfx/map.png}Visit The Academy on city":
            pass
        "_@_NODISPLAY_@_{image=gfx/quest.png}Talk to the Guild Courrier":
            pass
        "_@_NODISPLAY_@_{image=gfx/commercial.png}Visit shops on city":
            pass
        "_@_NODISPLAY_@_Talk to no one":
            pass

    menu:
        eSophia "{image=gfx/map.png}{b}Installation{/b} buttons are things almost unique to cities, which may contain extra story or not.\nOften they introduce new features, which are usually not found anywhere but that city. Some examples are academies, forges, parks, taverns, etc. Usually they are explained the first time you enter them."

        "_@_NODISPLAY_@_{b}TELETRANSPORT{/b}":
            pass
        "_@_NODISPLAY_@_{b}Sleep at Inn{/b}":
            pass
        "_@_NODISPLAY_@_{image=gfx/adventure.png}The Guild Master Room":
            pass
        "{image=gfx/map.png}Visit The Academy on city":
            pass
        "_@_NODISPLAY_@_{image=gfx/quest.png}Talk to the Guild Courrier":
            pass
        "_@_NODISPLAY_@_{image=gfx/commercial.png}Visit shops on city":
            pass
        "_@_NODISPLAY_@_Talk to no one":
            pass

    menu:
        eSophia "{image=gfx/quest.png}{b}Quests{/b} are entirely optional. They can provide you a quick collecting task, to make grinding less painful. They may have difficulty indicated by color. Often, you can just skip them."

        "_@_NODISPLAY_@_{b}TELETRANSPORT{/b}":
            pass
        "_@_NODISPLAY_@_{b}Sleep at Inn{/b}":
            pass
        "_@_NODISPLAY_@_{image=gfx/adventure.png}The Guild Master Room":
            pass
        "_@_NODISPLAY_@_{image=gfx/map.png}Visit The Academy on city":
            pass
        "{image=gfx/quest.png}Talk to the Guild Courrier":
            pass
        "_@_NODISPLAY_@_{image=gfx/commercial.png}Visit shops on city":
            pass
        "_@_NODISPLAY_@_Talk to no one":
            pass

    menu:
        eSophia "{image=gfx/commercial.png}{b}Shops{/b} are the section of the game you should visit every new town. There you can buy new weapons, armors, healing items, and even sell monster loot if there's no quest for them or if you need a quick-spot cash."

        "_@_NODISPLAY_@_{b}TELETRANSPORT{/b}":
            pass
        "_@_NODISPLAY_@_{b}Sleep at Inn{/b}":
            pass
        "_@_NODISPLAY_@_{image=gfx/adventure.png}The Guild Master Room":
            pass
        "_@_NODISPLAY_@_{image=gfx/map.png}Visit The Academy on city":
            pass
        "_@_NODISPLAY_@_{image=gfx/quest.png}Talk to the Guild Courrier":
            pass
        "{image=gfx/commercial.png}Visit shops on city":
            pass
        "_@_NODISPLAY_@_Talk to no one":
            pass

    menu:
        eSophia "And that's all for a city! Of course, extra buttons may appear or disappear, so watch out for each city individualities!"

        "_@_NODISPLAY_@_{b}TELETRANSPORT{/b}":
            pass
        "_@_NODISPLAY_@_{b}Sleep at Inn{/b}":
            pass
        "_@_NODISPLAY_@_{image=gfx/adventure.png}The Guild Master Room":
            pass
        "_@_NODISPLAY_@_{image=gfx/map.png}Visit The Academy on city":
            pass
        "_@_NODISPLAY_@_{image=gfx/quest.png}Talk to the Guild Courrier":
            pass
        "_@_NODISPLAY_@_{image=gfx/commercial.png}Visit shops on city":
            pass
        "Talk to no one":
            pass

    if tutorial_adm:
        jump TutorialAdm

label tutorial_battle:
    #eSophia "I'm tired... Someone please give me a cake... Oh no... Maybe I ate too much..."
    eSophia "Let me set up this fight."
    call setup_weapons
    call setup_mobs
    call setup_mobitem
    call setup_mobmob
    call setup_journal
    eSophia "Battles are tricky. I'll join you so we can do a quick tutorial."
    $ sophia.ordem=""
    call add_ally(sophia)
    $ EnemyParty=[tutorial_mob]
    $ fght_act=hero
    $ fighting=True
    $ autofight=False
    $ show_enbar=True
    $ rounda=1
    $ xp_rl=400
    $ col_rl=1800
    $ item_rl=1
    $ item_list="Sword Of Magic +51"
    # Set background
    $ renpy.show("battlebg generic")
    $ renpy.with_statement(dissolve)

    eSophia "We'll fight against a single, uhm, dummy. Yes, very dangerous."
    $ atk_on(hero)
    eSophia "The first turn is almost always yours." # TODO: maybe a CO to give enemy first turn get implemented later

    menu:
        eSophia "Uhm, it only have 5 HP. Maybe you can finish it on a swift attack. Attack!"
        #"Round [rounda]\n\nIt's [hero.name]'s turn!!\nPlease take an action."
        "Attack":
            eSophia "Click on the monster to attack. There is an abort button on the bottom-right of the screen, don't click that, click on the monster."
            window hide
            $ set_combat_target()
            $ target=ui.interact()
            window show
            if target=="abort": # Abort button
                $target=Enemy("the abort button", 0)
            if target != tutorial_mob:
                $sophia_mood+=1
            $ renpy.block_rollback()
            "You miss [target.name!t]!"
            pass
        "_@_NODISPLAY_@_Use Skill":
            pass
        "_@_NODISPLAY_@_Use Item":
            pass
        "_@_NODISPLAY_@_Flee":
            pass
        "_@_NODISPLAY_@_Wait":
            pass

    $ atk_off()
    eSophia "Oh well. That can happen, depending on your weapon acc. The base value is 70%%, so if you miss, I guess you're out of luck."
    eSophia "Also note you cannot rollback from a battle result. Pressing PageUp will not allow you to revert your miss."
    $ atk_on(sophia)
    $ fght_act=sophia
    eSophia "Now it's my turn!"
    show UpArrow as HPMASTER with dissolve:
        xpos 0.4 ypos 0.15
        rotate -90.00
    eSophia "Check again how this interface changed. You can only change your party during your own turn, and you cannot access the whole menu during a fight. Also, there's a button called \"auto\". I've disabled it for this fight, but clicking on it will make any character after currently active one attack."
    eSophia "Meaning, if during your turn you clicked \"auto\", your character would not automatically attack, but I would. Well, don't worry. I've disabled this button, anyway."
    eSophia "Note as well that the basic information displayed is about me, and not about you. This is for easier fight management."
    hide HPMASTER with dissolve
    $ sophia.frozen+=1
    eSophia "Just for tutorial purposes, I'm now paralyzed. You can notice the icon which just showed up."
    menu:
        eSophia "...It means I can only wait."
        #"Round [rounda]\n\nIt's [hero.name]'s turn!!\nPlease take an action."
        "_@_NODISPLAY_@_Attack" if False:
            pass
        "_@_NODISPLAY_@_Use Skill" if False:
            pass
        "_@_NODISPLAY_@_Use Item" if False:
            pass
        "Wait":
            e "[sophia.name!t] is paralyzed."
            $sophia.frozen-=1
    $ atk_off()
    eSophia "Oh well. I shouldn't have paralyzed myself, because..."
    $ atk_on(tutorial_mob)
    "Round [rounda]\n\nIt's enemy turn!"
    eSophia "Brace yourself for its attack!"
    $hit_player_verbose(100)
    "[tutorial_mob.name!t] causes 100 damage to [yname], and is now with autorevive!"
    $ atk_off()
    $ atk_on(hero)
    eSophia "It's your turn again. BURN THIS FOE DOWN WITH SOME SKILL!"
    $ done=False
    menu:
        eSophia "Note that you can click on the skill icon for a detailed explanation about it."
        "_@_NODISPLAY_@_Attack":
            pass
        "Use Skill":
            $ done=True
            call select_skill(hero)
            pass
        "_@_NODISPLAY_@_Use Item":
            pass
        "_@_NODISPLAY_@_Flee":
            pass
        "_@_NODISPLAY_@_Wait":
            pass
    if tutorial_mob.hp < 0:
        $ tutorial_mob.hp+=-(tutorial_mob.hp)
    if tutorial_mob.hp == 0:
        $ tutorial_mob.hp+=3
        "Autorevive effects unleashed!"
    if not done:
        $ sophia_mood += 1
        if sophia_mood < 5000 and sophia_mood > 2:
            eSophia "...I hate you. VERY WELL, I WON'T TEACH YOU SKILLS ANYMORE."
        else:
            eSophia "*sigh* I give up. You'll figure out how to use skills in combat. I hope."
            if not skp:
                eSophia "By the way, I hope you had skills and a class... You know, developer might have been lazy when designing this part, and didn't checked if you've did other parts from tutorial before."

    $ atk_off()
    $ atk_on(sophia)
    $ hero.items.append(MinorPot)
    menu:
        eSophia "You're hurt. Let's use a healing item. Select the potion from the list, and then click on yourself."
        #"Round [rounda]\n\nIt's [hero.name]'s turn!!\nPlease take an action."
        "_@_NODISPLAY_@_Attack":
            pass
        "_@_NODISPLAY_@_Use Skill":
            pass
        "Use Item":
            $ done=True
            call use_item
        "_@_NODISPLAY_@_Wait":
            pass
    if not done:
        $ sophia_mood+=1
        eSophia "Okay, save that item. You're not allowed to take anything outside this area, anyway."
    elif hero.hp < 200:
        $ sophia_mood+=1
        eSophia "...Why are you still hurt? I hope you didn't tried to heal me."

    $ atk_off()
    $ atk_on(tutorial_mob)

    "Round [rounda]\n\nIt's enemy turn!"
    $hit_player_verbose(hero.hp)
    $ hero.hp=1
    $hit_someone_verbose(sophia, sophia.hp)
    $ sophia.hp=1
    "[tutorial_mob.name!t] causes absurd damage to everyone, and sets autorevive!"

    $ atk_off()
    $ atk_on(hero)

    menu:
        eSophia "It's hopeless. We should try to flee, we have 70%% chance and succeeding at that. This monster keeps casting autorevive. I'm pretty sure it have some awesome, rare drop or something."
        "_@_NODISPLAY_@_Attack":
            pass
        "_@_NODISPLAY_@_Use Skill":
            pass
        "_@_NODISPLAY_@_Use Item":
            pass
        "Flee":
            e "You've flew sucefully!"
        "_@_NODISPLAY_@_Wait":
            pass

    $ autofight=False
    $ atk_off() # Doublesure
    $ EnemyParty=[] # Clear enemies list
    $ fighting=False
    $ p=" "
    window hide
    # Display battle results
    $ ui.window(background=Frame("gfx/blackwindow.png", 6, 6), xalign=.5, yalign=.5, ymaximum=.5, xmaximum=.5)
    $ ui.vbox(xalign=.5)
    $ ui.text("{i}Results{/i}", xalign=.5)
    $ ui.text("XP:[p][p][p][p][p][p][p][p][p][p][p][p][xp_rl]")
    $ ui.text("Col:[p][p][p][p][p][p][p][p][p][p][p][col_rl]")
    $ ui.text("Items:[p][p][p][p][p][p][p][item_rl]")
    $ ui.text("___________________")
    if item_rl > 0:
        $ ui.hbox()

        $ jrncslvp=ui.viewport(xfill=False, mousewheel=True, draggable=False) # what's with the silly name
        $ ui.text("[item_list]")
        $ ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar', #left_bar="#888", right_bar="#888", xmaximum=4,
        thumb=theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar_thumb.png", "#FFF"),
        left_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12),
        right_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12)
        )
        $ ui.close()

    $ ui.close()
    pause
    window show

    $ show_enbar=False
    $ renpy.hide("battlebg") # combat_bg
    $ renpy.with_statement(dissolve)

    eSophia "...Now, of course these results are a joke. Fleeing will forsake all gained EXP, Credits and Items."
    eSophia "This concludes our battle tutorial."

    if tutorial_adm:
        jump TutorialAdm

label tutorial_finish:
    if not sophia_mood:
        eSophia "Thanks for bearing me 'till the end. May the game start!"
    else:
        eSophia "May the game start!"

    "Chief Developer" "Note for myself: Add a quick tutorial, with print from the menu screens and descriptive menu boxes..."

label tutorial_end:
    $ load_factor=0
    stop music fadeout 5.0
    scene black with dissolve
    jump dev_beta_setup
