########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2015  Jesusalva
#     Copyright (C) 2017  Michel Glenn

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Configures handling of UnreleasedFeature and UnapprovedFeature.
# 2017-11-13: This is not necessary because it was moved to cheat codes
init python:
    if persistent.premium is None:
        persistent.premium=False
        #raise NotImplementedError("Unreleased content is handled by persistent.premium")

    try:
        import subprocess
        whoami=subprocess.check_output('logname')
        if 'jesusalva' in whoami:
            config.developer=True
    except:
        pass

