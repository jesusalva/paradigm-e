########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# There's no AI in Project-E. Sorry for the misleading title.
# ai.rpy selects the target for enemies, so it can be configured as necessary.
# ai.rpy also defines criteria for selecting skills.
# 
# It takes two params: aispec defines the default behavior. aieffc defines the
# AI behavior ineffience, for randomness mostly. Therefore it may have selected
# the default target, but may have a struck of inefficence and override that
# target - that is a little left to luck, of course. It DOES floats based on how
# you arranged the party. For most cases, members in the front-line may suffer
# most based from inefficence. Please note some modes are immune to inefficience
# like default and avoid_hero.
#
# NOTE: If you remove this from an IF loop, and make some flags cumulative
# (eg. avoid_hero + strongest) it may start to look like a REAL intelligence.

init python:
    def ai_targetting(WhoAmI):
        # WEAKEST - target whoever is with less HP in party (for efficient death)
        if WhoAmI.aispec == "WEAKEST":
          WhoIllKillToday=hero
          for hitme in battling:
           if hitme.hp > 0 and ((hitme.hp < WhoIllKillToday.hp) or (renpy.random.random() < WhoAmI.aieffc)):
            WhoIllKillToday=hitme

        # THREAT - attack whoever has highest Damage Power.
        elif WhoAmI.aispec == "THREAT":
          WhoIllKillToday=hero
          DmgPower=float((hero.weapon.mindmg+hero.weapon.mindmg)*hero.weapon.acc)
          for hitme in battling:
           DmgPower2=float((hitme.weapon.mindmg+hitme.weapon.mindmg)*hitme.weapon.acc)
           if hitme.hp > 0 and ((DmgPower < DmgPower2) or (renpy.random.random() < WhoAmI.aieffc)):
            DmgPower=float((hitme.weapon.mindmg+hitme.weapon.mindmg)*hitme.weapon.acc)
            WhoIllKillToday=hitme

        # STRONGEST - target whoever is with most HP in party (support role)
        # TODO: Won't almost always target hero then?
        elif WhoAmI.aispec == "STRONGEST":
          WhoIllKillToday=hero
          for hitme in battling:
           if hitme.hp > 0 and ((hitme.hp > WhoIllKillToday.hp) or (renpy.random.random() < WhoAmI.aieffc)):
            WhoIllKillToday=hitme

        # AIM_HERO - almost always target hero. (Most dangerous)
        elif WhoAmI.aispec == "AIM_HERO":
          WhoIllKillToday=hero
          for hitme in battling:
           if hitme.hp > 0 and renpy.random.random() < WhoAmI.aieffc:
            WhoIllKillToday=hitme

        # AVOID_HERO - avoids attacking hero (savours the best by last)
        elif WhoAmI.aispec == "AVOID_HERO":
          WhoIllKillToday=[]
          for hitme in battling:
           if hitme.hp > 0 and ((hitme != hero) or (renpy.random.random() < WhoAmI.aieffc)):
            WhoIllKillToday.append(hitme)
          if len(WhoIllKillToday) == 0:
            WhoIllKillToday=hero
          else:
            WhoIllKillToday=renpy.random.choice(WhoIllKillToday)

        # TYMER - attack whoever has highest Power (weapon and magics), Consider HP, MP and how much defense the enemy haves (buffs too)
        elif WhoAmI.aispec == "TYMER":
          WhoIllKillToday=hero
          DmgPower=float((hero.weapon.mindmg+hero.weapon.mindmg)*hero.weapon.acc)
          for hitme in battling:
           DmgPower2=float((hitme.weapon.mindmg+hitme.weapon.mindmg)*hitme.weapon.acc)
           if hitme.add_dmg_dur > 0:
            DmgPower2*=1.0+hitme.add_dmg
           DmgPower3=0.0
           for e in hitme.skill: # This may result in always targeting hero!!
            if e.type in ['H', 'A', 'E']: # Using array as power level - but is it fair to equiparate AoE with normal skills?
                DmgPower3+=float((e.array[0]+e.array[1])/2.0)
                #print "HAE: %.2f" % (float((e.array[0]+e.array[1])/2.0))
            elif e.type == 'R': # Use array as power level but is more dangerous - take this in consideration
                DmgPower3+=float((e.array[0]+e.array[1])/2.0)*1.1
                DmgPower3+=5.0
            elif e.type == 'B': # Buff
                DmgPower3+=float(e.float*200)
                #print "Buff: %.2f" % (float(e.float*200))
            elif e.type == 'MB': # Multi-Buff
                DmgPower3+=float(e.float*(197+len(battling)*3))
            else: # Unknown skill!!!
                DmgPower3+=50.0
                print "WARNING: Uknown Skill? Name: %s (%s)" % (str(e.name), str(e.type))
           DmgPower3*=float(hitme.mp/(hitme.maxmp+0.1))
           DmgPower3*=0.75 # Revalue DP3 or we could have big values (eg. 222)
           DmgPower2+=DmgPower3
           # Weaker = better
           DmgPower4=1.1-float(hitme.hp/(hitme.maxhp+0.1))
           DmgPower4*=DmgPower2
           DmgPower4-=hitme.armor.abdg*hitme.armor.rate
           DmgPower4-=hitme.offhand.abdg*hitme.offhand.rate
           if hitme.add_def_dur > 0:
            DmgPower4*=-(1.0+hitme.add_def_rt)
           # Just dump DP4 value. It's a small change, I think DP3 is being overestimated
           DmgPower2+=DmgPower4
           if hitme.hp > 0 and ((DmgPower < DmgPower2) or (renpy.random.random() < WhoAmI.aieffc)):
            DmgPower=0.0+DmgPower2
            WhoIllKillToday=hitme
           if config.developer:
            print "1Tymer Aggro Ponderation for: " + str(hitme.name)
            print "SK: %.2f | HPDEF: %.2f | FINAL: %.2f || Record: %.2f" % (DmgPower3, DmgPower4, DmgPower2, DmgPower)
          if config.developer:
            print "1Tymer will attack: " + str(WhoIllKillToday.name)

        else:
          WhoIllKillToday=[]
          WhoIllKillToday.append(hero)
          for hitme in battling:
           if hitme.hp > 0:
            WhoIllKillToday.append(hitme)
          WhoIllKillToday=renpy.random.choice(WhoIllKillToday)

        return WhoIllKillToday













    # Declares how AI shall choice their skills when casting
    def ai_skillcasting(WhoAmI):
        global WhoIllKillToday

        AiSkList=[]
        for i in WhoAmI.skill:
            AiSkList.append(i)

        # Summoner procedures
        for a in WhoAmI.aiskpec:
            if "SUMMONER" in a:

                # Do not summon if enemy party is full
                if len(EnemyParty) >= 9:
                    for i in AiSkList:
                        if i.type == "SUMMON":
                            AiSkList.remove(i)
                else:
                    # Always summon if no summons are alive
                    ai_skcast_epsummon=False
                    for i in EnemyParty:
                        if i.hp > 0 and "Summoned" in i.name:
                            ai_skcast_epsummon=True
                            break
                    if not ai_skcast_epsummon:
                        for i in AiSkList:
                            if i.type != "SUMMON":
                                AiSkList.remove(i)

            # Healer procedures, overrides variable (WhoIllKillToday);
            # Note HEALER is followed by a number, which is the % of hp to
            # heal!
            # TODO: DO NOT use HEALER with only heal skills, it'll crash!
            if "HEALER" in a:
                ai_skcast_epheal=[]

                for i in EnemyParty:
                    if config.developer:
                        print "%s matrix: %d under %d or %.2f <= %.2f" % (i.name, i.hp, i.maxhp, (i.hp/(i.maxhp+0.0)), (int(a.replace('HEALER_', ''))/100.0) )
                    if (i.hp/(i.maxhp+0.0)) <= (int(a.replace('HEALER_', ''))/100.0 ):
                        ai_skcast_epheal.append(i)

                # None on healing criteria? Exclude this skill.
                if len(ai_skcast_epheal) == 0:
                    for i in AiSkList:
                        if i.type == "H":
                            AiSkList.remove(i)

        AiSkill=renpy.random.choice(AiSkList)

        if "USE_MANA" in WhoAmI.aiskpec:
            WhoAmI.mp-=AiSkill.cost

        # Must change target to friendly? (TODO: Possibly broken)
        if AiSkill.type == "H":
            WhoIllKillToday=renpy.random.choice(ai_skcast_epheal)
            if config.developer:
                print "Healing pattern active: choosen %s within %d options" % (str(WhoIllKillToday.name), len(ai_skcast_epheal))

        return AiSkill

