########################################################################################
#     This file is part of Castle.
#     Copyright (C) 2015  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# This file holds all major definitions to Castle and Project E.
# Changing those variables will affect the balance, beware!

# Images are divided in the following classes:
# battlebg - Battle backgrounds for use with $ combat_bg
# dungeon  - Prefix which some dungeons use, THIS CODE NEEDS TO BE CHECKED (TODO)
# shop     - same as stated above
image battlebg generic  = Frame("gfx/bg/generic.png",0,0)
image battlebg woods    = Frame("gfx/bg/woods_battle.jpg",0,0)
image battlebg sea      = Frame("gfx/bg/arctis_top.jpg",0,0)
image battlebg greenhill= Frame("gfx/bg/Greenhills.jpg",0,0)
image battlebg darkland = Frame("gfx/bg/darkland-battle.png",0,0)
image battlebg village  = Frame("gfx/bg/farmland_night.jpg",0,0)
image battlebg cave     = Frame("gfx/bg/cave_battle.jpg",0,0)
image battlebg swamp    = Frame("gfx/bg/swamp_n.png",0,0)

image Hits Final = Frame("gfx/HIT.png",0,0)
image IC default = Frame("gfx/bg/background_full.jpg",0,0)

image white = "#FFF"

# Initialize required data
init -1:
    # Class defs
    python:
        C_NONE=0
        C_MAGE=1
        C_WARR=2
        C_TANK=3
        C_RANG=4

    if persistent.lost is None:
        $ persistent.lost = False
    if persistent.won is None:
        $ persistent.won=False
    if persistent.MCoin is None:
        $ persistent.MCoin = 0
    if persistent.ccodes is None:
        $ persistent.ccodes = []
    $ playing=False
    $ fighting=False
    $ just_won_boss_fight=False

    python:
        # This balances how much extra HP/MP you get per levelup
        HP_PER_LEVEL=150
        MP_PER_LEVEL=10
        # Rather useless, attack animation
        ATTCK_ID=""
        ATTCK_ST=False
        # Next env variable
        #ENV_V3=False

init:
    $ prologue=False

    python:
        try:
            GUIOpacity+=0
        except:
            GUIOpacity=85#105

    if persistent.battleShortcut is None:
        $ persistent.battleShortcut=False
    if persistent.Always100 is None:
        $ persistent.Always100=False

label prestart:

    $ classe=C_NONE
    $ dungeon_state=0                                           # 0: You're not on dungeons.
    $ quick_skip_dg=0                                           # 0: Quick skip for levels below (including) 0.
    $ took_quest=[]                                             # Which quests were taken?
    $ claimed_codes=[]                                          # Which codes were already claimed?
    $ canflee=True                                              # Can flee?
    $ combat_bg="generic"                                       # Background for combat

    $ show_hpbar=False
    $ show_enbar=False

    $ g_maxbt=5                                                 # Maximum size for party (recomended: 5~9)
    $ ymale=True
    $ yname="Challenger"
    $ HClass="Dummy"
    $ hero=Enemy(_("Challenger"), 200)
    $ battling=[hero]                                           # Which members are active, up to 4 or 5?
    $ party=[hero]                                              # Which members are on your party? (Arranjable)

    $ playing=True
    $ fighting=False
    $ managelock=False                                          # Lock some commands.
    $ fght_act=Enemy(_("BUG, REPORT ME!"), 0)
    $ gp=1000                                                   # TODO: gp: Amount of Col (or Credits) that you have.
    $ save_name=_("Setup Countryard\n")

    # TODO IMPORTANT: Global floor level specifications.

    $ g_max_level=5                                             # TODO: g_* = global variable (cannot override)
    $ g_cur_level=0                                             # TODO: You start outside the castle.
    $ g_act_level=1                                             # TODO: g_act_level: Just how far did you went?
    $ g_chapter=1                                               # If we need to restruct things

    $ g_story_lbl=False                                         # Change this to a string to control a jump!
    $ gstory_ctrl=[]                                            # Holds Story Control. Holds directives for toogables.

    # IMPORTANT: finished

    $ ctrl_lgc=[]                                               # All evolved characters
    $ skp=0                                                     # Available Skill Points.
    $ regen_hprate=1
    $ forge_lvl=0                                               # Higher levels = Better forging stats and chances
    $ forged_names=[]                                           # List of already used item names on forge
    $ ql=Journal()                                              # Initialize quest log

    $ gdf_mult=1.0                                               # Global Difficulty Multiplier
    $ ThisEnemy=Enemy("The Bug", 1000000, 51, bar=1)
    $ EnemyParty=[]

    return


###################################################
# gstory_ctrl scripting guide
#
#
# This special array contains all control variables which allow to change the
# flux of the game. Using this variable to hold specially-formated data, you're
# capable to interfer on the main engine without having to touch directly
# mechanics.rpy, combat.rpy, or any other unreadable script files done by me!
#
# Here is a quick scripting guide to make life easier when using them.
#
# Any string can be stored within this array, and then be used as a variable.
# A normal story variable. We will use <txt1> marker to represent a text valor.
# 
# INN
#       makes inn price zero. Cannot have any argument after or before it.
# INN_<txt1>
#       After sleeping at inn, causes game control to switch
#       to label <txt1>, using g_story_lbl variable.
# RMINN
#       Remove this and INN flag after sleeping. Can also be used
#       as RMINN_<txt1> for convenience.
#
# T_<txt1>
#       Functionally useless, it's meant to have a hardcoded action attributed on
#       story.rpy, at label scene_ctrl_tavern.
# bs_<txt1>
#       Different from T_, the bs_ will provoke a jump at label scene_ctrl_tavern
#       being moderated by the one-per-visit-or-drink rule.
# CASTLE / TAVERN
#       Also functionally useless. It's used to unlock a button to travel to castle/tavern,
#       but this is essencially a normal text variable. You can use it by programming
#       beta.rpy - just like any variable. Yes. As I said. It's a plain text value.
#
# UponDefeat_<txt1>
#       Override default "call end_beta ; return", and forces control to be
#       switched to label <txt1> after game over. See script.rpy for details.
#       You can also have a 'call end_beta' at top of <txt1> label to make credit
#       roll even before defeat.
#
# COMBAT_OVERRIDE
#       Enable combat override commands usage. This variable exist to spare some
#       processing power, though. MUST BE AN EXACT TEXT.
# COR_<txt1>_@_<txt2>
#       Standard 'Combat Override Round' command. On Round <txt1>, will call label
#       <txt2>. This variable will be DELETED afterwards.
# COD_<txt1>_@_<txt2>
#       Standard 'Combat Override OnDeath' command. When <txt1> (name) dies,
#       <txt2> label will be called. This often is a blank label with "jump defeat".
#       Please note this is only processed when the turn begins and before COR.
#       We should use renpy.pop_call() if we want to jump, instead of calling.
# COM_<txt1>_@_<txt2>
#       Standard 'Combat Override OnMadness' command. When <txt1>'s hp drop enough
#       to use last hp bar, label <txt2> will be called. Such label can change its
#       aispecs, buff the enemy attack (MAD), and even change its sprite.
# COF_<txt1>_@_<txt2>
#       Standard 'Combat Override OnFrozen' command. When <txt1> is frozen, label
#       <txt2> will be called. Unlike most Combat Overrides, this one is processed
#       after and before enemy attack, and is NOT automatically removed.
