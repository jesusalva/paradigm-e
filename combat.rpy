########################################################################################
#     This file is part of Castle.
#     Copyright (C) 2015  Jesusalva
#     This file is part of 2017 Journal.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# THIS IS A BRANCH FROM MECHANICS.RPY
# COMBAT LABELS AND FUNCTIONS

init python:
 def cooldown(en):
    if en.frozen > 0:
        en.frozen-=1
    if en.silence > 0:
        en.silence-=1
    if en.hidden > 0:
        en.hidden-=1
    if en.poison > 0:
        en.hp-=int(en.maxhp/20) # 5% damage
        en.poison-=1

 def recalculate_damage(en, dmg, caster="@AUTOGEN", complete=True):
    # Function for calculating armor defense and avoid damage.

    dmg_T=0+dmg # double-safe against alias

    dmgabs_CL=0.0 # Damage absorbed by clotches
    dmgabs_OH=0.0 # Damage absorbed by off hand.
    dmgabs_PB=0.0 # Damage absorbed by player buffs.
    dmginc   =0.0 # Damage INCREASED for whatever reasons

    # Double-safe
    if caster == "@AUTOGEN":
        caster=Enemy('no bonus', 0)

    # FIRST OF ALL, we calculate any DAMAGE INCREMENT. They're applied first, according to rules
    if caster.add_dmg_dur > 0:
        dmginc+=dmg_T*caster.add_dmg

        # We're done calculing buffed damage. (NOTE: It can be negative too :>)
        dmg_T+=int(dmginc)
        if caster.add_dmg_dur > 1:
            tmp_decr=float(caster.add_dmg/caster.add_dmg_dur) # It should float automagically but just double sure
            caster.add_dmg-=tmp_decr
        else:
            caster.add_dmg-=caster.add_dmg
        caster.add_dmg_dur-=1


    # Let's start calculing buff defense. Not affected by skills.
    if en.add_def_dur > 0:
        if en.add_def_rt >= 1: # Correct absurd values (so you don't regen)
            en.add_def_rt=0.99
        dmgabs_PB+=dmg_T*en.add_def_rt

        # We're done calculing buffed defense. (NOTE: It can be negative too :>)
        dmg_T-=int(dmgabs_PB)
        if en.add_def_dur > 1:
            tmp_decr=float(en.add_def_rt/en.add_def_dur) # It should float automagically but just double sure
            en.add_def_rt-=tmp_decr
        else:
            en.add_def_rt-=en.add_def_rt
        en.add_def_dur-=1


    # Now we calc shield defense. Affected by vit.
    if en.offhand.type=="armor":
            dmgabs_OH=((en.offhand.rate*en.vit)*dmg_T) # Your defense * Yor vit = decrease damage factor
            if dmgabs_OH>en.offhand.abdg:
                dmgabs_OH=en.offhand.abdg # Maximum cap for shield must be respected

    # We're done calculing shield defense.
    dmg_T-=int(dmgabs_OH)



    # Let's finish with the sure block: Clotching. Affected by vit.
    i=0
    while i < dmg_T:
            i+=1
            dmgabs_CL+=(en.armor.rate*en.vit)
            if dmgabs_CL>=en.armor.abdg:
                break;

    # We're done calculing armor defense.
    dmg_T-=int(dmgabs_CL)


    if persistent.premium or config.developer:
        print ("%d BONUS | %d BUFF | %d SHIELD | %d ARMOR" % ( dmginc, dmgabs_PB, dmgabs_OH, dmgabs_CL ) )
        print ("%s hit: %d damage (%d absorbed)" % (en.name, dmg_T, (dmg-dmg_T)) )

    # If not complete, we only care for Buffs (that is, magic skills)
    if not complete:
        return int(dmg+dmginc-dmgabs_PB)

    return dmg_T



 def calc_weapon_skill(en, wpn, caster):
    tmp_cws_effects=[]
    # Handles Paralyze & Self-Healing (same RNG, affected by luck)
    if renpy.random.random() < (wpn.skpc*caster.luk):
        if "paralyze" in wpn.skcm:
          for char in wpn.skcm: # Each I is a mark.
           if char == "I":
            en.frozen+=1

        if "heal" in wpn.skcm:
          for char in wpn.skcm: # Each I is a mark.
           if char == "I":
            caster.hp+=4
          if caster.hp>caster.maxhp:
            caster.hp=caster.maxhp

    # Handles poisons
    if "poison" in wpn.skcm:
        if  renpy.random.random() <= (wpn.skpc):
            for char in wpn.skcm: # Each I is a mark.
                if char == "I":
                    en.poison+=1
            tmp_cws_effects.append(_("poison"))

    # Handles silence
    if "silence" in wpn.skcm:
        if  renpy.random.random() <= (wpn.skpc):
            for char in wpn.skcm: # Each I is a mark.
                if char == "I":
                    en.silence+=1
            tmp_cws_effects.append(_("silence"))

    # Handles drain
    if "drain" in wpn.skcm:
        if renpy.random.random() <= (wpn.skpc):
            for char in wpn.skcm: # Each I is a mark.
                if char == "I":
                    caster.hp+=int(dmg/10)
            if caster.hp>caster.maxhp:
              caster.hp=caster.maxhp

    # Handles INSTAKILL
    if "instakill" in wpn.skcm:
        if  renpy.random.random() <= (wpn.skpc) and (en.bar <= 1): # Do not put along other state effects
            en.hp=0
            tmp_cws_effects.append(_("instakill"))

    cws_char=","
    return #cws_char.join(tmp_cws_effects)

 def winners_losers():
    tmp_enhp=0
    tmp_alhp=0
    tmp_ax=0

    for mob in EnemyParty:
        tmp_ax=mob.hp
        if tmp_ax >= 0:
            tmp_enhp+=mob.hp

    for mob in battling:
        tmp_ax=mob.hp
        if tmp_ax >= 0:
            tmp_alhp+=mob.hp

    if tmp_alhp <= 0:
        return "losers"
    elif tmp_enhp <= 0:
        return "winners"
    else:
        return "-"

 def set_combat_target():
    global ThisEnemy
    ui.fixed()

    # Foes Target
    tmp_xpos=700
    tmp_ypos=150
    tmp_crowd=0
    for ThisEnemy in EnemyParty:
        if ThisEnemy.hp > 0 and not ThisEnemy.hidden:
            ui.imagebutton("gfx/target.png", "gfx/target2.png", clicked=ui.returns(ThisEnemy), xpos=tmp_xpos, ypos=tmp_ypos)
        tmp_ypos+=60
        tmp_crowd+=1
        if tmp_crowd >= 5:
            tmp_crowd-=5
            tmp_xpos-=50
            tmp_ypos-=280 # off-set is 300

    # Friends Target
    tmp_xpos=20
    tmp_ypos=150
    tmp_crowd=0
    for ally in battling:
        ui.imagebutton("gfx/target_none.png", "gfx/target2.png", clicked=ui.returns(ally), xpos=tmp_xpos, ypos=tmp_ypos)
        tmp_crowd+=1
        tmp_ypos+=60
        if tmp_crowd >= 5:
            tmp_crowd-=5
            tmp_xpos+=50
            tmp_ypos-=280 # off-set is 300

    ui.imagebutton("gfx/abort.png", "gfx/abort2.png", clicked=ui.returns('abort'), xalign=0.98, yalign=0.98)
    ui.close()

 def render_prototype():
    global battling, ThisEnemy, ATTCK_ST, ATTCK_ID

    # Friends Definition
    tmp_xpos=20
    tmp_ypos=150
    tmp_crowd=0
    # Render friends
    for ally in battling:
        ui.text(ally.name, size=7, xpos=tmp_xpos, ypos=tmp_ypos-13)

        # HP bar
        tmp_barsize=32+(ally.maxhp/25)
        if tmp_barsize>72:
            tmp_barsize=72
        ui.bar(ally.maxhp, ally.hp, xmaximum=tmp_barsize, ymaximum=3, xpos=tmp_xpos, ypos=tmp_ypos-6, left_bar="#0F0", right_bar="#333")
        ui.bar(ally.maxmp, ally.mp, xmaximum=tmp_barsize, ymaximum=3, xpos=tmp_xpos, ypos=tmp_ypos-3, left_bar="#00F", right_bar="#333")
        # Effect box (displays only if alive)
        if ally.hp > 0:
            ui.hbox(xpos=tmp_xpos, ypos=tmp_ypos)
            if ally.add_def_dur:
                if ally.add_def_rt > 0:
                    ui.image("gfx/defup.png")
                else:
                    ui.image("gfx/defdwn.png")
                ui.text(str(int(ally.add_def_rt*100)) + "%", size=8)
            if ally.add_dmg_dur:
                if ally.add_dmg > 0:
                    ui.image("gfx/atkup.png")
                else:
                    ui.image("gfx/atkdwn.png")
                ui.text(str(int(ally.add_dmg*100)) + "%", size=8)
            if ally.frozen:
                ui.image("gfx/paralyzed.png")
                ui.text(str(int(ally.frozen)), size=8)
            if ally.silence:
                ui.image("gfx/silence.png")
                ui.text(str(int(ally.silence)), size=8)
            if ally.poison:
                ui.image("gfx/poisoned.png")
                ui.text(str(int(ally.poison)), size=8)
            ui.close()

            # Rendering
            if ATTCK_ST and ATTCK_ID == ally.name:
                ui.image("gfx/enemies/"+str(ally.pic)+"-attack.png", xpos=tmp_xpos, ypos=tmp_ypos)
            else:
                ui.image("gfx/enemies/"+str(ally.pic)+".png", xpos=tmp_xpos, ypos=tmp_ypos)
        else:
            ui.image("gfx/enemies/dead.png", xpos=tmp_xpos, ypos=tmp_ypos)
        tmp_crowd+=1
        tmp_ypos+=60
        if tmp_crowd >= 5:
            tmp_crowd-=5
            tmp_xpos+=50
            tmp_ypos-=280 # off-set is 300

    # Foes Definition
    tmp_xpos=700
    tmp_ypos=150
    tmp_crowd=0
    # Render foes
    for ThisEnemy in EnemyParty:
        ui.text(ThisEnemy.name, size=7, xpos=tmp_xpos, ypos=tmp_ypos-12)

        # HP bar
        tmp_barsize=32+(ThisEnemy.maxhp/25)
        if tmp_barsize>72:
            tmp_barsize=72
        ui.bar(ThisEnemy.maxhp, ThisEnemy.hp, xmaximum=tmp_barsize, ymaximum=5, xpos=tmp_xpos, ypos=tmp_ypos-5, left_bar="#F00", right_bar="#333")
        # Effect box (displays only if alive)
        if ThisEnemy.hp > 0:
            ui.hbox(xpos=tmp_xpos, ypos=tmp_ypos)
            if ThisEnemy.add_def_dur:
                if ThisEnemy.add_def_rt > 0:
                    ui.image("gfx/defup.png")
                else:
                    ui.image("gfx/defdwn.png")
                ui.text(str(int(ThisEnemy.add_def_rt*100)) + "%", size=8)
            if ThisEnemy.add_dmg_dur:
                if ThisEnemy.add_dmg > 0:
                    ui.image("gfx/atkup.png")
                else:
                    ui.image("gfx/atkdwn.png")
                ui.text(str(int(ThisEnemy.add_dmg*100)) + "%", size=8)
            if ThisEnemy.hidden:
                ui.image("gfx/hidden.png")
                ui.text(str(int(ThisEnemy.hidden)), size=8)
            if ThisEnemy.frozen:
                ui.image("gfx/paralyzed.png")
                ui.text(str(int(ThisEnemy.frozen)), size=8)
            if ThisEnemy.poison:
                ui.image("gfx/poisoned.png")
                ui.text(str(int(ThisEnemy.poison)), size=8)
            if ThisEnemy.silence:
                ui.image("gfx/silence.png")
                ui.text(str(int(ThisEnemy.silence)), size=8)
            ui.close()

            # Rendering
            if ATTCK_ST and ATTCK_ID == ThisEnemy.name:
                ui.image("gfx/enemies/"+str(ThisEnemy.pic)+"-attack.png", xpos=tmp_xpos, ypos=tmp_ypos)
            else:
                ui.image("gfx/enemies/"+str(ThisEnemy.pic)+".png", xpos=tmp_xpos, ypos=tmp_ypos)
        else:
            ui.image("gfx/enemies/dead.png", xpos=tmp_xpos, ypos=tmp_ypos)

        tmp_ypos+=60
        tmp_crowd+=1
        if tmp_crowd >= 5:
            tmp_crowd-=5
            tmp_xpos-=50
            tmp_ypos-=280 # off-set is 300


 # This function was disabled!! New skill system was demanded.
 #def cooldown_skills(ename="all", rate=1, pe=True):

# Now with possible skill usage
label AnEnemyDaresToAttack(ThatEnemy):
    $ adta_add=""
    if autofight:
        $ adta_add="{w=.5}{nw}"
    if not ThatEnemy.frozen:
       # Determine Target
       $ WhoIllKillToday=ai_targetting(ThatEnemy)

       # Use a skill: Chances are determined by luck.
       if len(ThatEnemy.skill) > 0 and renpy.random.random() < (ThatEnemy.luk-1) and not ThatEnemy.silence:

            if config.developer:
                $ print "%s may die in %s's hands" % (WhoIllKillToday.name, ThatEnemy.name)
            $ WhatIllCastToday=ai_skillcasting(ThatEnemy) # Necessary because WhoIllKillToday might be altered!
            if config.developer:
                $ print "%s against %s by %s" % (WhatIllCastToday.name, WhoIllKillToday.name, ThatEnemy.name)
            call auto_skill(WhoIllKillToday, WhatIllCastToday, ThatEnemy)
            return


       # Use a normal attack
       if CRandom() < ThatEnemy.weapon.acc*ThatEnemy.dex:
               play sound "sfx/"+renpy.random.choice(['W', 'W2', 'W3', 'W4', 'P', 'G'])+".ogg"
               python:
                  dmg=int(renpy.random.randint(ThatEnemy.weapon.mindmg, ThatEnemy.weapon.maxdmg)*ThatEnemy.str)
               $ fght_act=WhoIllKillToday
               $ dmg=recalculate_damage(WhoIllKillToday, dmg, caster=ThatEnemy)
               $ hit_someone_verbose(WhoIllKillToday, dmg)
               $ calc_weapon_skill(WhoIllKillToday, ThatEnemy.weapon, ThatEnemy)
               $announce_dmg(dmg, WhoIllKillToday)
               if WhoIllKillToday==hero:     
                    "[ThatEnemy.name!t] successfully hits you for [dmg] damage.[adta_add]"
               else:
                    "[ThatEnemy.name!t] hits [WhoIllKillToday.name!t] for [dmg] damage.[adta_add]"
       else:
                "[ThatEnemy.name!t] misses![adta_add]"
    else:
                "[ThatEnemy.name!t] is paralyzed![adta_add]"

    return

# TODO Contains bugs
label AnAllyAutoAttacks(ThatAlly):
    if not ThatAlly.frozen:
        if CRandom() < ThatAlly.weapon.acc*ThatAlly.dex:
               play sound "sfx/"+renpy.random.choice(['W', 'W2', 'W3', 'W4', 'P', 'G'])+".ogg"
               python:
                  for hitme in EnemyParty:
                    if hitme.hp > 0:
                      break
                  dmg=int(renpy.random.randint(ThatAlly.weapon.mindmg, ThatAlly.weapon.maxdmg)*ThatAlly.str)

               $dmg=recalculate_damage(hitme, dmg, caster=ThatAlly)
               $hit_someone_verbose(hitme, dmg)
               $calc_weapon_skill(hitme, ThatAlly.weapon, ThatAlly)
               "[ThatAlly.name!t] hits [hitme.name!t] for [dmg] damage.{fast}{w=.5}{nw}"
        else:
                "[ThatAlly.name!t] misses!{fast}{w=.5}{nw}"
    else:
                "[ThatAlly.name!t] is paralyzed!{fast}{w=.5}{nw}"

    return





















init python:
    def get_en_xypos(who):
        if who in EnemyParty:
            tmp_xpos=700
            tmp_ypos=150
            tmp_crowd=0
            for ena in EnemyParty:
                if ena == who:
                    break
                tmp_ypos+=60
                tmp_crowd+=1
                if tmp_crowd >= 5:
                    tmp_crowd-=5
                    tmp_xpos-=50
                    tmp_ypos-=280 # off-set is 300
        elif who in battling:
            tmp_xpos=700
            tmp_ypos=150
            tmp_crowd=0
            for ena in battling:
                if ena == who:
                    break
                tmp_crowd+=1
                tmp_ypos+=60
                if tmp_crowd >= 5:
                    tmp_crowd-=5
                    tmp_xpos+=50
                    tmp_ypos-=280 # off-set is 300
        else:
            tmp_xpos=0.5
            tmp_ypos=0.5
        return [tmp_xpos, tmp_ypos]

    def announce_dmg(dmg, who):
        if type(dmg) == int and dmg > 0:
            CCOLOR='#F00'
        elif type(dmg) == int and dmg < 0:
            CCOLOR='#0F0'
        else:
            CCOLOR='#777'
        ui.at(Position(xpos=get_en_xypos(who)[0], ypos=get_en_xypos(who)[1], xanchor="center", yanchor=0.0))
        ui.text(str(dmg), color=CCOLOR,  outlines=[ (absolute(1), "#FFF", absolute(0), absolute(0)) ], zorder=9000)

    def skip_fight(): # TODO: I hate this function. With a full team never playing you skip first fights (40 >= 22). Not with hero alone (10)
        tmp_op=0
        tmp_oe=0
        SFMagic=7.5
        for i in battling:
            tmp_op+=i.level*10 # TODO: Find a different way to calc this
        for i in EnemyParty:
            tmp_oe+=i.reward_lvl

        if config.developer:
            print "Skip fight? Is %d >= %d?" % (tmp_op,int(tmp_oe*SFMagic))
        if tmp_op >= int(tmp_oe*SFMagic) and dungeon_state and persistent.battleShortcut:
            print "Yes, we can skip it."
            return True
        return False

label setup_fight:          # XXX Setup Fight label. Do not confuse with setup_boss_fight (bigger rewards and unique drops)
    $ combat_autosave()
    $ renpy.force_autosave()
    $ config.has_autosave = False
    play sound "sfx/fight.ogg"
    centered "{image=gfx/fight.png}" with sshake_sanctum
    $ fighting=True
    $ autofight=False
    if EnemyParty == []:
        $ EnemyParty=[ThisEnemy] # TODO
        $ print "\033[31mWARNING: A deprecated call with only ThisEnemy set was detected. Please report this bug to developers: " + ThisEnemy.name + "\033[0m"

    # load basic env
    $ show_enbar=True
    $ rounda=1

    $ xp_rl=0
    $ col_rl=0
    $ item_rl=0                                        # This is calculated later. Amount of itens dropped.
    $ item_list=""                                     # Which items were dropped? Handled later. 

    # Generate rewards
    python:
        for UglyEnemy in EnemyParty: # IMPORTANT: Never ever use Enemy as tmp name
         xp_rl  +=int(UglyEnemy.reward_lvl*((renpy.random.random()*renpy.random.randint(1,  6))+1))
         # Boss Proposal: randint(5, 12)
         col_rl +=int( (UglyEnemy.reward_lvl*renpy.random.randint(5, 7)) * (1.00+(renpy.random.random()/4.0)) *hero.luk )
         # 5~7 RL in credits, with 25% randomness bonus, plus your Luck bonus.
         # A level 4 mob (easy) renders 40 RL so → 200~350 Col
         # rl: 0~24 : BAD MUST REWRITE TODO (?)
         #int(Enemy.reward_lvl*renpy.random.randint(3, 11)*hero.luk) # For boss: randint(7, 24) Extra from luck.

        drops=drop_items(133731415)              # Drop items

    # Set background
    $ renpy.show("battlebg "+combat_bg)
    $ renpy.with_statement(dissolve)

    # Adjustment by difficulty
    if gdf_mult != 1.0:
      python:
        for enab in EnemyParty:
            enab.maxhp*=gdf_mult
            enab.hp*=gdf_mult
            enab.weapon.maxdmg*=gdf_mult
            enab.weapon.mindmg*=gdf_mult
            # Fix values
            enab.hp,enab.maxhp,enab.weapon.maxdmg,enab.weapon.mindmg=int(enab.hp),int(enab.maxhp),int(enab.weapon.maxdmg),int(enab.weapon.mindmg)
        xp_rl +=abs(int(10*gdf_mult)-10)
        col_rl+=abs(int(20*gdf_mult)-20)

    # Handle skip-fight cheat
    if skip_fight() or persistent.skipFight:
        if not "COMBAT_OVERRIDE" in gstory_ctrl and not show_bossb:
            $xp_rl-=int(xp_rl/10)
            $col_rl-=int(col_rl/10)
            jump won_fight

    $ renpy.block_rollback()
    jump do_fight

label do_fight:         # XXX Fight label for regular mobs. Do not confuse with do_boss_fight (more complex)
    # We expect to some variables get specified, and by that i mean:
    # ThisEnemy -- Enemy struct.
    # hero      -- Enemy struct.

    # Story Overriding Controls
    if "COMBAT_OVERRIDE" in gstory_ctrl:
      python:
        for ID in gstory_ctrl:
            # COD_ → Death based dialog override command.
            if "COD_" in ID:
                IDX=ID.replace("COD_", "")
                IDX=IDX.split("_@_")
                IDN=get_player_by_name(IDX[0])
                if IDN.hp <= 0:
                    gstory_ctrl.remove(ID)
                    renpy.call_in_new_context(IDX[1])
            # COR_ → Round based dialog override command.
            if "COR_" in ID and "COMBAT_OVERRIDE" in gstory_ctrl: # We double check COMBAT_OVERRIDE.
                IDX=ID.replace("COR_", "")
                IDX=IDX.split("_@_")
                if rounda == int(IDX[0]):
                    gstory_ctrl.remove(ID)
                    renpy.call_in_new_context(IDX[1])
            # COM_ → Combat Override Mad: When a BOSS HP reaches last bar, it may become mad and change aispec
            if "COM_" in ID and "COMBAT_OVERRIDE" in gstory_ctrl: # We double check COMBAT_OVERRIDE.
                IDX=ID.replace("COM_", "")
                IDX=IDX.split("_@_")
                IDN=get_player_by_name(IDX[0], "INCLUDE_ENEMIES")
                if IDN.name == "SYSTEM ERROR - BUG, REPORT ME: GPBN":
                    gstory_ctrl.remove(ID)
                    IDN.hpbar=12 # Prevent
                print str(IDN.maxhp)+' / '+str(IDN.hpbar)
                print str(IDN.hp)+' <= '+str(int(IDN.maxhp/IDN.hpbar))
                if IDN.hp <= int(IDN.maxhp/IDN.hpbar):
                    gstory_ctrl.remove(ID)
                    renpy.call_in_new_context(IDX[1])
            # COF_ → Combat Override Freeze: When someone/something is frozen, is NOT auto-removed
            if "COF_" in ID and "COMBAT_OVERRIDE" in gstory_ctrl: # We double check COMBAT_OVERRIDE.
                IDX=ID.replace("COF_", "")
                IDX=IDX.split("_@_")
                IDN=get_player_by_name(IDX[0], "INCLUDE_ENEMIES")
                if IDN.frozen > 0:
                    renpy.call_in_new_context(IDX[1])


    # It's show time!!
    $ fght_act=hero
    $ atk_on(hero)

    if not autofight:
        call hero_combat_menu
    else:
        call AnAllyAutoAttacks(hero)

    $ cooldown(hero)
    $ atk_off()
    if winners_losers() == "winners":           # Won the fight?
        jump won_fight

    # All your friends must have a chance to fight.
    python:
      for friendee in battling:

        if winners_losers() == "winners":           # Won the fight? It's here because hero is in battling.
            renpy.jump('won_fight')
            break

        if (friendee != hero) and (friendee.hp > 0):
              friend=friendee # FDEE: Always do this before calling friend_menu
              fght_act=friendee
              atk_on(friendee)
              if not autofight:
                renpy.call_in_new_context("friend_menu")
              else:
                renpy.call_in_new_context("AnAllyAutoAttacks", friend)
              cooldown(friend)

    $ atk_off()
    #$ autofight=False
    if winners_losers() == "winners":           # Won the fight?
        jump won_fight

    # REPROCESS some special CO flags (like COF)
    if "COMBAT_OVERRIDE" in gstory_ctrl:
      python:
        for ID in gstory_ctrl:
            # COF_ → Combat Override Freeze: When someone/something is frozen, is NOT auto-removed
            if "COF_" in ID and "COMBAT_OVERRIDE" in gstory_ctrl: # We double check COMBAT_OVERRIDE.
                IDX=ID.replace("COF_", "")
                IDX=IDX.split("_@_")
                IDN=get_player_by_name(IDX[0], "INCLUDE_ENEMIES")
                if IDN.frozen > 0:
                    renpy.call_in_new_context(IDX[1])


    if autofight:
        "Round [rounda]\n\nIt's enemy turn!{w=1.0}{nw}"
    else:
        "Round [rounda]\n\nIt's enemy turn!"
    python:
        for ThoseEnemy in EnemyParty:
            if ThoseEnemy.hp <= 0:
                continue
            atk_on(ThoseEnemy)
            renpy.call_in_new_context("AnEnemyDaresToAttack", ThoseEnemy)
            cooldown(ThoseEnemy)

            atk_off()
            if winners_losers() == "losers" or persistent.lost or hero.hp <= 0:                # Lost the fight and therefore game?
                renpy.block_rollback()
                renpy.jump('defeat')

    $ rounda += 1
    $ renpy.fix_rollback()
    jump do_fight                   # Anything exceptional happened? Continue!


label won_fight:    # You just won a regular fight, show results. Do not confuse w/ won_boss_fight (shows also CONGRATULATIONS image)
    $ renpy.force_autosave()
    $ autofight=False
    # TODO: won_fight():83 - Calculate item drops based on round number.
    $ atk_off() # Doublesure

    $ clean_all_status_effects() # Clear all effect status carryovers
    $ EnemyParty=[] # Clear enemies list
    # Default behavior restore.
    if not canflee:
        $ canflee=True

    $ fighting=False
    $ item_list =writedrop(drops)
    $ item_rl   =len(drops)

    $ p=" "

    # Display battle results
    $ ui.window(background=Frame("gfx/blackwindow.png", 6, 6), xalign=.5, yalign=.5, ymaximum=.5, xmaximum=.5)
    $ ui.vbox(xalign=.5)
    $ ui.text("{i}Results{/i}", xalign=.5)
    $ ui.text("XP:[p][p][p][p][p][p][p][p][p][p][p][p][xp_rl]")
    $ ui.text("Col:[p][p][p][p][p][p][p][p][p][p][p][col_rl]")
    $ ui.text("Items:[p][p][p][p][p][p][p][item_rl]")
    $ ui.text("___________________")
    if item_rl > 0:
        $ ui.hbox()

        $ jrncslvp=ui.viewport(xfill=False, mousewheel=True, draggable=False) # what's with the silly name
        $ ui.text("[item_list]")
        $ ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar', #left_bar="#888", right_bar="#888", xmaximum=4,
        thumb=theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar_thumb.png", "#FFF"),
        left_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12),
        right_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12)
        )
        $ ui.close()

    $ ui.close()
    pause

    $ show_enbar=False
    # Now, we add the status.
    $ gp += col_rl
    $ hero.xp+=xp_rl
    $ stockdrop(drops)

    # Your other allies on fight also receive XP: 2/3 if alive, 1/3 if dead.
    python:
        for helper in battling:
          helper.aff+=1 # Increase affection =D It takes too long to reach 100% though
          if helper != hero:
            if helper.hp > 0:
                helper.xp+=int((xp_rl*2)/3) # Alive Friends only receive 2/3 from total xp.
            else:
                helper.xp+=int((xp_rl)/3) # Dead Friends only receive 1/3 from total xp.
            # Check for LVLUPS
            if helper.xp >= helper.mxp:
                nlvl=helper.level+1
                (centered("[helper.name] LVLUP!\n[helper.level] {image=gfx/arrow.png} [nlvl]"))
                helper.s_lvlup()

    # Now, we check for potential level ups.
    if hero.xp >= hero.mxp:
        $nlvl=hero.level+1
        centered "[yname] LVLUP!\n[hero.level] {image=gfx/arrow.png} [nlvl]"
        $hero.lvlup()

    # Remove remaining combat override controls
    if "COMBAT_OVERRIDE" in gstory_ctrl:
      python:
        for ID in gstory_ctrl:
            if "COD_" in ID:
                    gstory_ctrl.remove(ID)
            if "COR_" in ID:
                    gstory_ctrl.remove(ID)
            if "COM_" in ID:
                    gstory_ctrl.remove(ID) # TODO: broken?
            if "COF_" in ID:
                    gstory_ctrl.remove(ID)
            if "COMBAT_OVERRIDE" in ID:
                    gstory_ctrl.remove(ID)
    #$ renpy.free_memory()

    # Hide background
    $ renpy.hide("battlebg") # combat_bg
    $ renpy.with_statement(dissolve)

    # How we should quit?
    $ config.has_autosave = True

    if not dungeon_state:       # XXX Case 1: You're not on dungeons.
            jump dev_beta
    else:                       # XXX Case 2: You're on dungeons.
            $ renpy.jump("dev_beta__dungeon_l" + str(g_cur_level))


            
label hero_combat_menu:
    $ done=False
    menu:
        "Round [rounda]\n\nIt's [hero.name]'s turn!!\nPlease take an action."
        "Attack" if not hero.frozen:
            $ done=True

            # XXX Target Set
            window hide
            $ set_combat_target()
            $ target=ui.interact()
            window show
            if target=="abort": # Abort button
                jump hero_combat_menu
            # XXX Target Set

            if CRandom() < hero.weapon.acc*hero.dex:
                play sound "sfx/"+renpy.random.choice(['W', 'P', 'G', 'W2', 'W3', 'W4'])+".ogg"
                $dmg=int(renpy.random.randint(hero.weapon.mindmg, hero.weapon.maxdmg)*hero.str)
                $dmg=recalculate_damage(target, dmg, caster=hero)
                $hit_someone_verbose(target, dmg)
                $calc_weapon_skill(target, hero.weapon, hero)
                # Test new damage tracking system
                $announce_dmg(dmg, target)
                "You sucefully hit [target.name!t] for [dmg] damage."
            else:
                "You miss [target.name!t]!"

        "Use Skill" if not hero.frozen and not hero.silence:
            $ done=True
            call select_skill(hero)

        "Use Item" if not hero.frozen:
             $ done=True
             call use_item

        "Flee" if not hero.frozen and canflee:
            $ done=True
            if (not dungeon_state) and renpy.random.random() < 0.7: # 70% odds of fleeing. Does not work on dungeons.
                $ col_rl=0
                $ xp_rl=0
                $ drops=[] # TODO
                e "You've flew sucefully!"
                jump won_fight
            else:
                e "Failed to run!"

        "Wait":
            $ done=True
            if not hero.frozen:
                $ hero.mp+=int(hero.maxmp/20)
                $ hero.hp+=int(hero.maxhp/100)
                if hero.mp > hero.maxmp:
                    $ hero.mp-=(hero.mp-hero.maxmp)
                if hero.hp > hero.maxhp:
                    $ hero.hp-=(hero.hp-hero.maxhp)
                $ renpy.block_rollback()
                e "You decide to wait."
            else:
                e "You're paralyzed."

    if done:
        return
    else:
        jump hero_combat_menu

label friend_menu:
    $ renpy.fix_rollback()
    $ done=False
    menu:
        "It's [friend.name!t]'s turn."
        "Attack" if not friend.frozen:
            $ done=True

            # XXX Target Set
            window hide
            $ set_combat_target()
            $ target=ui.interact()
            window show
            if target=="abort": # Abort button
                jump friend_menu
            # XXX Target Set

            $ renpy.fix_rollback()
            if CRandom() < friend.weapon.acc*friend.dex:
                play sound "sfx/"+renpy.random.choice(['W', 'W2', 'W3', 'W4', 'P', 'G'])+".ogg"
                $dmg=int(renpy.random.randint(friend.weapon.mindmg, friend.weapon.maxdmg)*friend.str)
                $dmg=recalculate_damage(target, dmg, caster=friend)
                # Test new damage tracking system (TODO)
                #if config.developer or persistent.premium:
                #    $announce_dmg(dmg, target)
                $hit_someone_verbose(target, dmg)
                $ renpy.fix_rollback()
                $calc_weapon_skill(target, friend.weapon, friend)
                # Test new damage tracking system
                $announce_dmg(dmg, target)
                "[friend.name!t] sucefully hits [target.name!t] for [dmg] damage."
            else:
                "[friend.name!t] misses [target.name!t]!"


        "Use Skill" if not friend.frozen and not friend.silence:
            $ done=True
            call select_skill(friend)

#       TODO!! Friends using their items. :) - NOTE: Better hero carry everything
        "Use Item" if not friend.frozen:
            $ done=True
            call use_item

        "Wait":
            $ done=True
            if not friend.frozen:
                $ friend.mp+=int(friend.maxmp/20)
                $ friend.hp+=int(friend.maxhp/100)
                if friend.mp > friend.maxmp:
                    $ friend.mp-=(friend.mp-friend.maxmp)
                if friend.hp > friend.maxhp:
                    $ friend.hp-=(friend.hp-friend.maxhp)
                $ renpy.block_rollback()
                e "[friend.name!t] waits."
            else:
                e "[friend.name!t] is paralyzed."

    if done:
        return
    else:
        jump friend_menu


























init python:
    g_overlaytext="@cancel"
    g_overlayfunc="show_hpbar"

#init 999 python:
#    g_overlaytext="@cancel"
#    g_overlayfunc="show_hpbar"

#    def SetTrue_overlay(text, vari):
#        if g_overlaytext != "@cancel":
#            ui.fixed(xfill=True, yfill=True)
#            ui.textbutton(text, clicked=SetVariable(vari, True), xalign=0.5, yalign=0.5)
#            ui.close()

#    config.overlay_functions.append(SetTrue_overlay(g_overlaytext, g_overlayfunc))
