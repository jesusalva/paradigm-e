Low Healing
	Heals an ally... or a foe. And, best of all... No cooldown!

MP Cost: 15
Dmg: (-5) - (-25)
cooldown: 0
----------------------
Healing
	Heals an ally... or a foe. W/ cooldown, so, unpopular.

MP Cost: 25
Dmg: (-15) - (-45)
cooldown: 1
----------------------
Great Healing
	Heals an ally... or a foe. That is one of the best of all heals.

MP Cost: 50
Dmg: (-25) - (-50)
cooldown: 0
----------------------
Subliminal Healing
	Heals an ally... or a foe. Very good, very high heal... And very expensive.

MP Cost: 75
Dmg: (-75) - (-125)
cooldown: 0
----------------------
Terminal Healing
	Heals fully. Takes all MP. W/ cooldown... not too popular, but will save your life.

MP Cost: 100%
Dmg: (-[ThisEnemy->MaxHP])
cooldown: 3
----------------------
Coletive Healing
	Heals all allies! The most popular ever.

MP Cost: 50
Dmg: (-50) - (-76)
cooldown: 2
-----------------------
G. Coletive Healing
	Heals all alies! Takes all your MP and a good cooldown. But it's worth the trouble.

MP Cost: 100%
Dmg: (-[ThisEnemy->MaxHP])
cooldown: 5

