########################################################################################
#     This file is part of Castle.
#     Copyright (C) 2015  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Mechanics file

init python:
 print "\n==================== " + config.name + " v" + config.version + " " + persistent.release_name    # Print HEADER
 class Enemy:
  def __init__(self, name, hp, lvl=1, bar=1, rl=0, dl=-1, mp=100):


    self.level      =lvl                   # Level. Isolated... How sad! Python req.

    fct=20+(lvl*42)                        # Modificator for level. Initial, haves fixed value.
    if self.level > 25:
        fct+=30                            # additional past-four xp.
    if self.level > 50:
        fct+=30                            # additional past-four xp.
    if self.level > 75:
        fct+=30                            # additional past-four xp.

    #### COSMETIC
    self.name       =name
    self.desc       =""                         # Description
    self.quote      =""                         # A title, like "Linarius Secret's Guardian"
    self.girl       =False                      # This is, uhm, important
    self.dislike    =[]                         # Contains a list of dislikes (internal control)
    self.pic        ="brute"                    # Image to be displayed in-combat
    self.pot        ="none"                     # Portrait.
    self.aff        =0                          # Affection (0-100)
    self.hpbar      =bar                        # Number of HP bars
    self.ordem      ="N"                        # Affects skill selection (except hero)

    #### VARIABLE
    self.hp         =hp
    self.maxhp      =hp
    self.mp         =mp
    self.maxmp      =mp
    self.mxp        =lvl*fct    # Amount of XP needed to level up
    self.xp         =0          # Amount of XP that you currently have
    self.skp        =0          # Amount of Skill Points (for skills)

    #### EQUIPMENT
    self.weapon     =Weapon()   # Weapon() class
    self.offhand    =Armor()    # Weapon() or Armor() class.
    self.armor      =Armor()    # Armor() class.
    self.items      =[]         # a dict full of Weapon()s, HealItem()s, GenericItem()s and Armor()s.
    self.skill      =[]         # List of skills this character owns. When AI casts skill, it'll be choosen randomly.
    self.skauto     =Skill()    # Skill for semiauto button

    #### CONFIGURATION
    self.reward_lvl =rl         # How much reward you get by defeating this one, ranging from 0 to 1000. (if 0: level * 10)
    self.droplvl    =dl         # Drop list to use. This varies mob to mob. See note on drops.rpy
    self.aispec     =""         # Targetting Priority
    self.aieffc     =0.0        # AI Inefficience - Chances of override default behavior
    self.aiskpec    =[]         # Skill Priority and Intelligence

    #### INFLICTED STATUS
    self.frozen     =0          # Time remaining in paralize. Change to float on RTS.
    self.poison     =0          # Poison time remain
    self.silence    =0          # Silence time remain
    self.hidden     =0          # Hidden time remain
    self.add_def_rt =0.0        # Additional armor rate (%)
    self.add_def_dur=0          # Additional armor time
    self.add_dmg    =0.0        # Additional damage (%)
    self.add_dmg_dur=0          # Additional damage time

    # Attributes. Unlike (or like) other games, ours are multipliers. Display is int((str-1)*100)
    self.str=1.00               # Force. Affects total damage.
    self.dex=1.00               # Dextry. Affect chances to hit.
    self.vit=1.00               # Vitality. Affects armor.
    self.luk=1.00               # Luck. Affects skills casting, drop chances, and Col rewards.

  # Clear status effects
  def clean_st(self):
    self.silence    =0      # Delay util next skill usage (Silence Skill)
    self.frozen     =0      # Time remaining in paralize. Change to float on RTS.
    self.poison     =0      # Poison time remain

  def lvlup(self, fast=False):
    global skp
    # Correct xp and level up.
    self.xp-=self.mxp
    self.level+=1
    if self.name == yname:
        skp+=1

    # Recalc needed xp to lvlup.
    fct=20+(self.level*150) #diflev                 # Modificator for level.
    if self.level > 25:
        fct+=30                            # additional past-four xp.
    if self.level > 50:
        fct+=30                            # additional past-four xp.
    if self.level > 75:
        fct+=30                            # additional past-four xp.
    self.mxp        =self.level*fct    # Amount of XP needed to level up. XXX WARNING: Please note that it squares fct.

    # Clear status effects
    self.clean_st()

    # Improve stats and heal
    # TODO: Use fraction on maxhp lvlup increase. ie. 200/5=x | hero.maxhp+=(..*..)
    if not fast:
        counting=0
        while counting < HP_PER_LEVEL:
                    self.maxhp  += (HP_PER_LEVEL/10)
                    counting    += (HP_PER_LEVEL/10)
                    renpy.pause(0.01)
        hit_someone_verbose(self, self.hp-self.maxhp, True)
    else:
        self.maxhp  += HP_PER_LEVEL
        self.hp=0
        self.hp+=int(self.maxhp)

    self.maxmp+=MP_PER_LEVEL
    self.mp+=self.maxmp-self.mp


    # Increase stats
    if (self.level % 4) == 0:
        self.str+=0.01
    elif (self.level % 4) == 1:
        self.dex+=0.01
    elif (self.level % 4) == 2:
        self.vit+=0.01
    else:
        self.luk+=0.01

    # XXX: Done. Leveled up sucefully.


  def s_lvlup(self):
    self.lvlup(fast=True)


  # Add affection
  def add_aff(self, amount):
    global ctrl_lgc
    self.aff += amount
    if self.aff > 100:
        self.aff = 100
    if self.aff < 0:
        self.aff = 0
    # Handle evolutions
    if (self.aff == 100):
        if self.name not in ctrl_lgc: # Already evo-broken
            self.maxhp+=100
            self.maxmp+=50
            self.str+=0.10
            self.dex+=0.10
            self.vit+=0.10
            self.luk+=0.10
            ctrl_lgc.append(self.name)
            (player(_("%s won't hold back when supporting you! STR, DEX, VIT, LUK, HP, MP permanent bonus!" % (self.name))))

 class Weapon:                  # XXX Weapon Item class.
  def __init__(self, name="Fist", mdmg=0, xdmg=1, acc=0.7, sk_pc=0.00, sk_cmd="failproof", pic="sword"):
    self.name   =name           # Weapon name
    self.mindmg =mdmg           # Minimal Damage
    self.maxdmg =xdmg           # Maximum Damage
    self.acc    =acc            # Weapon Precision (0-1 for random() )

    self.skpc   =sk_pc          # How much % of triggering a skill
    self.skcm   =sk_cmd         # Skill type + mark. Like "paralyze II": paralyze 2 turns.
    self.type   ="sword"        # DO NOT EDIT. Helps on sorting.
    self.pic    =pic            # Image fo inv display

 class Armor:                   # XXX Armor/Shield class.
  def __init__(self, name="None", abdmg=0, rate=0.0, pic="armor"):
    self.name   =name           # Name of the armor or shield item.
    self.abdg   =abdmg          # How much attack can be absorved, on maximum?
    self.rate   =rate           # How much percent it will actually absorb util abdg get reached?
    self.type   ="armor"        # DO NOT EDIT. Helps on sorting.
    self.pic    =pic            # Image fo inv display

 class HealItem:                # XXX Healing Item class.
  def __init__(self, name, heal, mpheal=0, pic="potion"):
    self.name   =name           # Name of the healing item
    self.heal   =heal           # How much hp this item heals?
    self.mp     =mpheal         # How much mp this item heals? (If any)
    self.type   ="heal"         # DO NOT EDIT. Helps on sorting.
    self.pic    =pic            # Image fo inv display

 class GenericItem:             # XXX Generic Item class.
  def __init__(self, name, s=-1, pic="misc"):
    self.name   =name           # Name of the generic item.
    self.type   ="misc"         # DO NOT EDIT. Helps on sorting.
    self.sell   =s              # How much this item sells for? Default: 10
    self.pic    =pic            # Image fo inv display

 class Crystaline:              # XXX Cristaline Device Item Class
  def __init__(self, name, label, behavior="call", pic="ball"):
    self.name   =name           # Name of the crystaline item.
    self.label  =label          # Label which is called/jumped when using
    self.bhv    =behavior       # Only two values accepted: 'call' and 'jump'. Determines what do with label.
    self.type   ="crystal"      # DO NOT EDIT. Helps on sorting.
    self.pic    =pic            # Image fo inv display

 class Skill:
  def __init__(self, name="BUG, REPORT ME!", typen='P', cost=0, lvl=1, desc="Blabla - TODO", halo="NONE"):
    self.name   =name           # Skill Name
    self.type   =typen          # Skill Type (P-passive A-attack H-healing B-buff D-debuff R-rangerattack)
    self.cost   =cost           # Skill mana cost.
    self.lvl    =lvl            # Skill level. Not always used.

    self.desc   =desc           # Only for displays
    self.halo   =halo           # What display when used?

    self.int    =0              # integer value
    self.float  =0.0            # float value
    self.array  =[]             # array value
    self.string =""             # string value

#    self.cool   =cooldown       # Total cooldown time each use # Too complex for now! ;-)


 friend=Enemy("a", 1)
 def CRandom():
    if persistent.Always100:
        return 0.0
    else:
        return renpy.random.random()

 def buy_item(price, item, equip="INV"):
    global gp, hero
    if ((gp-price) < 0):
      (e (_("You cannot buy that item")))
    else:
        gp-=price
        hero.items.append(item)

        # XXX: This code is to auto-equip on hero new items. Disabled in 2017-09-30
        #if (equip=="INV"):
        #    hero.items.append(item)
        #elif (equip == "WPN"):
        #    hero.items.append(hero.weapon)
        #    hero.weapon=item
        #elif (equip == "ARM"):
        #    hero.items.append(hero.armor)
        #    hero.armor=item
        #elif (equip == "OFF"):
        #    hero.items.append(hero.offhand)
        #    hero.offhand=item
        #else:
        #    print "Warning: Invalid equip slot! Code: " + str(equip)
        #    hero.items.append(item)

 def rm_mult_items(qnt, item):
    i=0
    while i < qnt:
        hero.items.remove(item)
        i+=1

 def add_mult_items(qnt, item):
    i=0
    while i < qnt:
        hero.items.append(item)
        i+=1



label use_item:
    python:
        hitems=[]
        nitems={}
        xitems=[['Cancel', 'SIG_ABRT3']]
        for a in hero.items:
            if a.type=="heal":
                if not a.name in nitems.keys():
                    hitems.append([a.name, a.heal, a, a.mp])
                    nitems[a.name]=1
                else:
                    nitems[a.name]+=1
        for some in hitems:
            if some[3]:
                xitems.append([some[0] + " - " + str(some[1]) + " HP, " + str(some[3]) + " MP (" + str(nitems[some[0]]) + ")", some[2]])
            else:
                xitems.append([some[0] + " - " + str(some[1]) + " HP (" + str(nitems[some[0]]) + ")", some[2]])

    $_return=renpy.display_menu(xitems)

    if _return == "SIG_ABRT3":
        $ done=False
        return

    if fighting:
        # XXX Target Set
        window hide
        $ set_combat_target()
        $ target=ui.interact()
        window show
        if target=="abort": # Abort button
            $ done=False
            return # Yes this works
        # XXX Target Set
    else:
        $ target=hero
        $ print "\033[1;31mWARNING: \033[0m\033[31muse_item cast outside combat! This is a bug.\033[0m"

    $hit_someone_verbose(target, -(_return.heal))

    if _return.mp:
        $ target.mp+=_return.mp
        if target.mp > target.maxmp:
            $ target.mp-=(target.mp-target.maxmp)

    $hero.items.remove(_return)
    $renpy.block_rollback()

    return








label dev_beta_setup:
    $ show_loading_bar=True
    window hide

    call setup_weapons
    call setup_mobs
    call setup_mobitem
    call setup_mobmob
    call setup_skill
    call setup_hero
    call skill_check

    # This chunk has been moved to skill_check
    $ show_hpbar=True
    window show

    $ gcl_name=_("Linarius Prime") # TODO: gcl_name : Name of the level
    $ g_cur_level=1

    jump dev_beta   # Game start!!

label dev_beta:

    if g_story_lbl:
        $renpy.call(g_story_lbl) # Is this function correct?

    $ g_story_lbl=False # Double-sure

    if renpy.music.get_playing() != MUSIC_TOWN:
        stop music fadeout 1.5 # Stop possible musics. But we don't want to stop MUSIC_TOWN accidentaly.
        play music MUSIC_TOWN  # TODO 2017-08-20: This was now forgotten but we could add fadein 1.0 or 0.5 here...


    # Hide every possible loaded image. <code_deleted>
    # XXX 2017-08-20: Annuled. Ah, any eventually loaded image with be replaced with no fade. Oh well.

    $ renpy.scene() # Clear all loaded images, we don't need them! (TODO WARNING UNTESTED)
    $ renpy.show("city l" + str(g_cur_level)) # TODO: Use scene for city bg?

    $ save_name=_("Area ") + str(g_cur_level) + ': ' + str(gcl_name) + "\n"   # Set save name.

    $ show_hpbar=True


    menu:
        "Area %(g_cur_level)02d: %(gcl_name)s"                          # Print to user where he is.
        "Travel" if not prologue:
            $ renpy.jump("dev_beta__dungeon_l" + str(g_cur_level))   # Jump you to dungeons. Mobs are aleatory. You don't choose.

        "Explore city streets (quests, etc.)":
            $ renpy.jump("dev_beta__available_quests_on_l" + str(g_cur_level))   # Lists at your screen NPCs that may have or not quests.

        "Explore outskirts" if not prologue:                       # Why just explore the town when we have a whole world outside? I mean, a whole Castle! =)
            $ renpy.jump("dev_beta__outskirt_l" + str(g_cur_level))

        "Access character menu":    # Not really necessary to be here, but, you know, convenience.
            call ingame_menu        # Why a call? It's easier to implement instead of jumping.

    jump dev_beta
    #return










label cheat_menu:
  $ print "[DEBUG] Current Story Stack: " + str(gstory_ctrl) + " (Current Call Stack: " + str(renpy.call_stack_depth()) + ")"
  menu:
    "Cheat Menu - Project E, v[config.version] \"[persistent.release_name]\""
    "Kill ThisEnemy" if fighting:
        python:
            for movx in EnemyParty:
                movx.hp=0
        return

    "Manipulate Databases" if config.developer:
        menu:
            "reboot weapons":
                call setup_weapons
            "reboot mobdb":
                call setup_mobs
                call setup_mobitem
                call setup_mobmob
            "reboot skills":
                call setup_skill #skills.rpy file
            "reboot hero":
                call setup_hero
                call skill_check
            "reboot journal":
                call setup_journal
                #call setup_skill
            "core reboot":
                call prestart
            "abort":
                pass

        return

    "Increase one level":
        $ hero.xp = hero.mxp
        $ nlvl=hero.level+1
        centered "[yname] LVLUP!\n[hero.level] {image=gfx/arrow.png} [nlvl]"
        $ hero.s_lvlup()

    "Add 1000 Col":
        $ gp += 1000
        return

    "Add 10000 Col":
        $ gp += 10000
        return

    "Add 10 Skill Points":
        $ skp += 10
        return

    "Full Heal":
        call heal_crystal
        return
    
    "Open the Treasure Chest" if not 'CH_Paradigm' in gstory_ctrl:
        $ buy_item(0, Sandwich)
        $ hero.items.append(TeleportCRT)
        $ hero.items.append(HealingCRT)
        #$ hero.items.append(Elucidator)
        #$ hero.items.append(FreezingADM)
        $ hero.items.append(Paradigm)
        $ hero.items.append(Paradox)
        $ hero.items.append(Sacred_Cross)
        $ gstory_ctrl.append('CH_Paradigm')
        return

    "{color=#F00}{b}Reload Your Skills{/b}{/color}" if len(hero.skill) == 0:
        #call setup_skill
        call skill_check

    "Get Friends!" if len(party) <= 7:
        call add_member(pctpct)
        call add_member(jhonny)
        call add_member(mamede)
        call add_member(yasmin)
        call add_member(vermed)
        call add_member(gilvan)
        call add_member(ingrid)
        call add_ally(hc)
        return

    "Get 10 points in all atributtes (use with care!)":
        $hero.str+=0.1
        $hero.dex+=0.1
        $hero.vit+=0.1
        $hero.luk+=0.1
        return

    "Advanced Options" if config.developer:
        menu:
            "Free Memory":
                $ renpy.free_memory()
            "Initialize Questlog" if not ql.haz_quest("24 Developer Testing"):
                #$ ql=Journal()
                $ ql.add_quest_update("24 Developer Testing", "It works!", "Create a quest log for your first time.")
                $ ql.update_quest("24 Developer Testing", "It works! Doesn't it?", "Update the quest log for... reasons.")
                $ ql.close("24 Developer Testing")
                $ ql.close("24 Unitialized Quest")
                $ ql.update_quest("{color=#f4A}I Love You{/color} ({u}Spam{/u})", "", "Lorem Ipsum Dolor Sit Amet.")
            "Call a label":
                $_cheat_adocal=renpy.input("YOU ARE CALLING A LABEL. MAY CAUSE CRASHES.") or ""
                if _cheat_adocal != "":
                    $ renpy.call_in_new_context(_cheat_adocal)
            "Inject gstory_ctrl flag":
                $_cheat_adogcf=renpy.input("YOU ARE CHANGING GSTORY_CTRL") or ""
                if _cheat_adogcf != "":
                    $ gstory_ctrl.append(_cheat_adogcf)
            "Execute unsafe python code":
                $_cheat_adoupc=renpy.input("YOU ARE INJECTING UNSAFE PYTHON CODE. MAY CAUSE CRASHES.") or ""
                if _cheat_adoupc != "":
                    $ exec(_cheat_adoupc)
            "abort":
                pass
        return


    "Quit":
        return










label ingame_menu:
    # Image map?
    if not renpy.showing("IC default"):
        show IC default with Dissolve(0.2)
    menu:
        "Char Info":
            # In fights, we want to check party, not hero
            $ show_money=True
            $ managelock=True # You *can* check friends/players but not allowed to do changes
            if fighting:
                call party_screen
                $ managelock=False
                $ show_money=False
                jump ingame_menu

            # Experimental
            $ ui.window(xfill=False, yfill=False, xalign=0.91, yalign=0.25, ymaximum=80)
            $ ui.vbox()
            $ ui.text(_("%s, Lv %d, Area #%02d\n{size=12}%d XP to level up{/size}") % (yname, hero.level, g_cur_level, int(hero.mxp-hero.xp)  ))
            $ ui.text(_("STR: %02d DEX: %02d VIT: %02d LUK: %02d") % ((hero.str-1)*100, (hero.dex-1)*100, (hero.vit-1)*100, (hero.luk-1)*100 ) )
            $ skill_screen(hero, inline=True)
            $ ui.close()
            $ herX=_("hero")
            if not ymale:
                $herX=_("heroine")
            "Char sheet" "Player [yname], the [HClass!t] [herX!t].\n\
Primary weapon: [hero.weapon.name!t]   ,   Damage: [hero.weapon.mindmg] - [hero.weapon.maxdmg]   ,   Precision: [hero.weapon.acc]\n\
Off-hand: [hero.offhand.name!t]\n\
Armor: [hero.armor.name!t]   ,   Absorbs util [hero.armor.abdg] damage in a rate of [hero.armor.rate] : 1.\n\
Current area: [g_cur_level]\n\
Current Level: [hero.level]\n\
XP: [hero.xp]/ [hero.mxp]{fast}\n\
\n\
Project E - v[config.version] \"[persistent.release_name]\""

            $ managelock=False
            $ show_money=False
        "Quest Log":
            call quest_screen
        "Inventary" if not fighting: # During fights use standard useitem command
            call inv_screen
        "Skills" if not fighting:
            call skmanage
        "Party Settings" if not fighting:
            call party_screen
        "   ":#Quit": # Quit is... odd.
            hide IC with Dissolve(0.2)
            return

    jump ingame_menu

label rest_menu:
  "Rest: creates 0.55%% HP/second. Click to start/stop resting.\n\n(Values for Normal Difficulty. Time may differ on different difficulty levels.)"
  $ show_hpbar=False
  window hide
  python:
    while not renpy.pause(1): # diflev
      ui.frame(xalign=.5, yalign=.5)
      ui.vbox()
      ui.text(_("Now resting..."), font="f/Imperator.ttf")
      for i in battling:
            ui.hbox() # main listing from fighters
            if renpy.exists("gfx/enemies/" + i.pic + ".png"):
                ui.textbutton("{image=gfx/enemies/"+i.pic+".png} " + i.name, clicked=None, size=16)
            else:
                ui.textbutton(i.name, clicked=None, size=16)
            ui.vbox()
            ui.bar(i.maxhp, i.hp, xmaximum=300, xminimum=300, ymaximum=5, left_bar="#0F0", right_bar="#333")
            ui.bar(i.maxmp, i.mp, xmaximum=300, xminimum=300, ymaximum=3, left_bar="#00F", right_bar="#333")
            ui.close()
            ui.text(_("HP: %d/%d\nMP: %d/%d") % (i.hp, i.maxhp, i.mp, i.maxmp), size=12)
            ui.close()
      ui.close()

      for char in party:
        if (char.maxhp/180) < 3:
            char.hp+=3
        else:
            char.hp+=int(char.maxhp/180) #I decreased this value from 200 to 180. Gameplay reasons. (0.5% to effective 0.55% or 0.555555%) 
        char.mp+=(char.maxmp/100)
        if char.mp > char.maxmp:
            char.mp=char.maxmp
        if char.hp > char.maxhp:
            char.hp=char.maxhp

  $ show_hpbar=True
  window show
  $ renpy.block_rollback() # Prevent acidental HP loss.
  "Much better now, you fell refreshed."
  return

label sleep_menu:
  python:
    # Calculate how much the full INN healing will cost you
    factor=len(party)+1
    tmp_price=0
    for tmp_sm in party:
        tmp_price+=int(tmp_sm.maxhp-tmp_sm.hp)
        tmp_price+=int(tmp_sm.maxmp-tmp_sm.mp)
    tmp_price=int(tmp_price/factor)
    # During "prologue" sleeping at inn may advance story
    if "INN" in gstory_ctrl:
        tmp_price=0

  $ show_money=True

  menu:
    "Sleep at an inn: For only [tmp_price] you can fully heal in HP and MP your party!"
    "Sleep at inn" if gp > tmp_price:
        $ gp-=tmp_price
        python:
            for tmp_sm in party:
                tmp_sm.hp+=int(tmp_sm.maxhp-tmp_sm.hp)
                tmp_sm.mp+=int(tmp_sm.maxmp-tmp_sm.mp)
        centered "Zzzz..."

        # If we have a Controlled Post-Rest Jump (INN_ instead INN) do it.
        # Advised to use alongside prologue. Note that a INN_ jump don't provokes free night.
        python:
            for ID in gstory_ctrl:
                if "INN_" in ID:
                    g_story_lbl=ID.replace("INN_", "")

                # controlled inn removal
                if "RMINN" in ID:
                    gstory_ctrl.remove("INN") # Needed
                    gstory_ctrl.remove(ID) # Possible usage: RMINN_whatever

    "Don't sleep at inn":
        pass

  $ show_money=False
  $ renpy.block_rollback() # Prevent acidental HP loss.
  if not g_story_lbl:
    "Okay, it's time to go!"
  return





































#            "Char sheet" "Player [yname], the [HClass] [herX].\n\
#Primary weapon: ]   ,   Damage: [] - [hero.weapon.maxdmg]   ,   Precision: []\n\
#Off-hand: [hero.offhand.name!t]\n\
#Armor: [hero.armor.name!t]   ,   Absorbs util [hero.armor.abdg] damage in a rate of [hero.armor.rate] : 1.\n\
#Current area: [g_cur_level]\n\
#Current Level: [hero.level]\n\
#XP: [hero.xp]/ [hero.mxp]{fast}\n\
#\n\
#Project E - v[config.version] \"[persistent.release_name]\""



init python:
    def party_ui(who):
        if renpy.exists("gfx/portrait/" + who.pot.replace(' ', '-') + ".png"):
            ui.image("gfx/portrait/" + who.pot.replace(' ', '-') + ".png", xalign=0.0, yalign=1.0) # Will look badly.
        ui.window(xalign=.5, yalign=.5, xfill=False, yfill=False)# TODO: Move to side and display portrait
        ui.hbox()
        ui.vbox()
        ui.text(who.name)
        # ui.image("gfx/div.png")
        ui.text(_("LVL: %d") % who.level)
        ui.text(_("%d experience to level up") % int(who.mxp-who.xp))
        ui.null(height=10)
        ui.text(_("HP: %d/%d | MP: %d/%d") % (who.hp, who.maxhp, who.mp, who.maxmp))
        ui.bar(who.maxhp, who.hp, xmaximum=400, xminimum=400, ymaximum=5, left_bar="#0F0", right_bar="#333")
        ui.bar(who.maxmp, who.mp, xmaximum=400, xminimum=400, ymaximum=3, left_bar="#00F", right_bar="#333")
        ui.text(_("STR: %02d DEX: %02d VIT: %02d LUK: %02d") % ((who.str-1)*100,(who.dex-1)*100,(who.vit-1)*100,(who.luk-1)*100) )
        # ui.hbox() -- Skill icon
        skill_screen(who)
        ui.null(height=20)
        ui.vbox() #-- Equip icon

        ui.hbox() #-- Weapon
        ui.image("gfx/p_wpn.png")
        ui.vbox()
        ui.text("%s" % (who.weapon.name))
        ui.text(_("DMG: %d - %d | Precision: %d %%") % (who.weapon.mindmg, who.weapon.maxdmg, int((who.weapon.acc)*100)) )
        ui.close()
        ui.close() # weapon

        ui.hbox() #-- Off-hand
        ui.image("gfx/p_off.png")
        ui.vbox()
        ui.text("%s" % (who.offhand.name))
        ui.text(_("EFFICIENCY: %d %% | LIMIT: %d") % ( int(who.offhand.rate*100), who.offhand.abdg ) ) # TODO: What if it isn't a shield? Oh well.
        ui.close()
        ui.close() # off-hand

        ui.hbox() #-- Armor
        ui.image("gfx/p_arm.png")
        ui.vbox()
        ui.text("%s" % (who.armor.name))
        ui.text(_("EFFICIENCY: %d %% | LIMIT: %d") % ( int(who.armor.rate*100), who.armor.abdg ) )
        ui.close()
        ui.close() # armor


        ui.close() # equip

        ui.hbox() # -- managment
        if (not managelock) and (who != hero):
            if who in battling:
                if who in party:
                    ui.textbutton(_("FIGHT: ON"), clicked=ui.returns('toog'), size=15)
                else:
                    ui.textbutton(_("{color=#F00}Kick member{/color}"), clicked=ui.returns('toog'), size=15)
            else:
                if len(battling) < g_maxbt:
                    ui.textbutton(_("FIGHT: OFF"), clicked=ui.returns('toog'), size=15)
                else:
                    ui.textbutton(_("FIGHT: OFF"), clicked=None, size=15)
        ui.textbutton(_("Quit"), clicked=ui.returns('exit'))
        ui.close() # managment


        ui.close() # info

        ui.close() # frame

    # Party Mangment Functions
    def batt_toogle(who):
        if who in battling:
            battling.remove(who)
        else:
            battling.append(who)

    def atk_on(whom):
        global ATTCK_ST, ATTCK_ID
        ATTCK_ST=True
        ATTCK_ID=whom.name

    def atk_off():
        global ATTCK_ST
        ATTCK_ST=False

















label party_screen:
    window hide
    python:
        allparty=[]
        for i in party:
            allparty.append(i)

        ui.window(xalign=0.5, yalign=0.5)
        ui.hbox()

        ui.vbox()
        ui.text(_("Active Party Members  {font=f/Freemono.ttf}(%d/%d){/font}") % (len(battling), g_maxbt))
        for friend in battling: # TODO: Broken code detected!
            prefixo=""
            for chara in friend.ordem:
                if renpy.exists("gfx/c/" + str(chara) + ".png"):
                    prefixo+="{image=gfx/c/" + str(chara) + ".png}"
            if friend in allparty:
                allparty.remove(friend)
                ui.textbutton(_("%s%s, Lv %d") % (prefixo, friend.name, friend.level), clicked=ui.returns(friend.name))
            else:
                ui.textbutton(_("GUEST: %s%s, Lv %d") % (prefixo, friend.name, friend.level), clicked=ui.returns(friend.name))
        ui.close()

        ui.vbox()
        ui.text(_("  Inactive Party Members"))
        for friend in allparty:
            ui.textbutton("%s, Lv %d" % (friend.name, friend.level), clicked=ui.returns(friend.name))
        ui.close()

        ui.textbutton(_("Quit"), clicked=ui.returns("_SIGABORT"))
        ui.close()

        tmp_info=ui.interact()

    if tmp_info == "_SIGABORT":
        window show
        return
    else:
        jump partyscreenui

label partyscreenui:
        $show_hpbar=False
        $party_ui(enwhois(tmp_info))
        $_return = ui.interact()
        if _return == 'toog' and tmp_info != yname and not managelock:
            $ batt_toogle(enwhois(tmp_info))
        if "_@_" in _return:
            $ pax=_return.split("_@_")
            $ remember_skill(pax[0], pax[1])
            pause
            jump partyscreenui
        $show_hpbar=True
        jump party_screen

init python:
    def enwhois(xname):
        for people in battling:
            if people.name==xname:
                return people
        for people in party:
            if people.name==xname:
                return people
        return no_one





























































# TODO: Equip other members? Use on other targets? Etc...
label inv_screen:
  window hide
  $ ongoing=True
  $ show_hpbar=False
  $ ThisItem=GenericItem("Bug Drop")
  $ OldItem=GenericItem("Bug Drop")

  python:
   while ongoing:
    ALLITEM={}

    for ix in hero.items:
        if ix.name in ALLITEM:
            ALLITEM[ix.name][1]+=1
        else:
            ALLITEM[ix.name]=[ix, 1]

    ui.window(background=Frame("gfx/blackwindow.png", 6, 6), xfill=False, yfill=False, xalign=0.5, yalign=1.0, xminimum=800, yminimum=500, ymaximum=500, xmaximum=800)
    jrncslvp=ui.viewport(xmaxium=190, xminimum=190, xfill=False, ymaximum=600, mousewheel=True, draggable=False)
    #ui.viewport(child_size=(780, 500), mousewheel=True, draggable=True)
    ui.hbox()   # ViewPort Box

    ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar')
    ui.grid(4, int(len(ALLITEM)/4)+1)
    #ui.vbox()   # Main V-Box frame

    hbx_cnt=0   # hbox control counter.
    hbx_cat=4*(int(len(ALLITEM)/4)+1) # hbox null widget filler target
    for ia in ALLITEM:
        item=ALLITEM[ia][0]

        ui.vbox()
        if renpy.exists("gfx/items/" + str(item.pic.replace(" ", "_")) + ".png"):
            ui.image("gfx/items/" + str(item.pic.replace(" ", "_")) + ".png")
        else:
            ui.image("gfx/items/none.png")
        ui.text(str(item.name) + " x" + str(ALLITEM[ia][1]), size=16)

        if item.type=="crystal":
            ui.textbutton(_("Use"), size=12, action=[SetVariable("ThisItem", item), Jump("use_crystal")])
        elif item.type=="sword":
            ui.textbutton(_("Equip"), size=12, action=[SetVariable("ThisItem", item),SetVariable("OldItem", hero.weapon), Jump("equip_item")])
        elif item.type=="armor" and ("Armor" in item.name or "Cloack" in item.name):
            ui.textbutton(_("Equip"), size=12, action=[SetVariable("ThisItem", item),SetVariable("OldItem", hero.armor), Jump("equip_item")])
        elif item.type=="armor":
            ui.textbutton(_("Equip"), size=12, action=[SetVariable("ThisItem", item),SetVariable("OldItem", hero.offhand), Jump("equip_item")])
        elif item.type=="heal":
            ui.textbutton(_("Heal"), size=12, action=[SetVariable("ThisItem", item), Jump("use_consumable_item")])
        else:
            ui.null(height=12)
        #ui.textbutton() if item.type
        ui.close()

        hbx_cnt+=1


    #if hero.items:
    #    ui.close() # Close last row

    hbx_cnt+=1
    ui.textbutton(_("{image=gfx/arrow.png}Exit"), outlines=[(1, "#773300", 0, 0)], ypos=0.05, xpos=0.05, action=Return(False))
    while hbx_cnt < hbx_cat:
        hbx_cnt+=1
        ui.null()
    #revert to classic
    #ui.bar(adjustment=jrncslvp.xadjustment, style='scrollbar')
    ui.close()
    ui.close() # Close VP Box
    #renpy.pause(0.15)   # Updates every 0.15 seconds. Lags are usually the delay.
    ongoing=ui.interact()
  window show
  $ show_hpbar=True
  return



label dev_beta__sellwequip:
    $ show_money=True
    "Bonjour! Want to get rid of some weapons for some quick-cash?\n{size=10}protip: {cps=80}Your inventory is infinite and without size or weight restriction. Cool, isn't it?{/cps}{/size}"
    $ sellable=[[_("Nothing, I'm done"), "SIGABRT"]]
    python:
        for item in hero.items:
            if item.type=="sword":
                if not item in sellable:
                    # 1- Calculate price
                    rice=int(((item.mindmg+item.maxdmg)*3.5)*item.acc)
                    sellable.append([_("Sell ") + item.name + " for " + str(rice) + " credits", item])

    $_return=renpy.display_menu(sellable)

    if _return == "SIGABRT":
            $ show_money=False
            return
    else:
            $ rice=int(((_return.mindmg+_return.maxdmg)*2)*_return.acc)
            call dev_beta__sellmult(_return, rice)

    jump dev_beta__sellwequip

label dev_beta__sellaoequip:
    $ show_money=True
    "Bonjour! I buy used equipament for a very small price. Want to sell something?\n{size=10}protip: {cps=80}Your inventory is infinite and without size or weight restriction. Cool, isn't it?{/cps}{/size}"

    $ sellable=[[_("Nothing, I'm done"), "SIGABRT"]]
    python:
        for item in hero.items:
            if item.type=="armor":
                if not item in sellable:
                    # 1- Calculate price
                    rice=int( ( (item.abdg*10) * (item.rate*4) ) )
                    sellable.append(["Sell " + item.name + " for " + str(rice) + " credits", item])

    $_return=renpy.display_menu(sellable)

    if _return == "SIGABRT":
            $ show_money=False
            return
    else:
            $ rice=int( ( (item.abdg*10) * (item.rate*4) ) )
            call dev_beta__sellmult(_return, rice)


    jump dev_beta__sellaoequip










init python:
  import os
  adm_portraits=[]

  ##############################################################################################################

  for fname in os.listdir(config.gamedir + '/gfx/portrait'):
    if fname.endswith(('.jpg', '.png')):
      # we get the name of archive and delete ending (.png or .jpg) to name.
      tag = fname[:-4]
      tag = tag.replace("ã", "a").replace("ç", "c").replace("é", "e").replace(".", "_").replace("-", " ").upper()
      fname =  'gfx/portrait/' + fname
      adm_portraits.append("%s full" % tag)
      renpy.image(tag + " full", fname) # ConditionSwitch()

  ##############################################################################################################


  ##############################################################################################################

  for fname in os.listdir(config.gamedir + '/gfx/halo'):
    if fname.endswith(('.jpg', '.png')):
      # we get the name of archive and delete ending (.png or .jpg) to name.
      tag = fname[:-4]
      tag = tag.replace("ã", "a").replace("ç", "c").replace("-", " ").replace(".", "_").replace("_", " ").lower()
      fname =  'gfx/halo/' + fname
      renpy.image("halo " + tag, fname)

  ##############################################################################################################


  def combat_autosave():
    i=9
    while i > 0:
        renpy.copy_save("0-" + str(i), "0-" + str(i+1))
        i-=1

    renpy.take_screenshot()
    renpy.save("0-1", extra_info='Combat Autosave')
    renpy.notify("Combat autosave complete")

  def clean_all_status_effects():
    for i in party:
        i.clean_st()
    for i in EnemyParty:
        i.clean_st()

























label equip_item:
    # SetVariable("ThisItem", hero.weapon),SetVariable("OldItem", item), SetField(hero, "weapon", item), Jump("giveback_item")
    # mixed so ThisItem → Selected. OldItem → For our usage. We grab ThisItem.type
    window hide
    python:
        ui.window(background=Frame("gfx/blackwindow.png", 6, 6), xfill=False, yfill=False, xalign=0.5, yalign=1.0, xminimum=800, yminimum=500, ymaximum=500, xmaximum=800)
        jrncslvp=ui.viewport(xmaxium=190, xminimum=190, xfill=False, ymaximum=600, mousewheel=True, draggable=False)
        ui.hbox() # Info & Bar
        ui.vbox() # Info Screen Rows

        # Selected Item
        if ThisItem.type == "sword":
            ui.hbox()
            ui.image("gfx/p_wpn.png")
            ui.vbox()
            ui.text("NOW EQUIPPING:", size=14)
            ui.text(ThisItem.name, size=12)
            ui.text("%d-%d (%d%%)" % (ThisItem.mindmg, ThisItem.maxdmg, int(ThisItem.acc*100)), size=12)
            ui.close()
            ui.close()
        elif ThisItem.type=="armor":
            ui.hbox()
            if ("Armor" in ThisItem.name or "Cloack" in ThisItem.name):
                ui.image("gfx/p_arm.png")
            else:
                ui.image("gfx/p_off.png")
            ui.vbox()
            ui.text(ThisItem.name, size=12)
            ui.text("%d%% (MAX %d)" % (int(ThisItem.rate*100), ThisItem.abdg), size=12)
            ui.close()
            ui.close()
        # -- Selected Item

        #print str(party)
        for char in party:

            ui.hbox()
            # Display Character
            ui.vbox()
            ui.text(char.name, size=12)
            ui.image("gfx/enemies/" + char.pic + ".png")
            ui.close()
            ui.hbox()

            ui.image("gfx/p_wpn.png")
            ui.vbox()
            ui.text(char.weapon.name, size=12)
            ui.text("%d-%d (%d%%)" % (char.weapon.mindmg, char.weapon.maxdmg, int(char.weapon.acc*100)), size=12)
            ui.close()

            ui.image("gfx/p_off.png")
            ui.vbox()
            ui.text(char.offhand.name, size=12)
            ui.text("%d%% (MAX %d)" % (int(char.offhand.rate*100), char.offhand.abdg), size=12)
            ui.close()

            ui.image("gfx/p_arm.png")
            ui.vbox()
            ui.text(char.armor.name, size=12)
            ui.text("%d%% (MAX %d)" % (int(char.armor.rate*100), char.armor.abdg), size=12)
            ui.close()

            ui.close()
            ui.textbutton("{image=gfx/arrow.png}Swap", outlines=[(1, "#773300", 0, 0)], ypos=0.05, xpos=0.05, action=Return(char))
            ui.close() # Hold-all-togheter hbox


        ui.textbutton("{image=gfx/arrow.png}Cancel", outlines=[(1, "#773300", 0, 0)], ypos=0.05, xpos=0.05, action=Return(False))
        ui.close() # -- ISR
        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar')
        ui.close() # -- I&B

    $ target=ui.interact()
    # This will result in mess we must rely on invscreen to handle a few things?
    if target:
        if ThisItem.type == "sword":
            $ OldItem=target.weapon
            $ hero.items.append(OldItem)
            $ target.weapon=ThisItem
            $ hero.items.remove(ThisItem)
        elif ThisItem.type == "armor" and ("Armor" in ThisItem.name or "Cloack" in ThisItem.name):
            $ OldItem=target.armor
            if (OldItem != Armor()) and not (OldItem.type == "armor" and OldItem.name == "None"):
                $ hero.items.append(OldItem)
            $ target.armor=ThisItem
            $ hero.items.remove(ThisItem)
        elif ThisItem.type == "armor":
            $ OldItem=target.offhand
            if (OldItem != Armor()) and not (OldItem.type == "armor" and OldItem.name == "None"):
                $ hero.items.append(OldItem)
            $ target.offhand=ThisItem
            $ hero.items.remove(ThisItem)

    window show
    $renpy.block_rollback()
    jump inv_screen

label use_consumable_item:
    window hide
    python:
        ui.window(xalign=.5, yalign=.5)

        ui.vbox() # Needed for window
        ui.vbox() # This item info
        ui.text(ThisItem.name, size=16)
        if ThisItem.mp > 0:
            ui.text("%d HP, %d MP" % (ThisItem.heal, ThisItem.mp), size=12)
        else:
            ui.text("%d HP" % (ThisItem.heal), size=12)
        ui.close() # -- TII

        jrncslvp=ui.viewport(xfill=False, mousewheel=True, draggable=False)
        ui.hbox()
        ui.vbox()

        # guests are not counted in
        for i in party:
            ui.hbox()
            if renpy.exists("gfx/enemies/" + i.pic + ".png"):
                ui.textbutton("{image=gfx/enemies/"+i.pic+".png} " + i.name, clicked=ui.returns(i), size=16)
            else:
                ui.textbutton(i.name, clicked=ui.returns(i), size=16)
            ui.vbox()
            ui.bar(i.maxhp, i.hp, xmaximum=300, xminimum=300, ymaximum=5, left_bar="#0F0", right_bar="#333")
            ui.bar(i.maxmp, i.mp, xmaximum=300, xminimum=300, ymaximum=3, left_bar="#00F", right_bar="#333")
            ui.close()
            ui.text("HP: %d/%d\nMP: %d/%d" % (i.hp, i.maxhp, i.mp, i.maxmp), size=12)
            ui.close()

        ui.textbutton("{image=gfx/abort.png} Cancel", clicked=ui.returns("@CANCEL"))
        ui.close()
        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar')
        ui.close()

        ui.close() # Needed for window

    $ _return=ui.interact()

    window show
    if _return == "@CANCEL":
        jump inv_screen

    $renpy.block_rollback()
    $ hit_someone_verbose(_return,-ThisItem.heal, fast=True)
    $ _return.mp+=ThisItem.mp

    # Fixing
    if _return.mp > _return.maxmp:
        $ _return.mp-=(_return.mp-_return.maxmp)

    $ hero.items.remove(ThisItem)
    $renpy.block_rollback()
    jump inv_screen

label giveback_item:
    if not (ThisItem.type == "armor" and ThisItem.name == "None"):
        $ hero.items.append(ThisItem)
    $ hero.items.remove(OldItem)
    $renpy.block_rollback()
    jump inv_screen

label use_crystal:
    # TODO: Removing the item should be done after the call, so we can prevent
    # item removal in cases like using teleport crystal with prologue, or aborting.
    $ hero.items.remove(ThisItem)
    #$ print "[Device] " + str(ThisItem.label)
    $ renpy.call(str(ThisItem.label))
    $renpy.block_rollback()
    jump inv_screen

label heal_crystal:
    python:
        for me in party:
            hit_someone_verbose(me,(me.hp-me.maxhp), fast=True)
            me.mp+=me.maxmp-me.mp
    return

label teletransport:
    if dungeon_state:
        player "I cannot teletransport while on dungeons!"
        return

    $ tgmenu=[["Cancel Teletransport", 0]]
    python:
        cg=1
        if not config.developer:
            while cg <= g_act_level:
                tgmenu.append([assign_city_name(cg), cg])
                cg+=1
        else:
            while cg <= g_max_level:
                if cg <= g_act_level:
                    tgmenu.append([assign_city_name(cg), cg])
                else:
                    tgmenu.append(["[[@warp] " + assign_city_name(cg), cg])
                cg+=1

    $target=renpy.display_menu(tgmenu)

    #$ target=int(target)

    if target <= 0:
        pass
    elif target > g_max_level:
        "{color=#F00}FAILURE{/color}" "You cannot teletransport to a level {i}outside{/i} the castle.\nPlease wait for the 100-levels Castle release. This is a bug btw, so HURRY AND REPORT THIS."
    elif prologue:
        "{color=#F00}FAILURE{/color}" "You cannot teletransport. Why don't you finish what you came here to first, and then try it?"
    elif (target > g_act_level) and (not config.developer):
        "{color=#F00}FAILURE{/color}" "You cannot teletransport to a level which you didn't cleared the dungeons yet."
    elif target > g_act_level and config.developer:
        menu:
            "{image=gfx/command.png}{color=#00F}Confirmation{/color}" "Confirm usage of @warp System Command.\nYou are about to teletransport to a locked level at your own risk.\n{size=10}(PS. Unlocking target level dungeons will also unlock all previous dungeons){/size}"
            "@warp" if config.developer: # May cause bugs
                $ g_cur_level=int(target)
                $ gcl_name=assign_city_name(target)
                $ save_name="Area " + str(g_cur_level) + ': ' + str(gcl_name) + "\n"   # Set save name.
                $ ThisEnemy.hp=0
                $ col_rl=0
                $ xp_rl=0
                $ drops=[[], 0]
                $ show_enbar=False
                $ canflee=True
                $ fighting=False
                "{color=#0FF}SUCESS{/color}" "You are now on Floor [g_cur_level]: [gcl_name]."
            "Cancel":
                pass

    else:
        $ g_cur_level=int(target)
        $ gcl_name=assign_city_name(target)
        $ save_name="Area " + str(g_cur_level) + ': ' + str(gcl_name) + "\n"   # Set save name.
        $ ThisEnemy.hp=0
        $ col_rl=0
        $ xp_rl=0
        $ drops=[[], 0]
        $ show_enbar=False
        $ fighting=False
        $ canflee=True
        "{color=#0F0}SUCESS{/color}" "You are now on Area [g_cur_level]: [gcl_name]."
            
    return

label dev_dev_killall:
        python:
            for movx in EnemyParty:
                movx.hp=0
            renpy.notify("Judgment was made.")
        return
