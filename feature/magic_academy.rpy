########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Utilities for Magic Academy (dev_beta__skupd)
# TODO: This code requires SERIOUS optimization.

label dev_beta__skupd:
  window hide
  python:
    try:
        skupdid+=0
    except:
        skupdid=0

    ui.window(xalign=.5, yalign=.5, xfill=False, yfill=False, xmaximum=0.8, ymaximum=0.8)
    ui.hbox() # + MAIN SYSTEM

    ui.vbox() # + INFO SYSTEM
    ui.text("{size=12}" + str(skp) + " Skill Points{/size}")
    ui.text("{size=12}" + str(gp) + " Credits{/size}")

    #if skwindow == "P":
    #    ui.text("Permanent Skills")
    #    ui.hbox()
    #    ui.textbutton("HP +10", clicked=ui.returns("p_hp10"))
    #    ui.text("100 SKP")
    #    ui.close()

    if not skupdid:
        ui.text("Enhance Skills")

        jrncslvp=ui.viewport(xmaxium=190, xminimum=190, xfill=False, ymaximum=600, mousewheel=True, draggable=False)
        ui.vbox() # DUMMY LIST
        for dummy in party:
          if True:
            ui.hbox() # +++ MASTER DUMMY INFO
            ui.vbox() # ++ misc dummy info
            ui.text(dummy.name, size=14)
            ui.image("gfx/enemies/" + dummy.pic + ".png")
            ui.close() # -- misc dummy info

            ui.vbox() # ++ skill list
            for sk in dummy.skill:
                # ATTACK SKILLS
                if ((sk.type in ['A', 'R', 'H', 'E', 'AoE', 'ICEBREATH']) or # Some skills like R contain strings :/
                    (sk.type == 'PA' and sk.int < 0)):
                    ui.hbox() # ++ skill
                    if sk.name == "Heavy Slash":
                        ui.image("gfx/mg/w1.png")
                    elif sk.name == "Light Slash":
                        ui.image("gfx/mg/w2.png")
                    elif sk.name == "Stab":
                        ui.image("gfx/mg/w3.png")
                    elif sk.name == "Magic Arrow":
                        ui.image("gfx/mg/m1.png")
                    elif sk.name == "Fireball":
                        ui.image("gfx/mg/m2.png")
                    elif sk.name == "Ultima CRUSH":
                        ui.image("gfx/mg/m3.png")
                    elif sk.name == "Heal":
                        ui.image("gfx/mg/t1.png")
                    elif sk.name == "Paralizy Dart":
                        ui.image("gfx/mg/r1.png")
                    else:
                        ui.image("gfx/mg/tray.png")
                    ui.vbox() # ++ info
                    ui.text(str(sk.name), size=12, font='f/FreeMono.ttf')
                    if sk.type == 'H':
                        ui.text("Heal: %d-%d" % (sk.array[0],sk.array[1]) ,size=20 )
                    else:
                        ui.text("Dmg: %d-%d, Acc: %d" % (sk.array[0],sk.array[1],int(sk.float*100)) ,size=20 )
                    ui.hbox() # ++ enhance
                    ui.textbutton("Dmg+", clicked=ui.returns('DMGUP_@_'+sk.name+'_@_'+dummy.name))
                    if sk.type != 'H' and sk.float < 1.0:
                        ui.textbutton("Acc+", clicked=ui.returns('ACCUP_@_'+sk.name+'_@_'+dummy.name))
                    if config.developer:
                        ui.textbutton("Cost-", clicked=None) # I need a formula where as cost tends to zero, final result tends to infinite...
                        # Where zero is infinite, and ten is, like, 50.000 Col. And where fifty is no larger than 1.000 Col... What am I doing.
                        # Cost decrease should follow the formula to, so we have a scenario where 10→9 and 50→46, for example.
                    ui.close() # -- enhance
                    ui.close() # -- info
                    ui.close() # -- skill
                # BUFF/DEBUFF/MASSBUFF SKILLS
                elif sk.type in ['B', 'MB']:
                    ui.hbox() # ++ skill
                    if sk.name == "Buff":
                        ui.image("gfx/mg/t2.png")
                    elif sk.name == "Debuff":
                        ui.image("gfx/mg/t3.png")
                    elif sk.name == "Protector Sanctum":
                        ui.image("gfx/mg/s3.png")
                    else:
                        ui.image("gfx/mg/tray.png")
                    ui.vbox() # ++ info
                    ui.text(str(sk.name), size=12, font='f/FreeMono.ttf')
                    ui.text("Buff: %d for %d hits" % (int(sk.float*100), sk.int) ,size=20 )
                    ui.hbox() # ++ enhance
                    ui.textbutton("Buff+-", clicked=ui.returns('BFLTUP_@_'+sk.name+'_@_'+dummy.name))
                    ui.textbutton("Hits+", clicked=ui.returns('BINTUP_@_'+sk.name+'_@_'+dummy.name))
                    ui.close() # -- enhance
                    ui.close() # -- info
                    ui.close() # -- skill
            ui.close() # -- skill list


            ui.null(height=15)
            ui.close() # --- MASTER DUMMY INFO

        ui.close() # --DUMMY LIST
    else:
        ui.text("Permanent Bonuses")
        jrncslvp=ui.viewport(xmaxium=190, xminimum=190, xfill=False, ymaximum=600, mousewheel=True, draggable=False)
        ui.vbox() # PERMALIST
        ui.null(height=25)

        if persistent.won:
            ui.text("+ True End Unlocked!", font='f/unifont.ttf')
        if persistent.premium:
            ui.text("+ UnreleasedContent Unlocked!", font='f/unifont.ttf')
        if persistent.skipFight:
            ui.text("+ skipFights Unlocked!", font='f/unifont.ttf')
        if persistent.legacyTavern:
            ui.text("+ legacyTavern Unlocked!", font='f/unifont.ttf')
        if persistent.CheatAutoForge:
            ui.text("+ CheatAutoForge Unlocked!", font='f/unifont.ttf')
        if persistent.Always100:
            ui.text("+ Always 100% ACC Unlocked!", font='f/unifont.ttf')
        if persistent.AlwaysCountCodes:
            ui.text("+ Always Count Claimed CheatCodes Unlocked!", font='f/unifont.ttf')

        ui.text("+ Only Lose When Hero Dies Unlocked!", font='f/unifont.ttf')
        ui.close() # --PERMALIST

    ui.hbox() # Nav
    ui.textbutton("Active", clicked=ui.returns(0))
    ui.textbutton("Passive", clicked=ui.returns(1))
    ui.textbutton("Quit", clicked=ui.returns("X"))
    ui.close() # (Nav)

    ui.close() # - INFO SYS

    ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar')

    ui.close() # - MAIN SYS
    _action=ui.interact(suppress_overlay=True)
    #print str(_action)

  window show
  python:
    if type(_action) == int:
        skupdid=_action
    elif not "_@_" in _action:
        pass # Will be handled by Ren'Python
    else:
        # Now we do python processing
        skact=_action.split("_@_")
        # skact is [type, cost, skid, pid] - debug marker below
        #print str(skact)


        if skact[0] == 'DMGUP':
            skcastb=get_player_by_name(skact[2]).skill
            skcast=Skill()
            for i in skcastb:
                if i.name == skact[1]:
                    skcast=i
                    break
            price=int(skcast.array[0]+skcast.array[1])
            # renpy menu?
            _(e(_("Enhance %s damage for %d Col?\nPress PageUp to abort.\nYou have: %d Col.{fast}" % (skcast.name, price, gp) )))
            if gp > price:
                fact=renpy.random.randint(1,3)
                skcast.array[0]+=fact
                skcast.array[1]+=fact+1
                gp-=price
                renpy.block_rollback()


        elif skact[0] == 'ACCUP':
            skcastb=get_player_by_name(skact[2]).skill
            skcast=Skill()
            for i in skcastb:
                if i.name == skact[1]:
                    skcast=i
                    break
            price=int(1100*skcast.float)
            # renpy menu?
            _(e(_("Enhance %s acc for %d Col and 1 Skill Point?\nPress PageUp to abort.\nYou have: %d Col, %d SKP.{fast}" % (skcast.name, price, gp, skp) )))
            if gp > price and skp >= 1:
                fact=renpy.random.randint(1,2)
                skcast.float+=(0.01*fact)
                gp-=price
                skp-=1
                renpy.block_rollback()


        elif skact[0] == 'BINTUP':
            skcastb=get_player_by_name(skact[2]).skill
            skcast=Skill()
            for i in skcastb:
                if i.name == skact[1]:
                    skcast=i
                    break
            if skcast.float > 0:
                price=int(650*skcast.int)*1+(skcast.float)
            else:
                price=int(650*skcast.int)*1+(skcast.float*-1)
            # renpy menu?
            _(e(_("Enhance %s for %d Col and 1 Skill Point?\nPress PageUp to abort.\nYou have: %d Col, %d SKP.{fast}" % (skcast.name, price, gp, skp) )))
            if gp > price and skp >= 1:
                fact=renpy.random.choice([1,1,1,2])
                skcast.int+=fact
                gp-=int(price)
                skp-=1
                renpy.block_rollback()


        elif skact[0] == 'BFLTUP':
            skcastb=get_player_by_name(skact[2]).skill
            skcast=Skill()
            for i in skcastb:
                if i.name == skact[1]:
                    skcast=i
                    break
            if skcast.float > 0:
                price=int(250*skcast.int)*1+(skcast.float)
            else:
                price=int(250*skcast.int)*1+(skcast.float*-1)
            # renpy menu?
            _(e(_("Enhance %s for %d Col?\nPress PageUp to abort.\nYou have: %d Col.{fast}" % (skcast.name, price, gp) )))
            if gp > price:
                fact=renpy.random.randint(1,3)
                if skcast.float >= 0:
                    skcast.float+=(fact/100.0)
                else:
                    skcast.float-=(fact/100.0)
                gp-=int(price)
                renpy.block_rollback()
        else:
            _(e(_("ERROR")))


  if _action == "X":
    jump dev_beta


  jump dev_beta__skupd

