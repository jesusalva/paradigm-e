########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Utilities for Academy (dev_beta__increase_stats)
label dev_beta__increase_stats:
    #""
    $ tmp_max_lvl=g_cur_level*11
    $ tmp_max_lvl2=int(tmp_max_lvl)/5
    #$ tmp_max_lvl2=hero.level*3-int(((hero.str-1)*100)+((hero.dex-1)*100)+((hero.vit-1)*100)+((hero.luk-1)*100))
    #if classe > 0 and classe <= 4: # Class bonus
    #    $ tmp_max_lvl2+=10
    #if tmp_max_lvl2 < tmp_max_lvl1:
    #    $tmp_max_lvl=tmp_max_lvl2
    #else:
    #    $tmp_max_lvl=tmp_max_lvl1

    $ acad_fnd=[[_("I'm done"), "SIGABRT"]]
    python:
        for char in party:
            acad_fnd.append(["Train " + str(char.name), char])

    $_hero=renpy.display_menu(acad_fnd)

    if _hero == "SIGABRT":
            $ renpy.jump("dev_beta__available_quests_on_l" + str(g_cur_level))   # Return to city.

label dev_beta__increase_stats_post:
    $ show_money=True
    $ dbisp_str=int((_hero.str-1)*100) # dbisp → Display Beta Increase Stat Post
    $ dbisp_dex=int((_hero.dex-1)*100) # dbisp → Display Beta Increase Stat Post
    $ dbisp_vit=int((_hero.vit-1)*100) # dbisp → Display Beta Increase Stat Post
    $ dbisp_luk=int((_hero.luk-1)*100) # dbisp → Display Beta Increase Stat Post
    $ dbisp_hps=int(_hero.level*150)+tmp_max_lvl+50 # 50 is the presumed difference from starting HP
    $ show(_hero, None)
    menu:
        "{b}[_hero.name]{/b}\nYou can increase your stats for just 100 Col. Each point grants a 1%% bonus.\n\n{size=18}Strength: Increases damage\nDextry: Increases Precision.\nVitality: Increases armor efficiency\nLuck: Increases weapon skill casting chances and skill damage.{/size}{fast}\nAcademy #[g_cur_level]" # Best skills are dex and luck.
        "Hit Points ([_hero.maxhp]/[dbisp_hps])" if _hero.maxhp < dbisp_hps and gp >= 100:
            $gp -=100
            $ _hero.maxhp+=1
            if _hero.maxhp+1 <= dbisp_hps and renpy.random.random() < 0.2: # 20% chance of gaining an extra HP point
                $ _hero.maxhp+=1
            if 'T' in _hero.ordem and renpy.random.random() < 0.5: # 50% chance of tanker gaining an extra HP point (Tankers)
                if _hero.maxhp+1 <= dbisp_hps:
                    $ _hero.maxhp+=1
        "Strength ([dbisp_str]/[tmp_max_lvl])" if int((_hero.str-1)*100) < tmp_max_lvl and gp >= 100:
            $gp -=100
            $ _hero.str+=0.01
        "Dextry ([dbisp_dex]/[tmp_max_lvl2])" if int((_hero.dex-1)*100) < tmp_max_lvl2 and gp >= 100:
            $gp -=100
            $ _hero.dex+=0.01
        "Vitality ([dbisp_vit]/[tmp_max_lvl])" if int((_hero.vit-1)*100) < tmp_max_lvl and gp >= 100:
            $gp -=100
            $ _hero.vit+=0.01
        "Luck ([dbisp_luk]/[tmp_max_lvl])" if int((_hero.luk-1)*100) < tmp_max_lvl and gp >= 100:
            $gp -=100
            $ _hero.luk+=0.01
        "Nothing":
            $ show_money=False
            $ hide(_hero)
            jump dev_beta__increase_stats
    jump dev_beta__increase_stats_post

