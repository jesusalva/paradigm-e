########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Default function to generate & Report bugs
init python:
    def bug(msg='they are evil.', fatal=False):
        global bugprayer, party, battling, hero, btname, g_maxbt, g_act_level, fighting, show_hpbar, show_enbar, show_bossb
        bugid=str(renpy.random.randint(1000000,9999999))+str(randhex(renpy.random.randint(4,11)))
        renpy.save('bug-'+bugid+'+'+yname)
        prname=''
        btname=''
        for i in party:
            prname+=str(i.name)+' -- '
        for i in battling:
            btname+=str(i.name)+' -- '
        print """\033[1;31mBUG, REPORT ME!\n
                 \033[0;31m%s.\n
                 bugid: %s\n\n
                 Party: %s (%s)\n
                 Combat env: %s (%s) MAX %s\n\
                 Dev: %s    MSG: %s    FGHT: %s\033[0m\n""" % (msg, bugid, str(party), prname, str(battling), btname, g_maxbt, str(config.developer), str(persistent.premium), str(fighting))

        bugprayer='Developer, I beg you to fix the bugs, because ' + str(msg) + '\n'
        for i in gstory_ctrl:
            bugprayer+=renpy.random.choice(['There is an evil ', 'I wish prosperity to ', 'Please bless the ', 'Thanks for the ', 'Bring death to ', 'Witness all sins commited by '])
            bugprayer+=str(i)
            bugprayer+='. '
        bugprayer+="\nThanks for your hardwork in all the "+str(g_act_level)+" millenniums you've been around, and slay all evil bugs."
        (player(_("[bugprayer]\n[yname]")))

        if fatal:
            show_hpbar=False
            show_enbar=False
            show_bossb=False
            fighting=False
            try:
                renpy.pop_call()
            except:
                pass
            renpy.block_rollback()
            renpy.scene()
            renpy.show('black')
            (player(_('{b}A {color=#f00}FATAL ERROR{/color} happened:{/b} \n\n%s\n\nThe error is fatal. We are sorry if this interrupt a major game of yours... Error ID: %s.' % (msg, bugid) )))
            if not config.developer or persistent.premium:
                renpy.quit()
            entries=[['Restart Game', 'start'], ['Roll Credits', 'end_beta']]#, ['Quit to Main Menu', 'testing']] # devs: use shift+O
            tmp_fatalerr=renpy.display_menu(entries)
            renpy.jump(tmp_fatalerr)


