init python:

    class CodeField(object):

        def __init__(self, den=1.0):

            self.sm = SpriteManager(update=self.update)

            # A list of (sprite, starting-x, speed).
            self.stars = [ ]

            # Note: We store the displayable in a variable here.
            # That's important - it means that all of the stars at
            # a given speed have the same displayable. We render that
            # displayable once, and cache the result.

            d = Transform(Text(renpy.random.choice(["0", "1"]), font="f/unifont.ttf", color="#FF6900"), zoom=.42)
#            d = Transform("star.png", zoom=.02)
            for i in range(0, int(40*den)):
                self.add(d, 30)

            d = Transform(Text(renpy.random.choice(["0", "1"]), font="f/unifont.ttf", color="#FF6900"), zoom=.425)
#            d = Transform("star.png", zoom=.025)
            for i in range(0, int(30*den)):
                self.add(d, 80)

            d = Transform(Text(renpy.random.choice(["0", "1"]), font="f/unifont.ttf", color="#FF6900"), zoom=.45)
#            d = Transform("star.png", zoom=.05)
            for i in range(0, int(20*den)):
                self.add(d, 160)

            d = Transform(Text(renpy.random.choice(["0", "1"]), font="f/unifont.ttf", color="#FF6900"), zoom=.475)
            #d = Transform("star.png", zoom=.075)
            for i in range(0, int(20*den)):
                self.add(d, 320)

            d = Transform(Text(renpy.random.choice(["0", "1"]), font="f/unifont.ttf", color="#FF7910"), zoom=.5)
            #d = Transform("star.png", zoom=.1)
            for i in range(0, int(25*den)):
                self.add(d, 640)

            d = Transform(Text(renpy.random.choice(["0", "1"]), font="f/unifont.ttf", color="#FF7910"), zoom=.525)
#d = Transform("star.png", zoom=.125)
            for i in range(0, int(25*den)):
                self.add(d, 960)
            
        def add(self, d, speed):
            s = self.sm.create(d)

            start = renpy.random.randint(0, 840)
            s.y = renpy.random.randint(0, 600)

            self.stars.append((s, start, speed))
            
        def update(self, st):
            for s, start, speed in self.stars:
                s.x = (start + speed * st) % 840 - 20
                #linear 2.0 xalign .5 yalign .5 clockwise circles 3
#        alignaround (.5, .5)
#        linear 2.0 xalign .5 yalign .5 clockwise circles 3


            return 0

