########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################

label set_difficulty:
    menu:
        "Select new difficulty"
        "_@_NODISPLAY_@_Story Mode" if not persistent.won:
            pass
        "Story Mode" if persistent.won:
            $ gdf_mult=0.50
        "Easy":
            $ gdf_mult=0.75
        "Normal":
            $ gdf_mult=1.00
        "Hard":
            $ gdf_mult=1.25
        "Extreme" if persistent.won:
            $ gdf_mult=1.50
        "_@_NODISPLAY_@_Extreme" if not persistent.won:
            pass
        "Input a custom value" if persistent.premium or config.developer or (float(float(renpy.count_seen_dialogue_blocks()) / renpy.count_dialogue_blocks()) >= 0.80 and persistent.won): # 80% complete (even I didn't reached such value or dev.
            $ tmpgdfm=renpy.input("Insert a difficulty value, greater than zero, and float (1.0 is normal, 1.25 is hard)", allow="0123456789.", length=4)
            python:
                try:
                    if float(tmpgdfm) > 0:
                        gdf_mult=float(tmpgdfm)
                    else:
                        raise Exception('Zero?!')
                except:
                    (e(_("Unacceptable value: [tmpgdfm]")))

    return
