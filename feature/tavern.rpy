########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Utilities for tavern (scene_ctrl_tavern)
# TODO: get rid of menu, display the portrait at a set position on the screen. So
# if not in party and chance ui.imagebutton(char.pot) ui.at(xalign bla, yalign 1.0)
# 
# ----------------------------------------------------------------------
# Determinação Técnica Nº 1708170720
# 
# Os aros interiores são como um jardim, requerem cuidados e interesse
# de ambas as partes. Por isso, as pessoas dos aros exteriores, que
# não possui necessidade de muito cuidado, ficam na taverna.
# 
# Naturalmente, acessar o aro interior é muito mais complexo, e não
# é algo realizável, então a divisão se dará por proximidade AI, e
# não por ser efetivamente AI.

image Restaurant = Frame("gfx/bg/Susan_restaurant.jpg",0,0) ## CC-BY-SA by Susan

init python:
    gtavern_mode='TAVERN'
label scene_tavern:
        $ show_money=True
        if renpy.exists("gfx/bg/Susan_restaurant.jpg"):
            show Restaurant with dissolve
        e "There are some people here, who are looking for some adventures."
        $ rng=renpy.random.randint(0, 1000) # Too bad, too predictable
        if not persistent.legacyTavern:
            jump scene_tavern2
        menu:
            "We better leave.":
                $ show_money=False
                hide Restaurant with dissolve
                jump dev_beta
            "{image=gfx/command.png}Reload Tavern List" if config.developer or persistent.premium:
                pass
            "{image=gfx/command.png}{font=f/Imperator.ttf}Use the ReDesigned Interface{/font}\n{i}{size=14}Does not have random appearances!{/size}{/i}" if config.developer or persistent.premium:
                $ persistent.legacyTavern=False
                jump scene_tavern2
            "Yasmin Pulchra Leaf (Ranger) for 6,500 credits" if renpy.random.randint(0, 1000) <= 600 and not yasmin in party:
                if gp < 6500:
                    e "We're too poor to do so."
                else:
                    $gp-=6500
                    $ yasminname="Yasmin, the Sniper"
                    call add_member(yasmin)
                    $show(yasmin)
                    eyasmin "Try anything funny, and I'll struck an arrow on your head."
                    ejhonny "Since when were you so hostile?"
                    e "Who knows. It was a long time since we last seen each other."
                    $hide(yasmin)

            "Vinicius the Redhead (Warrior) for 7,000 credits" if renpy.random.randint(0, 1000) <= 800 and not vermed in party:
                if gp < 7000:
                    e "We're too poor to do so."
                else:
                    $gp-=7000
                    $ vermesname="Vinicius, the Redhead"
                    call add_member(vermed)
                    $show(vermed)
                    evermes "I'm feeling lazy today, but hey, IS THAT GOLD? Then, let's do our best."
                    e "..."
                    ejhonny "Frankly. He didn't changed a thing."
                    e "...It's surprising that he didn't mentioned any ladies..."
                    $hide(vermed)

            "Gill Vans (Warrior) for 6,000 credits" if renpy.random.randint(0, 1000) <= 700 and not gilvan in party:
                if gp < 6000:
                    e "We're too poor to do so."
                else:
                    $gp-=6000
                    $ gilvanname="Gill Vans"
                    call add_member(gilvan)
                    $show(gilvan)
                    egilvan "..."
                    e "Eerly quiet."
                    egilvan "Hello, how are you?"
                    e "Too simpatic. Ah, nevermind. Welcome aboard, Gill Vans."
                    $hide(gilvan)

            "Ingrid (Tanker/Mage) for 6,000 credits" if renpy.random.randint(0, 1000) <= 700 and not ingrid in party:
                if gp < 6000:
                    e "We're too poor to do so."
                else:
                    $gp-=6000
                    $ ingridname="Ingrid"
                    call add_member(ingrid)
                    $show(ingrid)
                    eingrid "Hello folks!"
                    $hide(ingrid)

            "Anna Gold (Tanker) for 6,500 credits" if renpy.random.randint(0, 1000) <= 400 and not anacma in party: # TODO
                if gp < 6500:
                    e "We're too poor to do so."
                else:
                    $gp-=6500
                    $ anacmaname="Anna Gold" # Or anna golden?
                    call add_member(anacma)
                    $show(anacma)
                    eanacma "Hello there. Let's do our best."
                    $hide(anacma)

            "Sophia Wisdom (Ranger) for 5,000 credits" if renpy.random.randint(0, 1000) <= 300 and not sophia in party and not sophia_mood:
                if gp < 5000:
                    e "We're too poor to do so."
                else:
                    $gp-=5000
                    call add_member(sophia)

        jump scene_tavern















label scene_tavern2:
    window hide
    python:
        if (gtavern_mode == 'TAVERN'):
            tlist=[yasmin, vermed, gilvan, ingrid, anacma, sophia]
            for i in tlist:
                if i in party:
                    tlist.remove(i)
        elif (gtavern_mode == 'SOCIAL'):
            # Ops, we must handle events first
            for x in gstory_ctrl:
                if 'bs_' in x:
                    gstory_ctrl.remove(x)
                    renpy.call_in_new_context(x)
                    break

            # Handle the bar
            tlist=[]
            for i in party:
                if i not in tlist:
                    # Blacklist, either Res. 1710122347 (LCKDWN) or will never be at tavern to drink
                    if (not 'beer' in i.dislike) and (not 'LCKDWN' in i.dislike):
                        tlist.append(i)

        if config.developer: # Debugging Purposes: Include key characters here too (we may have missed them with so much debugging)
            tlist.append(cecile)
            tlist.append(savium)
            tlist.append(mamede)


        ui.frame(xmaximum=.9, ymaximum=.9, yalign=.5, xalign=.5)
        ui.vbox()
        ui.text("Current Active Party: %d/%d" % (len(battling), g_maxbt), font='f/Imperator.ttf')
        ui.text("Gold: %d" % (gp), font='f/Imperator.ttf')
        ui.frame(background="#0009")
        tavernvp=ui.viewport(xmaximum=.9, xminimum=600, ymaximum=600, mousewheel=True, draggable=True, background="#0009")
        ui.hbox()

        for i in tlist:
            if renpy.random.random() < 0.7 or not config.developer:
                ui.vbox(xminimum=300, xmaximum=300)
                if renpy.exists("gfx/portrait/" + i.pot.replace(' ', '-') + ".png"):
                    ui.at(tavern_transf)
                    ui.image('gfx/portrait/'+i.pot.replace(' ', '-')+'.png') # ...Works... Somewhat...

                ui.text(i.name, size=22, font="f/Imperator.ttf")
                ui.text("Level %d" % (i.level), size=18, font="f/Imperator.ttf")
                ui.text(ordem_to_text(i.ordem), size=16, font="f/Imperator.ttf")
                if not i in party:
                    ui.bar(5, 5, xmaximum=100, xminimum=100, ymaximum=5, left_bar="#0F0", right_bar="#0F0")
                    ui.bar(5, 5, xmaximum=100, xminimum=100, ymaximum=5, left_bar="#00F", right_bar="#00F")
                else:
                    ui.bar(i.maxhp, i.hp, xmaximum=100, xminimum=100, ymaximum=5, left_bar="#0F0", right_bar="#333")
                    ui.bar(i.maxmp, i.mp, xmaximum=100, xminimum=100, ymaximum=5, left_bar="#00F", right_bar="#333")
                    ui.bar(100, i.aff, xmaximum=100, xminimum=100, ymaximum=5, left_bar="#F6F", right_bar="#333") # +renpy.random.randint(-5, 5)
                ui.text('HP: %d' % (i.maxhp), size=14)
                ui.text('MP: %d' % (i.maxmp), size=14)

                if not i in party:
                    price=5000*len(i.ordem)
                    ui.text('Cost: %d Col' % (price), font='f/unifont.ttf')
                    if gp >= price:
                        ui.textbutton('Recruit', clicked=ui.returns(i))
                    else:
                        ui.textbutton('Recruit', clicked=None)
                else: # TODO: Not sure if this is ready for release yet
                    price=50*(i.aff+5)
                    ui.text('Beer: %d Col' % (price), font='f/unifont.ttf')
                    if gp >= price:
                        ui.textbutton('Buy Beer', clicked=ui.returns(i))
                    else:
                        ui.textbutton('Buy Beer', clicked=None)

                ui.close()



        ui.close()
        ui.bar(adjustment=tavernvp.xadjustment, xmaximum=600, xminimum=600, style='scrollbar') # TODO: Why it doesn't works? =/
        ui.null(height=2)
        ui.hbox()
        ui.textbutton("{image=gfx/arrow.png}Abort", clicked=ui.returns(0))
        if config.developer or persistent.premium:
            ui.textbutton("{image=gfx/command.png}Reload List", clicked=ui.returns(-1))
            ui.textbutton("{image=gfx/command.png}Switch to Legacy", clicked=ui.returns(1))
        ui.close()


        ui.close()
        Taction=ui.interact(suppress_overlay=True)


    window show
    if type(Taction) != int:
        if Taction not in party:
            $price=5000*len(Taction.ordem)
            menu:
                "Recruit [Taction.name] for [price] Credits?"
                "Yes" if gp >= price:
                    $gp-=price
                    call add_member(Taction)
                    python:
                        try:
                            renpy.call_in_new_context('tv_'+Taction.name.lower().replace(' ', '_').replace('.', ''))
                        except:
                            pass
                    jump scene_tavern2
                "No":
                    jump scene_tavern2
        else:
            $ price=150*(Taction.aff+5)
            menu:
                "Buy beer for [Taction.name] for [price] Credits?"
                "Yes" if gp >= price:
                    $gp-=price
                    $Taction.aff+=renpy.random.randint(-1,2)
                    python:
                        try:
                            renpy.call_in_new_context('td_'+Taction.name.lower().replace(' ', '_').replace('.', ''))
                        except:
                            pass
                    jump scene_tavern2
                "No":
                    jump scene_tavern2

    $ renpy.block_rollback()
    if Taction > 0:
        $ persistent.legacyTavern=True
        jump scene_tavern
    elif Taction < 0:
        jump scene_tavern2

    $ show_money=False
    hide Restaurant with dissolve
    jump dev_beta

