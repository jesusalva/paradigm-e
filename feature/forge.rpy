########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Utilities for forge (scene_ctrl_forge)
# TODO Code is corrupted
init python:
    #class ForgeItem:
    #  def __init__(self, money, coal=0, iron=0, wood=0, alum=0, copp=0, hmet=0, stel=0, gold=0):
    #    self.Money=money
    #
    #    # TODO ForgeItem: This is hardcoded by default! Refer to database.rpy
    #    self.Coal           =coal
    #    self.IronScrap      =iron
    #    self.ManaTreeBranch =wood
    #    self.Aluminium      =alum
    #    self.Copper         =copp
    #    self.HolyMetal      =hmet
    #    self.Steel          =stel
    #    self.Gold           =gold

    def ForgeItem(money, coal=0, iron=0, wood=0, alum=0, copp=0, hmet=0, stel=0, gold=0):
        return [money, coal, iron, wood, alum, copp, hmet, stel, gold]

    def forgeadj_gp(x):
         global gpval
         gpval = x

    def forge_info(what, tag):
        if tag == "ARMOR_HEAVY":
            if what == "NAME":
                return "Heavy Armor"
            elif what == "IMG":
                return "gfx/items/forge/armor_heavy.png"
            elif what == "CLASS":
                return "D"
            elif what == "COST":
                return ForgeItem(20000, coal=15, iron=10, wood=1, alum=5, copp=2, hmet=5, stel=8, gold=3)
            elif what == "FACTOR":
                return [1.0, 1.5] # Abs, MaxAbs
        if tag == "ARMOR_LIGHT":
            if what == "NAME":
                return "Light Armor"
            elif what == "IMG":
                return "gfx/items/forge/armor_light.png"
            elif what == "CLASS":
                return "D"
            elif what == "COST":
                return ForgeItem(10000, coal=5, iron=5, wood=5, alum=5, copp=5, hmet=0, stel=0, gold=5)
            elif what == "FACTOR":
                return [1.5, 1.0] # Abs, MaxAbs
        if tag == "SHIELD_HEAVY":
            if what == "NAME":
                return "Heavy Shield"
            elif what == "IMG":
                return "gfx/items/forge/shield_heavy.png"
            elif what == "CLASS":
                return "S"
            elif what == "COST":
                return ForgeItem(20000, coal=5, iron=5, wood=5, alum=5, copp=5, hmet=2, stel=3, gold=5)
            elif what == "FACTOR":
                return [1.0, 1.5] # Abs, MaxAbs
        if tag == "SHIELD_LIGHT":
            if what == "NAME":
                return "Light Shield"
            elif what == "IMG":
                return "gfx/items/forge/shield_light.png"
            elif what == "CLASS":
                return "S"
            elif what == "COST":
                return ForgeItem(10000, coal=5, iron=5, wood=5, alum=5, copp=5, hmet=0, stel=0, gold=5)
            elif what == "FACTOR":
                return [1.5, 1.0] # Abs, MaxAbs
        if tag == "SWORD":
            if what == "NAME":
                return "Regular Sword"
            elif what == "IMG":
                return "gfx/items/forge/wpn_sword.png"
            elif what == "CLASS":
                return "A"
            elif what == "COST":
                return ForgeItem(10000, coal=5, iron=5, wood=5, alum=5, copp=5, hmet=3, stel=2, gold=5)
            elif what == "FACTOR":
                return [1.0, 1.0, 1.0] # DMG, ACC, COST
        if tag == "SABER":
            if what == "NAME":
                return "Saber Sword"
            elif what == "IMG":
                return "gfx/items/forge/wpn_saber.png"
            elif what == "CLASS":
                return "A"
            elif what == "COST":
                return ForgeItem(20000, coal=9, iron=8, wood=5, alum=7, copp=5, hmet=7, stel=6, gold=10)
            elif what == "FACTOR":
                return [1.2, 1.2, 0.85] # DMG, ACC, DISCOUNT
        if tag == "BASTARD":
            if what == "NAME":
                return "Bastard Sword"
            elif what == "IMG":
                return "gfx/items/forge/wpn_bastard.png"
            elif what == "CLASS":
                return "A"
            elif what == "COST":
                return ForgeItem(15000, coal=10, iron=10, wood=5, alum=5, copp=5, hmet=5, stel=5, gold=5)
            elif what == "FACTOR":
                return [1.5, 0.9, 1.0] # DMG, ACC, COST
        if tag == "DAGGER":
            if what == "NAME":
                return "Dagger Sword"
            elif what == "IMG":
                return "gfx/items/forge/wpn_dagger.png"
            elif what == "CLASS":
                return "A"
            elif what == "COST":
                return ForgeItem(5000, coal=5, iron=5, wood=3, alum=2, copp=1, hmet=1, stel=0, gold=2)
            elif what == "FACTOR":
                return [0.9, 1.5, 1.1] # DMG, ACC, DISCOUNT
        return "SEGMENTATION FAULT::ForgeInfo"

















label scene_ctrl_forge_old:
    window hide
    python:
        renpy.block_rollback()
        ui.window(xalign=.6, yalign=.5, xfill=False, yfill=False, ymaximum=0.9)
        ui.hbox()
        jrncslvp=ui.viewport(xfill=False, mousewheel=True, draggable=False) # what's with the silly name
        ui.vbox()
        ui.text("Gold: [gp]")
        ui.null(height=5)
        ui.text("Armors", font="f/ShadowedBlack.ttf")
        ui.null(height=5)
        ui.hbox() # ++ armor heavy
        ui.imagebutton("gfx/items/forge/armor_heavy.png", clicked=ui.returns("ARMOR_HEAVY"))
        ui.vbox()
        ui.text("Craft Heavy Armor", font="f/Imperator.ttf")
        ui.text("High maximum absorb", size=18)
        ui.close()
        ui.close() # -- armor heavy
        ui.hbox() # ++ armor light
        ui.imagebutton("gfx/items/forge/armor_light.png", clicked=ui.returns("ARMOR_LIGHT"))
        ui.vbox()
        ui.text("Craft Light Armor", font="f/Imperator.ttf")
        ui.text("High absorb %%", size=18)
        ui.close()
        ui.close() # -- armor heavy

        ui.null(height=5)
        ui.text("Shields", font="f/ShadowedBlack.ttf")
        ui.null(height=5)
        ui.hbox() # ++ shield heavy
        ui.imagebutton("gfx/items/forge/shield_heavy.png", clicked=ui.returns("SHIELD_HEAVY"))
        ui.vbox()
        ui.text("Craft Heavy Shield", font="f/Imperator.ttf")
        ui.text("High maximum absorb", size=18)
        ui.close()
        ui.close() # -- shield heavy
        ui.hbox() # ++ shield light
        ui.imagebutton("gfx/items/forge/shield_light.png", clicked=ui.returns("SHIELD_LIGHT"))
        ui.vbox()
        ui.text("Craft Light Shield", font="f/Imperator.ttf")
        ui.text("High absorb %%", size=18)
        ui.close()
        ui.close() # -- shield heavy

        ui.null(height=5)
        ui.text("Swords", font="f/ShadowedBlack.ttf")
        ui.null(height=5)
        ui.hbox() # ++ Normal Sword
        ui.imagebutton("gfx/items/forge/wpn_sword.png", clicked=ui.returns("SWORD"))
        ui.vbox()
        ui.text("Craft Normal Sword", font="f/Imperator.ttf")
        ui.text("Average in everything", size=18)
        ui.close()
        ui.close() # -- Normal Sword
        ui.hbox() # ++ Saber Sword
        ui.imagebutton("gfx/items/forge/wpn_saber.png", clicked=ui.returns("SABER"))
        ui.vbox()
        ui.text("Craft Saber Sword", font="f/Imperator.ttf")
        ui.text("Decent in everything, costly", size=18)
        ui.close()
        ui.close() # -- Saber Sword
        ui.hbox() # ++ Bastard Sword
        ui.imagebutton("gfx/items/forge/wpn_bastard.png", clicked=ui.returns("BASTARD"))
        ui.vbox()
        ui.text("Craft Bastard Sword", font="f/Imperator.ttf")
        ui.text("Damage ++, Precision --", size=18)
        ui.close()
        ui.close() # -- Bastard Sword
        ui.hbox() # ++ Dagger Sword
        ui.imagebutton("gfx/items/forge/wpn_dagger.png", clicked=ui.returns("DAGGER"))
        ui.vbox()
        ui.text("Craft Dagger Sword", font="f/Imperator.ttf")
        ui.text("Damage --, Precision ++", size=18)
        ui.close()
        ui.close() # -- Dagger Sword

        ui.textbutton("{image=gfx/abort.png}Abort", clicked=ui.returns('ABORT'), background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#000"),0,12), hover_background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#555"),0,12))
        ui.close()


        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar', #left_bar="#888", right_bar="#888", xmaximum=4,
        thumb=theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar_thumb.png", "#FFF"),
        left_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12),
        right_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12)
        )

        ui.close()
        forge_return=ui.interact()
    window show

    if forge_return == "ABORT":
        $ show_hpbar=True
        jump dev_beta
    else:
        jump scene_ctrl_forge_core

    # TODO now I need name=ui.input or something
    "TODO. Please report: [forge_return]"
    jump dev_beta

label scene_ctrl_forge_core:
    window hide
    python:
        gpval=0
        renpy.block_rollback()
        ui.window(xalign=.6, yalign=.5, xfill=False, yfill=False, ymaximum=0.9)
        ui.hbox()
        ui.vbox()
        ui.text("Input a name:")
        jrncstx=ui.input(exclude="{}[]@")

        ui.hbox()
        ui.textbutton("{image=gfx/abort.png}", clicked=ui.returns("_@_ABEND_@_"))
        ui.textbutton("{image=gfx/arrow.png}", clicked=ui.returns("_@_OK_@_"))
        ui.close()

        ui.null(height=20)
        ui.text("Name a price (max [gp]):")
        ui.hbox()
        forge_adj = ui.adjustment(gp, 0, changed=forgeadj_gp)
        jrncstp=ui.bar(adjustment=forge_adj, style='scrollbar', xmaximum=0.75)#ui.input(allow="0123456789")
        ui.text("%d" % gpval, size=14)
        ui.close()
        ui.close()

        jrncslvp=ui.viewport(xfill=False, mousewheel=True, draggable=False) # what's with the silly name

        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar', #left_bar="#888", right_bar="#888", xmaximum=4,
        thumb=theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar_thumb.png", "#FFF"),
        left_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12),
        right_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12)
        )

        ui.close()
        _action=ui.interact()
        print str(jrncstp.value)
    window show

    if (_action == "_@_OK_@_" or (not "_@_" in _action)) and (forge_return is not None) and (int(gpval) <= gp):
        $ _name=str(jrncstx.text[1]) # MAGIC

        if len(_name) < 3:
            jump scene_ctrl_forge_core

        $ _tipe=forge_info("NAME", forge_return)
        $ forge_f=(gpval/8)
        $ forge_extrainfo=""

        if forge_info("CLASS", forge_return) == "D":
            $ forge_abs=(forge_f/1000.0)*(forge_info("FACTOR", forge_return)[0]) # Eugh
            if forge_abs > 0.95:
                $ forge_abs=0.95
            $ forge_mab=forge_f*(forge_info("FACTOR", forge_return)[1])
            $ forge_extrainfo="Estimated Defense: %d %% up to %d damage." % (int(forge_abs*100), forge_mab)

        elif forge_info("CLASS", forge_return) == "A":
            $ forge_f*=(forge_info("FACTOR", forge_return)[2])
            $ forge_acc=0.5+(forge_f/1000.0)*(forge_info("FACTOR", forge_return)[1]) # TODO
            if forge_acc > 0.95:
                $ forge_acc=0.95
            $ forge_dmg=forge_f*(forge_info("FACTOR", forge_return)[0])
            $ forge_extrainfo="Estimated Damage: %d-%d\nEstimated Acc: %d %%" % (int(forge_dmg/1.5),int(forge_dmg*2/1.5), int(forge_acc*100))

        $ forge_extrainfo+="\nCost: %d GP" % int(gpval)
        menu:
            "Create [_name], a(n) [_tipe]?\n\n[forge_extrainfo]"
            "_@_NODISPLAY_@_Yes":
                pass
            "Maybe" if config.developer:
                window hide
                call forge_allocation_wpn_preset
                call screen forge_allocation_wpn
                window show
                $ cost=(faw_cp-creation_points)*100
                menu:
                    "You currently plan on spending [cost] GP.\nAtk: [faw_dmg] Acc: [faw_acc]\n\nPlease note that Acc is subject to caps."
                    "Confirm":
                        $ faw_ndg=renpy.random.randint(9, 12)*faw_dmg
                        $ faw_mdg=renpy.random.randint(12, 15)*faw_dmg
                        $ faw_pcc=0.80# I need an exponential function
                        "You did not got [faw_ndg]-[faw_mdg] weapon"
                    "Cancel":
                        pass
            "No":
                pass
    jump scene_ctrl_forge































label forge_allocation_wpn_preset:
    $ creation_points=gp/100
    $ faw_cp=gp/100
    $ faw_dmg=0
    $ faw_acc=0
    return

screen forge_allocation_wpn:

    frame:
        xalign .5
        yalign .5
        has vbox
        hbox:
            text "Points available: [creation_points]" # creation_points: 1 point for 100 GP
        hbox:
            if creation_points == 0:
                textbutton ("Damage") xminimum 200 action None
            else:
                textbutton ("Damage") xminimum 200 action [SetVariable("faw_dmg", faw_dmg + 1), SetVariable("creation_points", creation_points - 1)]
            bar range faw_cp value faw_dmg xmaximum 200
        hbox:
            if creation_points == 0:
                textbutton ("Precision") xminimum 200 action None
            else:
                textbutton ("Precision") xminimum 200 action [SetVariable("faw_acc", faw_acc + 1), SetVariable("creation_points", creation_points - 1)]
            bar range faw_cp value faw_acc xmaximum 200
        hbox:
            textbutton ("Start Over") action [SetVariable("faw_dmg", 0), SetVariable("faw_acc", 0), SetVariable("creation_points", faw_cp)]
            textbutton ("Finished") action Return()





























































# New forge system consists in betting money for Random Generated Weapons.
# You can get lucky or waste money. Major advantage is being able to name your
# weapon. In other words, Project-E's forge is something for story purposes (like
# Italus changing his armor, etc.), where you can order something if you want
# merely for sake of consistence. He'll sell some very powerful weapons (eg.
# Chilling Blade, Elucidator) for less money and some items (eg. Iron)
#
# Those weapons can seriously make your life easier but are limited.
label scene_forge2:
    window hide
    python:
        renpy.block_rollback()
        ui.window(xalign=.6, yalign=.5, xfill=False, yfill=False, ymaximum=0.9)
        ui.hbox()
        jrncslvp=ui.viewport(xfill=False, mousewheel=True, draggable=False) # what's with the silly name
        ui.vbox()
        ui.text(_("Gold: [gp]"))

        ui.null(height=5)
        ui.text(_("Swords"), font="f/ShadowedBlack.ttf")
        ui.null(height=5)

        if forge_lvl == 0:
            ui.hbox() # ++ Dagger Sword
            ui.imagebutton("gfx/items/forge/wpn_dagger.png", clicked=ui.returns("DAGGER"))
            ui.vbox()
            ui.text("Order a Sword", font="f/Imperator.ttf")
            ui.text("Low quality for a high price!", size=18)
            ui.close()
            ui.close() # -- Dagger Sword
        elif forge_lvl == 1:
            ui.hbox() # ++ Normal Sword
            ui.imagebutton("gfx/items/forge/wpn_sword.png", clicked=ui.returns("SWORD"))
            ui.vbox()
            ui.text("Order a Sword", font="f/Imperator.ttf")
            ui.text("Plain sword for plain persons.", size=18)
            ui.close()
            ui.close() # -- Normal Sword
        elif forge_lvl == 2:
            ui.hbox() # ++ Bastard Sword
            ui.imagebutton("gfx/items/forge/wpn_bastard.png", clicked=ui.returns("BASTARD"))
            ui.vbox()
            ui.text("Order a Sword", font="f/Imperator.ttf")
            ui.text("Too heavy to use, don't order!", size=18)
            ui.close()
            ui.close() # -- Bastard Sword
        elif forge_lvl >= 3:
            ui.hbox() # ++ Saber Sword
            ui.imagebutton("gfx/items/forge/wpn_saber.png", clicked=ui.returns("SABER"))
            ui.vbox()
            ui.text("Order a Sword", font="f/Imperator.ttf")
            ui.text("Higher price, higher quality.", size=18)
            ui.close()
            ui.close() # -- Saber Sword


        ui.null(height=5)
        ui.text(_("Armors"), font="f/ShadowedBlack.ttf")
        ui.null(height=5)

        if forge_lvl <= 1:
            ui.hbox() # ++ armor light
            ui.imagebutton("gfx/items/forge/armor_light.png", clicked=ui.returns("ARMOR_LIGHT"))
            ui.vbox()
            ui.text("Order an Armor", font="f/Imperator.ttf")
            ui.text("Would you trust your life to it?", size=18)
            ui.close()
            ui.close() # -- armor light
        elif forge_lvl >= 2:
            ui.hbox() # ++ armor heavy
            ui.imagebutton("gfx/items/forge/armor_heavy.png", clicked=ui.returns("ARMOR_HEAVY"))
            ui.vbox()
            ui.text("Order an Armor", font="f/Imperator.ttf")
            ui.text("Just don't be too rough!", size=18)
            ui.close()
            ui.close() # -- armor heavy

        ui.null(height=5)
        ui.text(_("Shields"), font="f/ShadowedBlack.ttf")
        ui.null(height=5)

        if forge_lvl <= 1:
            ui.hbox() # ++ shield light
            ui.imagebutton("gfx/items/forge/shield_light.png", clicked=ui.returns("SHIELD_LIGHT"))
            ui.vbox()
            ui.text("Order a Shield", font="f/Imperator.ttf")
            ui.text("It won't break during a fight... right?", size=18)
            ui.close()
            ui.close() # -- shield light
        elif forge_lvl >= 2:
            ui.hbox() # ++ shield heavy
            ui.imagebutton("gfx/items/forge/shield_heavy.png", clicked=ui.returns("SHIELD_HEAVY"))
            ui.vbox()
            ui.text("Order a Shield", font="f/Imperator.ttf")
            ui.text("It shouldn't have blind spots.", size=18)
            ui.close()
            ui.close() # -- shield heavy


        ui.textbutton("{image=gfx/abort.png}Abort", clicked=ui.returns('ABORT'), background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#000"),0,12), hover_background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#555"),0,12))
        ui.close()


        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar', #left_bar="#888", right_bar="#888", xmaximum=4,
        thumb=theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar_thumb.png", "#FFF"),
        left_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12),
        right_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12)
        )

        ui.close()
        forge_return=ui.interact()
    window show

    if forge_return == "ABORT":
        $ show_hpbar=True
        jump dev_beta
    else:
        jump scene_forge2_core

    # TODO now I need name=ui.input or something
    "TODO. Please report: [forge_return]"
    jump dev_beta



label scene_forge2_core:
    window hide
    python:
        renpy.block_rollback()
        ui.window(xalign=.6, yalign=.5, xfill=False, yfill=False, ymaximum=0.9)
        ui.hbox()
        jrncslvp=ui.viewport(xfill=False, mousewheel=True, draggable=False) # what's with the silly name
        ui.vbox()
        ui.text(_("Gold: [gp]"))

        ui.null(height=5)
        ui.text(_("Crafting an item:"), font="f/ShadowedBlack.ttf")
        ui.null(height=5)



        ui.hbox() # Main item crafting screen
        ui.imagebutton(forge_info('IMG', forge_return), clicked=None)
        ui.vbox()
        ui.text(forge_info('NAME', forge_return), font="f/Imperator.ttf")
        ui.text(_("Crafting this item will require:"), size=18)
        ui.close()
        ui.close() # -- Main item crafting screen


        lisat=["Bug, report me: lisat@forge", Coal, IronScrap, ManaTreeBranch, Aluminium, Copper, HolyMetal, Steel, Gold]
        #ForgeItem(20000, coal=5, iron=5, wood=5, alum=5, copp=5, hmet=5, stel=5, gold=5)
        i=0
        ALLITEM={}
        FORGETM={}
        cancraft=True

        for ix in lisat[1:]:
            ALLITEM[ix.name]=0
        for ix in hero.items:
            if ix.name in ALLITEM:
                ALLITEM[ix.name]+=1


        while i < len(lisat):
            if i == 0:
                i+=1
                continue
            FORGETM[lisat[i].name]=forge_info('COST', forge_return)[i]
            if forge_info('COST', forge_return)[i] > 0:
                if config.developer and persistent.CheatAutoForge:
                    add_mult_items(forge_info('COST', forge_return)[i], lisat[i])
                if ALLITEM[lisat[i].name] >= forge_info('COST', forge_return)[i]:
                    ui.text("%d/%d %s" % (ALLITEM[lisat[i].name], forge_info('COST', forge_return)[i], lisat[i].name), color="#0f0")
                else:
                    ui.text("%d/%d %s" % (ALLITEM[lisat[i].name], forge_info('COST', forge_return)[i], lisat[i].name), color="#f00")
                    cancraft=False
            i+=1

        ui.text("")
        ui.text(_("%d/%d Gold") % (gp, forge_info('COST', forge_return)[0]))
        if gp < forge_info('COST', forge_return)[0]:
            cancraft=False

        ui.hbox() # ++ Controls
        ui.textbutton(_("{image=gfx/abort.png}Abort"), clicked=ui.returns('ABORT'), background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#000"),0,12), hover_background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#555"),0,12))
        if cancraft:
            ui.textbutton(_("{image=gfx/arrow.png}Craft"), clicked=ui.returns('CRAFT'), background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#000"),0,12), hover_background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#555"),0,12))
        else:
            ui.textbutton(_("{image=gfx/arrow.png}Craft"), clicked=None, background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#111"),0,12), hover_background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#555"),0,12))
        ui.close() # -- controls
        ui.close()


        ui.bar(adjustment=jrncslvp.yadjustment, style='vscrollbar', #left_bar="#888", right_bar="#888", xmaximum=4,
        thumb=theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar_thumb.png", "#FFF"),
        left_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12),
        right_bar=Frame(theme.OneOrTwoColor("../common/_theme_marker/inkvscrollbar.png", "#888"),0,12)
        )

        ui.close()
        forge_return2=ui.interact()
    window show

    if forge_return2 == "ABORT":
        jump scene_forge2
    else:
        jump scene_forge2_name

    # TODO now I need name=ui.input or something
    "TODO. Please report: [forge_return]"
    jump dev_beta


label scene_forge2_name:
    window hide
    python:
        ui.window(xalign=.55, yalign=.5, xfill=False, yfill=False, ymaximum=0.7)
        ui.vbox()

        ui.hbox() # Main item crafting screen
        ui.imagebutton(forge_info('IMG', forge_return), clicked=None)
        ui.vbox()
        ui.text(_("Now Crafting:"), size=18)
        ui.text(forge_info('NAME', forge_return), font="f/Imperator.ttf")
        ui.close()
        ui.close() # -- Main item crafting screen


        ui.text(_("Input a name:"))
        jrncstx=ui.input(exclude="{}[]@", length=15) # TODO: Maybe 15 is too long for an item name!
        
        ui.hbox() # ++ Controls
        ui.textbutton(_("{image=gfx/abort.png}Abort"), clicked=ui.returns('ABORT'), background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#000"),0,12), hover_background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#555"),0,12))
        if cancraft:
            ui.textbutton(_("{image=gfx/arrow.png}Craft"), clicked=ui.returns('CRAFT'), background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#000"),0,12), hover_background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#555"),0,12))
        else:
            ui.textbutton(_("{image=gfx/arrow.png}Craft"), clicked=None, background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#111"),0,12), hover_background=Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#555"),0,12))
        ui.close() # -- controls
        ui.text(_("Already used names cannot be reused"), color="#F00", size=12)
        ui.close()

        forge_return3=ui.interact()
    window show

    if forge_return3 == "ABORT":
        jump scene_forge2
    else:
        $ forgedit_name=str(jrncstx.text[1]) # MAGIC

        if len(forgedit_name) < 3 or forgedit_name in forged_names:
            jump scene_forge2_name
        python:
            forged_names.append(forgedit_name)
            for item in lisat:
                if item == "Bug, report me: lisat@forge":
                    continue
                rm_mult_items(FORGETM[item.name], item)
            gp-=forge_info('COST', forge_return)[0]
            #craftchance=renpy.random.random()

            # Create random item!!!!!!!!!!!!!!
            forge_craft(forge_info('CLASS', forge_return))

        $ show_hpbar=True
        centered "{size=32}{font=f/Imperator.ttf}{color=#26F}Successfully created [forgedit_name]!{/color}{/font}{/size}"
        $ show_hpbar=False
        jump scene_forge2

init python:
    def forge_craft(idn):
            if idn == "A":
                if forge_lvl <= 1:
                    pict = "sword"
                elif forge_lvl <= 2:
                    pict = "crystal sword"
                else:
                    pict = "flame sword"
                tmp_mdg=int(((forge_lvl/2.0)+1)*renpy.random.randint(40, 60))
                hero.items.append(
                Weapon(name=str(forgedit_name) + " (Forged Weapon)", mdmg=tmp_mdg, xdmg=tmp_mdg+renpy.random.randint(5, 35),
                acc=0.60+(renpy.random.random()/4.0), pic=pict))
            elif idn == "S":
                if forge_lvl <= 0:
                    pict = "buckler"
                elif forge_lvl <= 1:
                    pict = "shield"
                elif forge_lvl <= 2:
                    pict = "shield gold"
                else:
                    pict = "shield tower"
                hero.items.append(
                Armor(name=str(forgedit_name) + " (Forged Shield)", abdmg=int(((forge_lvl/2.0)+1)*renpy.random.randint(20, 40)), 
                rate=(forge_lvl/100.0)+(renpy.random.random()/4.0), pic=pict))
            elif idn == "D":
                if forge_lvl <= 1:
                    pict = "armor"
                elif forge_lvl <= 2:
                    pict = "silver armor"
                else:
                    pict = "gold armor"
                hero.items.append(
                Armor(name=str(forgedit_name) + " (Forged Armor)", abdmg=int(((forge_lvl/2.0)+1)*renpy.random.randint(20, 40)), 
                rate=(forge_lvl/100.0)+(renpy.random.random()/4.0), pic=pict))
            else:
                raise Exception("Error, expected forge item to be within [A, S, D] but got \"%s\".\n\nForging item failed.\nReceive a humble Bread." % (forge_info('CLASS', forge_return)))
                hero.items.append(Bread)

