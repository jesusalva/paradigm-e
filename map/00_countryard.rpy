########################################################################################
#     This file is under the MIT license
# Copyright 2017 Jesusalva <cpntb1@ymail.com>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this script and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
########################################################################################
# This files contains labels for dev_beta.
# It is the most basic file for any derivate work. Not under LGPL for convenience
# Please copy this file as you go along, and build your Castle
image basemap l0 = Frame("gfx/none.png")


#################################################
#######   TODO LEVEL 00 LABELS TODO    ##########
#################################################

label dev_beta__outskirt_l0:
    menu:
        "You are on Countryard outskirts."
        "Rest":
            call rest_menu
        "Hunt" if not prologue:
            $ renpy.jump("dev_beta__hunt_l" + str(g_cur_level))      # Allow player to select mobs to fight with.
        "Leave":
            jump dev_beta
    $ renpy.jump("dev_beta__outskirt_l" + str(g_cur_level))


label dev_beta__hunt_l0:
    call load_sentences
    $ HUNT_INTRO_SENTENCE=HUNT_INTRO_SENTENCE.replace("ALV_DO_NOT_REMOVE__FLAG", str(g_cur_level))
    menu:
        "[HUNT_INTRO_SENTENCE]"

        "Exit":
            jump dev_beta

    jump dev_beta

# Shop labels are not required but strongly advised (wstore - astore - hstore - gstore)
label dev_beta__wstore_l0:
    call load_sentences

    # Process gender (sir or madam)
    if ymale:
        $ SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", "sir")
    else:
        $ SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", "madam")

    jump weapon_shop

label dev_beta__astore_l0:
    call load_sentences

    # Process gender (sir or madam)
    if ymale:
        $ SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", "sir")
    else:
        $ SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", "madam")

    $ show_money=True
    "[SHOPS_INTRO_SENTENCE]"
    jump armor_shop


label dev_beta__hstore_l0:
    call load_sentences

    # Process gender (sir or madam)
    if ymale:
        $ SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", "sir")
    else:
        $ SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", "madam")

    $ show_money=True
    "[SHOPS_INTRO_SENTENCE]"
    jump healitem_shop

label dev_beta__gstore_l0:
    call load_sentences

    # Process gender (sir or madam)
    if ymale:
        $ SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", "sir")
    else:
        $ SHOPS_INTRO_SENTENCE=SHOPS_INTRO_SENTENCE.replace("PLG_DO_NOT_REMOVE__FLAG", "madam")

    $ show_money=True
    "[SHOPS_INTRO_SENTENCE]"
    jump generic_shop

label dev_beta__available_quests_on_l0:
    $ chance=renpy.random.random()  # Techinical Explanation: There are a certain amount of chances of metting a NPC player while walking on streets. "CHANCE" variable handles this.


    menu:
        "Welcome to the City." # Money rewards should be always bigger than sell value and lower than buy value.

        # ----------- Core
        "{b}TELETRANSPORT{/b}" if not prologue: # This uses the town teletransport feature.
            call teletransport
        "{b}Sleep at Inn{/b}":  # This uses the town inn feature.
            call sleep_menu

        # ---------- Quests
        "{image=gfx/quest.png}Some quest" if False:
            "Hello. I need 10 Teeths and 05 Fangs. Why do I need they? Just bring them to me and stop asking questions, and I may reward you handsomely."

        # --------- Commerccial
        "{image=gfx/commercial.png}Visit shops on city" if not prologue:
            jump dev_beta__shops


        # --------- Features
        "{image=gfx/map.png}Visit The Academy on city" if not prologue:
            jump dev_beta__increase_stats

        "{image=gfx/map.png}Visit The Magic Academy" if not prologue and g_act_level >= 4:
            jump dev_beta__skupd

        "{image=gfx/map.png}Visit Tavern" if "TAVERN" in gstory_ctrl:
            $gtavern_mode='TAVERN'
            jump scene_ctrl_tavern

        "{image=gfx/map.png}Visit Bar" if "TAVERN" in gstory_ctrl:
            $gtavern_mode='SOCIAL'
            jump scene_ctrl_tavern

        "{image=gfx/map.png}Visit Forge" if "FORGE" in gstory_ctrl:
            jump scene_ctrl_forge

        "Return to City Entrance":
            pass
    jump dev_beta




##################################################
##################################################
#################### DUNGEONS ####################
##################################################
##################################################
label dev_beta__dungeon_l0:

    $ show_bossb=False
    $ additional_text=""

    if dungeon_state < 10:
        scene basemap l0 with None
    else: # After the boss is defeated, we might want to display something else
        scene basemap l0 with None

    if dungeon_state <= 10:
        $ renpy.show("dungeon l0 d%02d" % dungeon_state)

    if not prologue:
        centered "{color=#F00}Leaving Area: [g_cur_level]{/color}{fast}"
    else:
        centered "{color=#F00}Leaving Area: [g_cur_level]{fast}\nCurrently, impossible to flee.{/color}"

    if dungeon_state <= 10:
        $ additional_text=""

    # This is the quick skip system, so if you left boss behind to restock you need to walk less
    if quick_skip_dg >= 0:
        $ dungeon_state=4

    menu:
        "Continue" if (not just_won_boss_fight) and (g_act_level <= 0 or (dungeon_state < 2)):
            $ dungeon_state+=1
        # Other options; Retreat here is not instantaneous (we may have trouble!)
        "Retreat" if (not just_won_boss_fight) and (not prologue):
            "You decided to return to previous city.{nw}"
            if dungeon_state > 4:
                extend " You've walked a long way, it'll be a long return."
            else:
                extend " It shouldn't be too far now."
            if dungeon_state <= 2:
                $ dungeon_state=0
                jump dev_beta
            else:
                $ dungeon_state-=2
        # Developers: Please use @warp command!
        "Open next portal and acess next area" if just_won_boss_fight:
            ## Uncomment next line to have dialog after boss is won
            #$ g_story_lbl="scene1"
            $ dungeon_state=0
            $just_won_boss_fight=False
            $g_cur_level+=1
            if g_act_level <= 0: # XXX: Check if we need to unlock more levels. (g_act_level)
                $ g_act_level=1
            $gcl_name=assign_city_name(1)
            $ save_name=_("Area ") + str(g_cur_level) + ': ' + str(gcl_name) + "\n"   # Set save name.

            jump dev_beta   # Jump to THIRD FLOOR!! JK

    $ renpy.block_rollback()
    # This is old music code. Fell free to uncomment
    #$ plmusic=renpy.random.choice([MUSIC_SF01, MUSIC_SF03, MUSIC_SF02, MUSIC_SF02, MUSIC_SF03, MUSIC_SF04])
    #play music plmusic fadein 0.7 fadeout 0.3

    # Set dungeon music to play non-stop
    if renpy.music.get_playing() != MUSIC_ADVENTURING_SONG:
        play music MUSIC_ADVENTURING_SONG fadein 1.0

    # Dungeon Events
    if dungeon_state == 3:
        # A simple dialog
        $ show(jhonny, dissolve, at=[left])
        ejhonny "Meh, I'm tired from fighting."
        $ hide(jhonny)
    elif dungeon_state == 5:
        # When we need to update Quest Log
        $ ql.update_quest("00 Quest Entry", "Quest Capitalization", "This is a quest update. It even opens the quest!")
        $ ql.close("00 Quest Entry") # Closes the quest entry with this name
    # Scene 10 is provided as a rest scene, fell free to remove or renumber
    elif dungeon_state == 7:
        call scene10
    # Before boss fight event
    elif dungeon_state == 9:
        window hide
        scene battlebg darkland with Dissolve(1.5)
        window show
        e "Boss ahead!"
        play music "<from 20>"+MUSIC_SOCIAL08 fadein 1.0
        show EN_ASSASSIN full with dissolve
        $ assassname="The Paradigm Boss"
        eassass "Yes. Challenge me!"

    # Generate fights - until the boss
    if dungeon_state <= 8:
        # Update battle bg
        if dungeon_state < 5:
            $ combat_bg="greenhill"
            $ dg2_names=[['Armored Highwayman', 'armored-highwayman'], ['Bowman', 'bowman'], ['Soldier', 'corporal'], ['Horseman', 'horseman']]
            $ addval=renpy.random.randint(1, 2)
        else:
            $ combat_bg="darkland"
            $ dg2_names=[['Automaton', 'automaton'], ['One Hit One Killer', 'caveman'], ['Destroyer', 'destroyer'], ['Strange Warrior', 'Avenger']]
            $ addval=renpy.random.randint(1, 2)

        # 85% random attack chances
        if renpy.random.random() <= 0.85:
            $ tmp_en_len=renpy.random.randint(1,2)+int((dungeon_state+addval)/4)
            $ EnemyParty=[]
            python:
                i=0
                while i < tmp_en_len:
                    i+=1
                    tmp_ex=renpy.random.choice(dg2_names)
                    # 10% chance of boss monster. Note the renpy.random.randint() is what determines the monster strength
                    if renpy.random.random() < 0.1:
                        EnemyParty.append(reg("Titan "+tmp_ex[0], renpy.random.randint(40, 50)+int(dungeon_state*1.5), pic=tmp_ex[1], rgs=True, dl=7, aispec=[renpy.random.choice(['STRONGEST', 'WEAKEST', 'THREAT']), 0.15])) # Titan Monsters are at least smart...
                    else:
                        EnemyParty.append(reg(tmp_ex[0], renpy.random.randint(40, 45)+int(dungeon_state*1.5), dl=2, pic=tmp_ex[1]))
            $ role_advisor=renpy.random.choice(party)
            "[role_advisor.name]" "Watch out! An attack!"
            jump setup_fight
        else:
            $ role_advisor=renpy.random.choice(party)
            "[role_advisor.name]" "I'm bored."

        jump dev_beta__dungeon_l0




    # ---------------------------------------------------------------------------
    # Just some old code
    play music MUSIC_SF04 fadein 0.7 fadeout 0.3
    centered "BOSS\nFIGHT{fast}"
    call heal_crystal
    menu:
        "Take it on, take it over! Defeat the boss, and go to next area!\nTake it on?{fast}"
        "Take it on":
            $ prologue=False

            centered "{color=#FF0000}BOSS FIGHT{/color}{p=1.0}\nIt's action time!"
            hide EN_ASSASSIN with dissolve

            $ gstory_ctrl.append('COMBAT_OVERRIDE')
            $ gstory_ctrl.append('COR_1_@_CS_B03_S1')
            $ gstory_ctrl.append('COF_'+l3_boss.name+'_@_CS_B03_F')
            $ gstory_ctrl.append('COM_@_'+l3_boss.name+'_@_CS_B03_M')

            $l3_boss.mp=650
            $l3_boss.maxmp=650
            #$l3_boss.aispec="WEAKEST" # No AI?
            $l3_boss.aiskpec=["HEALER_67", "USE_MANA"] # May heal when HP < 67%
            $l3_boss.skill.append(retrieve_skill_from_db("Power Slash"))
            $l3_boss.skill.append(retrieve_skill_from_db("Poisoned Dagger"))
            $l3_boss.skill.append(retrieve_skill_from_db("Stun Potion"))
            $l3_boss.skill.append(retrieve_skill_from_db("Heal Potion"))

            $ EnemyParty=[l3_boss]
            $ show_bossb=True
            $ just_won_boss_fight=True
            $ renpy.block_rollback()
            jump setup_fight

        "Leave it! I'm not ready.": # This needs rework and is not used
            $ dungeon_state=0
            "Congratulations, you've reached boss room.\nUnhapply, you couldn't fight with it."
            if quick_skip_dg < 0: # Without maintenance, this is broken code :(
                extend "\nLet's move on. You gained some credits."
                $ gp += g_cur_level*10
                $ quick_skip_dg=0
                $ hero.items.append(HealingCRT)
            pass
    jump dev_beta

# TODO: WARNING: MAXIMUM ABSORB VALUES FOR ANY SHIELD IS 60%, MAXIMUM ABSORB VALUES FOR ANY ARMOR IS 40%.
