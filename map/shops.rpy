########################################################################################
#     This file is part of Castle and Project E.
#     Copyright (C) 2015-2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Uniformize a single shop, where options are displayed according to g_cur_level

init python:
    def shoplv(a=0, b=-1):
        if b < 0:
            b=a # Means only one, to avoid using shoplv(0,0)
        if g_cur_level >= a and g_cur_level <= b:
            return True
        else:
            return False

label weapon_shop:
    $ show_money=True
    menu:
        # Normal Swords
        "Buy regular sword for 500 credits.\nDamage: 20-40  Precision: 75%%" if shoplv(1) and hero.level>=1:
            $ buy_item(500, Sword, "WPN")

        "Buy good sword for 1,000 credits.\nDamage: 30-50  Precision: 75%%" if shoplv(1) and hero.level>=2:
            $ buy_item(1000, GSword, "WPN")

        "Buy excellent sword for 2,000 credits.\nDamage: 40-60  Precision: 75%%" if shoplv(1) and hero.level>=3:
            $ buy_item(2000, ESword, "WPN")

        "Buy Baby Dark Killer for 3,000 credits.\nDamage: 50-70  Precision: 75%%" if shoplv(2) and hero.level>=4:
            $ buy_item(3000, BDarkiller, "WPN")

        "Buy Dark Killer Prototype for 4,000 credits.\nDamage: 60-80  Precision: 75%%" if shoplv(2) and hero.level>=5:
            $ buy_item(4000, PDarkiller, "WPN")

        "Buy Dark Killer for 5,000 credits.\nDamage: 70-90  Precision: 75%%" if shoplv(3) and hero.level>=6:
            $ buy_item(5000, Darkiller, "WPN")

        "Buy White Lily for 7,500 credits.\nDamage: 90-120  Precision: 75%%" if shoplv(3) and hero.level>=7:
            $ buy_item(7500, WhiteLily, "WPN")

        "Buy White Prince for 9,000 credits.\nDamage: 120-140  Precision: 75%%" if shoplv(4) and hero.level>=8:
            $ buy_item(9000, WhitePrince, "WPN")

        "Buy White Queen for 11,000 credits.\nDamage: 140-160  Precision: 75%%" if shoplv(4) and hero.level>=9:
            $ buy_item(11000, WhiteQueen, "WPN")

        "Buy White King for 15,000 credits.\nDamage: 160-180  Precision: 75%%" if shoplv(5) and hero.level>=10:
            $ buy_item(15000, WhiteKing, "WPN")


        # Daggers
        "Buy a Blind Dagger for 500 credits.\nDamage: 15-36  Precision: 85%%" if shoplv(1) and hero.level>=1:
            $ buy_item(500, Dagger, "WPN")

        "Dagger for 1,000 credits\nDamage: 25-45  Precision: 86%%" if shoplv(2) and hero.level>=2:
            $ buy_item(1000, DaggeR, "WPN")

        "Sharp Dagger for 2,000 credits\nDamage: 35-55  Precision: 87%%" if shoplv(2) and hero.level>=3:
            $ buy_item(2000,  SDagger, "WPN")

        "Good Dagger for 3,000 credits.\nDamage: 45-65  Precision: 88%%" if shoplv(3) and hero.level>=4:
            $ buy_item(3000, GDagger, "WPN")

        "Excellent Dagger for 4,000 credits.\nDamage: 55-75  Precision: 89%%" if shoplv(3,4) and hero.level>=5:
            $ buy_item(4000, EDagger, "WPN")

        "Master Dagger for 5,000 credits.\nDamage: 65-85  Precision: 90%%" if shoplv(4) and hero.level>=6:
            $ buy_item(5000, MDagger, "WPN")


        # Sabers
        "Buy a pratice Saber for 1,000 credits.\nDamage: 25-45  Precision: 80%%" if shoplv(1) and hero.level>=2:
            $ buy_item(1000, Saberb, "WPN")

        "Basic Saber for 2,500 credits\nDamage: 40-60  Precision: 80%%" if shoplv(2) and hero.level>=3:
            $ buy_item(2500,  Sabern, "WPN")

        "Saber Grade E for 4,500 credits\nDamage: 50-69  Precision: 80%%" if shoplv(2) and hero.level>=4:
            $ buy_item(4500,  SaberE, "WPN")

        "Saber Grade D for 5,500 credits\nDamage: 60-79  Precision: 80%%" if shoplv(3) and hero.level>=5:
            $ buy_item(5500,  SaberD, "WPN")

        "Saber Grade C for 6,500 credits\nDamage: 70-89  Precision: 80%%" if shoplv(3,4) and hero.level>=6:
            $ buy_item(6500,  SaberC, "WPN")

        "Saber Grade B for 7,500 credits\nDamage: 80-99  Precision: 80%%" if shoplv(4) and hero.level>=7:
            $ buy_item(7500,  SaberB, "WPN")

        "Saber Grade A for 8,500 credits\nDamage: 90-119  Precision: 80%%" if shoplv(5) and hero.level>=8:
            $ buy_item(8500,  SaberA, "WPN")


        # Bastard Swords
        "Buy a pratice bastard sword for 1,000 credits.\nDamage: 20-50  Precision: 65%%" if shoplv(1) and hero.level>=2:
            $ buy_item(1000, TwoHandsP, "WPN")

        "Basic Bastard Sword for 3,500 credits.\nDamage: 40-80  Precision: 65%%" if shoplv(2) and hero.level>=3:
            $ buy_item(3500, TwoHandsB, "WPN")

        "Bastard Sword for 5,500 credits.\nDamage: 60-100  Precision: 65%%" if shoplv(3) and hero.level>=4:
            $ buy_item(5500, TwoHands, "WPN")

        "Blood Slasher for 7,500 credits.\nDamage: 80-120  Precision: 65%%" if shoplv(4) and hero.level>=5:
            $ buy_item(7500, TwoBlood, "WPN")

        "Fourth Killer for 9,500 credits.\nDamage: 100-140  Precision: 65%%" if shoplv(5) and hero.level>=6:
            $ buy_item(9500, FourKill, "WPN")


        "Buy Nothing":
            e "Come back soon!"
            $ show_money=False
            jump dev_beta__shops
            #jump weapon_shop

    jump weapon_shop












label armor_shop:
    menu:
        # Shields
        "Buy a buckler for 1,000 credits.\nAbsorbs 15%% of damage taken, up to 40." if shoplv(1,2) and hero.level>=2:
            $ buy_item(1000, Buckler, "OFF")

        "Buy a basic shield for 2,500 credits.\nAbsorbs 20%% of damage taken, up to 80." if shoplv(2,3) and hero.level>=3:
            $ buy_item(2500, Shield, "OFF")

        "Buy a regular shield for 3,000 credits.\nAbsorbs 21%% of damage taken, up to 100." if shoplv(3,4) and hero.level>=4:
            $ buy_item(3000, Std_Shd, "OFF")

        "Buy a bronze shield for 6,000 credits.\nAbsorbs 24%% of damage taken, up to 120." if shoplv(4,5) and hero.level>=5:
            $ buy_item(6000, Bronze_Shd, "OFF")

        "Buy a tower shield for 14,000 credits.\nAbsorbs 35%% of damage taken, up to 100." if shoplv(5) and hero.level>=6:
            $ buy_item(14000, Tower_Shd, "OFF")


        # Armors
        "Buy a starter armor for 500 credits.\nAbsorbs 10%% of damage taken, up to 15." if shoplv(1) and hero.level>=0:
            $ buy_item(500, Starter_Armor, "ARM")

        "Buy a regular armor for 1,200 credits.\nAbsorbs 12%% of damage taken, up to 40." if shoplv(1,2) and hero.level>=2:
            $ buy_item(1200, Normal_Armor, "ARM")

        "Buy a reinforced armor for 2,500 credits.\nAbsorbs 15%% of damage taken, up to 80." if shoplv(2,3) and hero.level>=3:
            $ buy_item(2500, Reinforced_Armor, "ARM")

        "Buy a leather armor for 3,500 credits.\nAbsorbs 20%% of damage taken, up to 60." if shoplv(3,4) and hero.level>=4:
            $ buy_item(3500, Leather_Armor, "ARM")

        "Buy an iron armor for 4,000 credits.\nAbsorbs 20%% of damage taken, up to 80." if shoplv(4,5) and hero.level>=5:
            $ buy_item(4000, Iron_Armor, "ARM")

        "Buy a steel armor for 7,000 credits.\nAbsorbs 25%% of damage taken, up to 100." if shoplv(5) and hero.level>=6:
            $ buy_item(7000, Steel_Armor, "ARM")


        "Buy Nothing":
            e "Come back soon!"
            $ show_money=False
            jump dev_beta__shops

    jump armor_shop













label healitem_shop:
    menu:
        "Buy one Mana Potion by 200 credits. (+50 MP)" if shoplv(0,5) and hero.level >= 2:
            call dev_beta__buymult(ManaPot, 200)
        "Buy one Bread by 30 credits. (+25 HP)" if shoplv(0,1) and hero.level >= 1:
            call dev_beta__buymult(Bread, 30)
        "Buy one Sandwich by 60 credits. (+50 HP)" if shoplv(0,2) and hero.level >= 1:
            call dev_beta__buymult(Sandwich, 60)
        "Buy one Minor Healing Pot by 120 credits. (+100 HP)" if shoplv(0,3) and hero.level >= 2:
            call dev_beta__buymult(MinorPot, 120)
        "Buy one Medium Healing Pot by 300 credits. (+250 HP)" if shoplv(0,4) and hero.level >= 3:
            call dev_beta__buymult(MedPot, 300)
        "Buy one Big Healing Pot by 600 credits. (+500 HP)" if shoplv(1,5) and hero.level >= 3:
            call dev_beta__buymult(BigPot, 600)
        "Buy one Mega Healing Pot by 1200 credits. (+1000 HP)" if shoplv(2,5) and hero.level >= 5:
            call dev_beta__buymult(MegPot, 1200)
        "Buy one Super Healing Pot by 1800 credits. (+1500 HP, +25 MP)" if shoplv(2,5) and hero.level >= 5:
            call dev_beta__buymult(SupPot, 1800)
        "Buy one Sacred Healing Pot by 3200 credits. (+3000 HP, +50 MP)" if shoplv(3,5) and hero.level >= 7:
            call dev_beta__buymult(SacPot, 3200)
        "Buy one Healing Crystal for 2500 credits. (full party HP/MP heal, Battle: X)" if shoplv(2,5):
            call dev_beta__buymult(HealingCRT, 2500)
        "Buy Nothing":
            "Come back soon!"
            $ show_money=False
            jump dev_beta__shops

    jump healitem_shop












# Needs to be thought again!
label generic_shop:
    menu:
        "Buy one Fang for 20 credits." if shoplv(0,5):
            $ buy_item(20, Fang)
        "Buy one Teeth for 25 credits." if shoplv(1,5):
            $ buy_item(25, Teeth)
        "Buy one Fur for 25 credits." if shoplv(1,5):
            $ buy_item(25, Fur)
        "Buy Coal for 25 credits." if shoplv(3,5):
            $ buy_item(25, Coal)
        "Buy one Glass Shard for 40 credits." if shoplv(2,5):
            $ buy_item(60, Glass)
        "Buy one Glass for 60 credits." if shoplv(4,5):
            $ buy_item(60, Glass)
        "Buy Nothing":
            "Come back soon!"
            $ show_money=False
            jump dev_beta__shops

    jump generic_shop

