########################################################################################
#     This file is part of Castle.
#     Copyright (C) 2015  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# This script file makes sure that our customized HP bars work. :)

init -5 python:
    #create our bar -----------------------
    style.hp_bar = Style(style.default)
    style.hp_bar.xalign = 0.5
    style.hp_bar.xmaximum = 770 # bar width
    style.hp_bar.ymaximum = 66  # bar height
    style.hp_bar.left_gutter = 11
    style.hp_bar.right_gutter = 12

    style.hp_bar.left_bar = Frame("ui/hpbar_full.png", 0, 0)          # Image when you're full of HP
    style.hp_bar.right_bar = Frame("ui/hpbar_empty.png", 0, 0)        # Image when you have zero HP
    style.hp_bar.hover_left_bar = Frame("ui/hpbar_full.png", 0, 0)    # Image when you hover the bar. Often brighter.
   
    style.hp_bar.thumb = "gfx/none.png"                             # Navigator. (thumb) on our case, nothing.
    style.hp_bar.thumb_shadow = None
    style.hp_bar.thumb_offset = 5


    #create a red bar variation -----------------------
    style.hp_rbar = Style(style.default)
    style.hp_rbar.xalign = 0.5
    style.hp_rbar.xmaximum = 770 # bar width
    style.hp_rbar.ymaximum = 66  # bar height
    style.hp_rbar.left_gutter = 11
    style.hp_rbar.right_gutter = 12

    style.hp_rbar.left_bar = Frame("ui/hpbar_red.png", 0, 0)            # Image when you're full of HP
    style.hp_rbar.right_bar = Frame("ui/hpbar_empty.png", 0, 0)         # Image when you have zero HP
    style.hp_rbar.hover_left_bar = Frame("ui/hpbar_red.png", 0, 0)      # Image when you hover the bar. Often brighter.
   
    style.hp_rbar.thumb = "gfx/none.png"                             # Navigator. (thumb) on our case, nothing.
    style.hp_rbar.thumb_shadow = None
    style.hp_rbar.thumb_offset = 5

    #create a yellow bar variation -----------------------
    style.hp_ybar = Style(style.hp_bar)

    style.hp_ybar.left_bar = Frame("ui/hpbar_yellow.png", 0, 0)            # Image when you're full of HP
    style.hp_ybar.right_bar = Frame("ui/hpbar_empty.png", 0, 0)            # Image when you have zero HP
    style.hp_ybar.hover_left_bar = Frame("ui/hpbar_yellow.png", 0, 0)      # Image when you hover the bar. Often brighter.
   
    style.hp_ybar.thumb = "gfx/none.png"                             # Navigator. (thumb) on our case, nothing.

    style.mp_bar = Style(style.hp_bar)
    style.mp_bar.left_bar = Frame("ui/mpbar_full.png", 0, 0)          # Image when you're full of MP
    style.mp_bar.right_bar = Frame("ui/mpbar_empty.png", 0, 0)        # Image when you have zero MP
    style.mp_bar.hover_left_bar = Frame("ui/mpbar_full.png", 0, 0)    # Image when you hover the bar. Often brighter.

#            ui.frame(xalign=0.0, yalign=1.0)
#            ui.vbox()
#            ui.imagebutton("gfx/buttons/heromenu.png", "gfx/buttons/heromenu.hover.png", clicked=ui.callsinnewcontext("ingame_menu"))
#            ui.close()

init python:

    show_hpbar=False    # Display your HP bar
    show_enbar=False    # Display enemy's bar
    show_bossb=False    # Display aboss's bar
    show_money=False    # Display credits bar
    show_loading_bar=False
    load_factor=0

    def myhex(val):
        #tmp3=int(val % 16)
        tmp2=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f']
        tmp1=int(val/7.0) # 7.0 is (105/15)
        if config.developer and False: # If we need to test GUIOpacity again
            print "Got %d which means %d" % (val, tmp1)
            print str(len(tmp2)) + " chars, wanting " + str(tmp1) + " aka. " + str(tmp2[tmp1])

        return tmp2[tmp1]

    def randhex(amount):
        i=0
        seq=""

        while i < amount:
            i+=1
            seq+=str(myhex(renpy.random.randint(0,105)))

        if config.developer:
            print "Random Hexadecimal: "+str(seq)

        return seq

    def opac():
        return str(myhex(GUIOpacity))

    def opaque_frame():
        return Frame(theme.OneOrTwoColor("../common/_theme_marker/ink_box.png", "#6496c8"+opac()+opac()),12,12)

    def display_hpbar():
        #global regen_ticks

        # Coordinates
        lasty=15

        if show_loading_bar:
            ui.frame(ypos=0.8, xalign=0.5)
            ui.vbox()
            ui.bar(6, load_factor)
            ui.text(_("LOADING"), font="f/Jura-Regular.otf")
            ui.close()

        if show_hpbar:
            ## Correct excess if not on battle.
            if playing and not fighting:
                if hero.hp > hero.maxhp:
                    hero.hp=hero.maxhp
                if hero.mp > hero.maxmp:
                    hero.mp=hero.maxmp


            # If you're on fight only display currently active member stats
            if fighting:
                Afighter=fght_act
                render_prototype() # Render battle board
            else:
                Afighter=hero


            ui.frame(xalign = 0.05, ypos=lasty, ymaximum=95, xmaximum=0.41, background=opaque_frame())
            # RoundRect(window, False) # False -> less roundrect see themes.rpym
            # According to 00themes.rpy:105 it should be Frame(theme.OneOrTwoColor(file_prefix + "_box.png", color), 12, 12)

            ui.vbox() # Global alignment
            ui.hbox() # Name and any relevant 15x15 icon
            if config.developer or persistent.premium:
                ui.vbox()
                ui.image("gfx/crown.png")
                if persistent.premium:
                    ui.text("MSG", color="#F00", size=5)
                elif config.developer:
                    ui.text("DEV", color="#F00", size=5)
                else:
                    ui.text("BUG", color="#F00", size=5)
                ui.close()
            ui.text("%s" % (Afighter.name), size=15)
            ui.close()

            ui.hbox() # bars and status
            if Afighter.hp <= ((Afighter.maxhp*15)/100): # less than 15%, change to red variant.
                ui.bar(Afighter.maxhp, Afighter.hp,
                        style="hp_rbar", xmaximum=0.75, yminimum=25, ymaximum=25)
            elif Afighter.hp <= ((Afighter.maxhp*30)/100): # less than 30%, change to yellow variant.
                ui.bar(Afighter.maxhp, Afighter.hp,
                        style="hp_ybar", xmaximum=0.75, yminimum=25, ymaximum=25)
            else: # more than 30% of HP, use standard bar.
                ui.bar(Afighter.maxhp, Afighter.hp,
                        style="hp_bar", xmaximum=0.75, yminimum=25, ymaximum=25)


            ui.close()

            # MP bar
            # This complicate calculation compensates the HP bar moves.
            xvar= len(str(int(Afighter.maxhp)))+len(str(int(Afighter.hp)))+len(str(int(Afighter.level)))-1
            if (show_money):
                xvar+=len(str(int(gp)))-2
            xfin=0.54-(xvar*0.0175) # magic numbers! =D
            ui.bar(Afighter.maxmp, Afighter.mp,
                        style="mp_bar", xmaximum=0.72, yminimum=15, ymaximum=15, ypos=-10, xpos=xfin)

            if (not show_money):
                ui.text("                                 %d/%d LV: %d" % (Afighter.hp, Afighter.maxhp, Afighter.level), size=15)
            else:
                ui.text("GP: [gp]                      %d/%d LV: %d" % (Afighter.hp, Afighter.maxhp, Afighter.level), size=15)

            ui.close()

            ## Party Overview
            ui.frame(xalign = 0.05, ypos=lasty+91, ymaximum=95, xmaximum=0.41, background=opaque_frame())
            ui.hbox()
            ui.vbox()
            if ATTCK_ST and get_player_by_name(ATTCK_ID, 'INCLUDE_ENEMIES') in battling:
                ui.text(_("%s   MP: %d" % (get_first_name(ATTCK_ID), get_player_by_name(ATTCK_ID, 'INCLUDE').mp)  ), size=12)

                ui.hbox()
                # TODO: Add status indicators.
                if Afighter.frozen:
                    #ui.vbox()
                    ui.image("gfx/paralyzed.png")
                    ui.text("%02d" % (Afighter.frozen) , size=10)
                    #ui.close()
                if Afighter.poison:
                    #ui.vbox()
                    ui.image("gfx/poisoned.png")
                    ui.text("%02d" % (Afighter.poison) , size=10)
                    #ui.close()
                if Afighter.silence:
                    #ui.vbox()
                    ui.image("gfx/silence.png")
                    ui.text("%02d" % (Afighter.silence) , size=10)
                    #ui.close()
                ui.close()

                # Data
                ui.text(_("%d/%d in fight" % (len(battling), g_maxbt)  ), size=12)
            else:
                ui.text(_("%s" % (assign_city_name(g_cur_level))  ), size=12)
                #ui.text(_("" % (hero.mp)  ), size=12)
                ui.text(_("%d in party    MP: %d" % (len(party), hero.mp)  ), size=12)
            ui.close()
            # ingame_menu
            if not fighting:
                    ui.textbutton(_("Menu"), clicked=ui.callsinnewcontext("ingame_menu"), size=12)
            else:
                if ATTCK_ST and ATTCK_ID != hero.name:
                    ui.textbutton(_("Party"), clicked=None, size=12)
                else:
                    ui.textbutton(_("Party"), clicked=ui.callsinnewcontext("party_screen"), size=12)

            # Auto Battle Button
            if fighting:
                if autofight:
                    ui.textbutton(_("{color=#F00}Auto{/color}"), clicked=SetVariable("autofight", (not autofight)), size=12)
                else:
                    ui.textbutton(_("{color=#FFF}Auto{/color}"), clicked=SetVariable("autofight", (not autofight)), size=12)
            ui.close()


        # Party stats overview bar + a button
        # Enemy bar, do not mistake with boss bar.
        if show_enbar and not show_bossb:
            ui.frame(xpos = 0.57, ypos=15, ymaximum=55, xmaximum=0.41, background=opaque_frame())

            ui.vbox() # Global alignment
            ui.text("[ThisEnemy.name!t]", size=15)

            ui.hbox() # bars and status

            if ThisEnemy.hp < (ThisEnemy.maxhp/10):
                ui.bar(ThisEnemy.maxhp, ThisEnemy.hp,
                        style="hp_rbar", xmaximum=0.95, yminimum=25)

            elif ThisEnemy.hp < (ThisEnemy.maxhp/2.5):
                ui.bar(ThisEnemy.maxhp, ThisEnemy.hp,
                        style="hp_ybar", xmaximum=0.95, yminimum=25)

            else:
                ui.bar(ThisEnemy.maxhp, ThisEnemy.hp,
                        style="hp_bar", xmaximum=0.95, yminimum=25)

            # TODO: Add status indicators.
            if ThisEnemy.frozen:
                ui.vbox()
                ui.image("gfx/paralyzed.png")
                ui.text("%02d" % (ThisEnemy.frozen) , size=10)
                ui.close()
            if ThisEnemy.hidden:
                ui.vbox()
                ui.image("gfx/hidden.png")
                ui.text("%02d" % (ThisEnemy.hidden) , size=10)
                ui.close()
            if ThisEnemy.silence:
                ui.vbox()
                ui.image("gfx/silence.png")
                ui.text("%02d" % (ThisEnemy.silence) , size=10)
                ui.close()

            ui.close()

            ui.hbox()
            ui.text("                                 [ThisEnemy.hp]/[ThisEnemy.maxhp]", size=15)
            ui.text(_("   LV: [ThisEnemy.level]"), size=15)
            ui.close()


            ui.close()


        # XXX BOSS BAR!!! XXX
        if show_bossb:
            ui.frame(xpos = 0.57, ypos=15, ymaximum=300, xmaximum=0.41, background=opaque_frame())

            ui.vbox() # Global alignment
            ui.text("[ThisEnemy.name!t]", size=15)

            ui.hbox() # bars and status


            ui.vbox() # Health bars
            cur=ThisEnemy.hpbar
            while cur > 0:
                cur-=1
                if (ThisEnemy.hp-(ThisEnemy.maxhp/ThisEnemy.hpbar*cur)) < ((ThisEnemy.maxhp/ThisEnemy.hpbar)/10):
                    ui.bar((ThisEnemy.maxhp/ThisEnemy.hpbar), (ThisEnemy.hp-(ThisEnemy.maxhp/ThisEnemy.hpbar*cur)),
                            style="hp_rbar", xmaximum=0.95, yminimum=25, ymaximum=25)
                elif (ThisEnemy.hp-(ThisEnemy.maxhp/ThisEnemy.hpbar*cur)) < ((ThisEnemy.maxhp/ThisEnemy.hpbar)/2.5):
                    ui.bar((ThisEnemy.maxhp/ThisEnemy.hpbar), (ThisEnemy.hp-(ThisEnemy.maxhp/ThisEnemy.hpbar*cur)),
                            style="hp_ybar", xmaximum=0.95, yminimum=25, ymaximum=25)
                else:
                    ui.bar((ThisEnemy.maxhp/ThisEnemy.hpbar), (ThisEnemy.hp-(ThisEnemy.maxhp/ThisEnemy.hpbar*cur)),
                            style="hp_bar", xmaximum=0.95, yminimum=25, ymaximum=25)

            ui.close()

            # TODO: Add status indicators. NOTE: Boss should be invulnerable...
            ui.vbox()
            if ThisEnemy.frozen:
                ui.vbox()
                ui.image("gfx/paralyzed.png")
                ui.text("%02d" % (ThisEnemy.frozen) , size=10)
                ui.close()
            if ThisEnemy.hidden:
                ui.vbox()
                ui.image("gfx/hidden.png")
                ui.text("%02d" % (ThisEnemy.hidden) , size=10)
                ui.close()
            if ThisEnemy.silence:
                ui.vbox()
                ui.image("gfx/silence.png")
                ui.text("%02d" % (ThisEnemy.silence) , size=10)
                ui.close()
            ui.close()

            ui.close()

            ui.hbox()
            ThisEnemy.hp   =int(ThisEnemy.hp)
            ThisEnemy.maxhp=int(ThisEnemy.maxhp)
            ui.text("                                 [ThisEnemy.hp]/[ThisEnemy.maxhp]", size=15) # TODO: Force int() interpretation.
            ui.text(_("     BOSS"), size=15)
            ui.close()


            ui.close()


    config.overlay_functions.append(display_hpbar)


    # This function is for story purposes
    def limited_partyheal(floater=1.0):
        for abaco in party:
            abaco.hp+=int(abaco.maxhp*floater)
            abaco.mp+=int(abaco.maxmp*floater)
            if abaco.hp > abaco.maxhp:
                abaco.hp-=(abaco.hp-abaco.maxhp)
            if abaco.mp > abaco.maxmp:
                abaco.mp-=(abaco.mp-abaco.maxmp)

    # This function is for story purposes
    def hit_player_verbose(dmg):
        global hero
        hit_someone_verbose(hero, dmg)

    # Core processing to be on a while loop
    def hit_verbose(someone, damage, counting, dmgfactor, fixfactor):
        someone.hp  -= dmgfactor
        counting += dmgfactor
        if counting+fixfactor == damage:
            someone.hp-=(fixfactor)
            counting+=fixfactor
        # TODO: What if something went wrong?
        renpy.pause(0.001)    
        return someone, damage, counting, dmgfactor, fixfactor

    # Main hit verbose function
    def hit_someone_verbose(someone, damage, fast=False):
        counting = 0

        # We can use dmgfactor
        if ((damage > 10) or (damage < 10)) and (not persistent.SkipHPAnimation) and (not fast):
            fixfactor=damage%10
            dmgfactor=int(damage/10)
            dbg_interactions=0
            dbg_srchp=0+someone.hp

            # Which loop to use?
            if damage > 0:
                while counting < damage:
                    someone, damage, counting, dmgfactor, fixfactor=hit_verbose(someone, damage, counting, dmgfactor, fixfactor)
                    someone.hp=int(someone.hp)
                    dbg_interactions+=1

                    # Something went wrong
                    if dbg_interactions > 15:
                        raise Exception("WARNING 15 interactions or more happened without hit_verbose concluding.\n\nBy all means report this bug\nBUG ID: HSV FAILED on hpbar_handler with params someone.name, damage, counting, dmgfactor, fixfactor being %s,%d,%d,%d.\n\nClick \"Ignore\" to continue." % (someone.name, damage, counting, dmgfactor, fixfactor))
                        counting=damage
                        someone.hp=dbg_srchp-damage

            # So, healing it is
            else:
                while counting > damage:
                    someone, damage, counting, dmgfactor, fixfactor=hit_verbose(someone, damage, counting, dmgfactor, fixfactor)
                    someone.hp=int(someone.hp)
                    dbg_interactions+=1

                    # Something went wrong
                    if dbg_interactions > 15:
                        raise Exception("WARNING 15 interactions or more happened without hit_verbose concluding.\n\nBy all means report this bug\nBUG ID: HSV FAILED on hpbar_handler with params someone.name, damage, counting, dmgfactor, fixfactor being %s,%d,%d,%d.\n\nClick \"Ignore\" to continue." % (someone.name, damage, counting, dmgfactor, fixfactor))
                        counting=damage
                        someone.hp=dbg_srchp-damage

        # We cannot use dmgfactor
        else:
            someone.hp  -= damage

        if (someone.hp > someone.maxhp):
            someone.hp=int(someone.maxhp)   # Apply some corrections. TODO possible alias?
        renpy.block_rollback()              # No rollback from there.

