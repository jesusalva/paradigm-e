########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2017  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# THIS IS A BRANCH FROM MECHANICS.RPY
# THIS IS A BRANCH FROM DATABASE.RPY
# ENEMY() DATABASES. Setups enemies and players alike

label setup_hero:
    $ hero=Enemy(yname, 200)
    $ hero.weapon=Sword
    $ hero.armor=Starter_Armor
    $ hero.items.append(Bread)
    $ hero.pic="hero"
    if ymale:
        $ hero.pot="boy"
    else:
        $ hero.pot="girl"
    $ hero.ordem="WRMTS"
    $ hero.dislike=['LCKDWN'] # Prevent issues

    # a bonus based on class
    if classe == C_MAGE:
        $ hero.luk+=0.1
    elif classe == C_WARR:
        $ hero.str+=0.1
    elif classe == C_TANK:
        $ hero.vit+=0.1
    elif classe == C_RANG:
        $ hero.str+=0.05
        $ hero.luk+=0.05

    $ battling=[hero] # We must be double sure your party is composed of only one member - you.
    $ party=[hero]    # TODO: This shouldn't be here. Broken code. FIXME!
    $ skp=0           # Be double-sure you have NO SKILL POINTS from tutorial cheat

    jump setup_journal

label setup_journal:
    #python:
    #    import copy
    #    hero.skills=sk.copy()

    # Now add friends of the hero. (Import from Castle)
    $ susana=Enemy(_("Susana"), 200)
    $ susana.weapon=Saberb
    $ susana.armor=Starter_Armor
    $ susana.ordem="W"
    $ susana.girl=True

    $ edward=Enemy(_("Edward"), 200)
    $ edward.weapon=Sword
    $ edward.offhand=Buckler
    $ edward.armor=Starter_Armor
    $ edward.ordem="T"

    $ corina=Enemy("Corina", 200)
    $ corina.weapon=Dagger
    $ corina.armor=Starter_Armor
    $ corina.ordem="R"
    $ corina.girl=True

    $ hc=Enemy("Christopher", 450)  # aka. Admin
    $ hc.weapon=GSword
    $ hc.offhand=Buckler
    $ hc.armor=Normal_Armor
    $ hc.level=2
    $ hc.mxp+=30
    $ hc.str=1.15
    $ hc.dex=1.21
    $ hc.vit=1.27
    $ hc.ordem="M"



    # Jonatas Luis
    $ jhonny=Enemy(_("Jhonny Jesusaves"), 200)
    $ jhonny.weapon=Sabern
    $ jhonny.offhand=Buckler
    $ jhonny.armor=Normal_Armor
    $ jhonny.ordem=""
    $ jhonny.pic="overlord"
    $ jhonny.pot="jhonny"
    $ jhonny.dislike=['beer']

    # Gustavo Mamede
    $ mamede=Enemy(_("Gustavus Mamedis"), 250)
    $ mamede.weapon=Sword
    $ mamede.offhand=Shield
    $ mamede.armor=Normal_Armor
    $ mamede.ordem="T"
    $ mamede.vit+=0.1
    $ mamede.pic="siegetrooper"
    $ mamede.pot="mamede"

    #  Ítalo Pinheiro
    $ italus=Enemy(_("Italus Primus"), 200)
    $ italus.weapon=GSword
    $ italus.armor=Starter_Armor
    $ italus.ordem="W"
    $ italus.str+=0.1
    $ italus.dex+=0.05
    $ italus.pic="dark_knight" # todo experimental (maybe it should be stalwart and change when changing portrait to "ITALUS PRIMUS")
    $ italus.pot="italus"

    #  Cleidir Salvato (RPS)
    # Presidente da Academia Real de Linarius
    $ pctpct=Enemy(_("Dr. Cleidir Pct. Salvato"), 300)
    $ pctpct.weapon=Dagger
    $ pctpct.armor=Normal_Armor
    $ pctpct.ordem="M"
    $ pctpct.luk+=0.1
    $ pctpct.pic="knight"
    $ pctpct.pot="pct"




    # Tavern characters

    #  Vinicius Martins: “Verme D(esprezível)” (até que Ele o salvou)
    $ vermed=Enemy(_("ViniVerme"), 200)
    $ vermed.weapon=GSword
    $ vermed.armor=Starter_Armor
    $ vermed.ordem="W"
    $ vermed.str+=0.1
    $ vermed.pic="scythemaster"
    $ vermed.pot="vinicius"
    $ vermed.dislike=['LCKDWN'] # I would rather keep him away from beer.

    #  Gilvan
    $ gilvan=Enemy(_("Gill Vans"), 200)
    $ gilvan.weapon=GSword
    $ gilvan.armor=Starter_Armor
    $ gilvan.ordem="W"
    $ gilvan.str+=0.1
    $ gilvan.pic="corporal"
    $ gilvan.pot="havenger" # TODO: Provisory and temporary.

    #  Yasmin Carneiro
    $ yasmin=Enemy(_("Yasmin P. Leaf"), 200)
    $ yasmin.weapon=Sword
    $ yasmin.armor=Normal_Armor
    $ yasmin.ordem="R"
    $ yasmin.str+=0.05
    $ yasmin.luk+=0.05
    $ yasmin.pic="prophet"
    $ yasmin.pot="yasmin"
    $ yasmin.girl=True
    $ yasmin.dislike=['beer']

    #  Ingrid Cristina
    $ ingrid=Enemy(_("Ingrid"), 200)
    $ ingrid.weapon=Sword
    $ ingrid.armor=Normal_Armor
    if renpy.random.random() < 0.5:
        $ ingrid.ordem="T"
        $ ingrid.vit+=0.1
    else:
        $ ingrid.ordem="M"
        $ ingrid.luk+=0.1
    $ ingrid.pic="warlock+female"
    $ ingrid.pot="merina"
    $ ingrid.girl=True

    #  Savyo Costa
    $ savium=Enemy(_("Savium Pacioli"), 200)
    $ savium.weapon=Freezing
    $ savium.armor=Starter_Armor
    $ savium.ordem="M"
    $ savium.luk+=0.1
    $ savium.pic="darkwizard"#"enlightened-archmage"
    $ savium.pot="savyo"

    # Ana Clara Macedo
    $ anacma=Enemy(_("Anna Gold"), 200)
    $ anacma.weapon=Sword
    $ anacma.offhand=Shield
    $ anacma.armor=Normal_Armor
    $ anacma.ordem="T"
    $ anacma.vit+=0.05
    $ anacma.dex+=0.02
    $ anacma.luk+=0.01
    $ anacma.pic="shieldmaiden"
    $ anacma.pot="anam"
    $ anacma.girl=True



    #  Cecile Sprite 11
    $ cecile=Enemy(_("Cecile"), 200)
    $ cecile.weapon=ESword
    $ cecile.armor=Normal_Armor
    $ cecile.ordem="M"
    $ cecile.luk+=0.08
    $ cecile.str+=0.02
    $ cecile.dex+=0.02
    $ cecile.vit+=0.03
    $ cecile.pic="oldelvish-enchantress"
    $ cecile.pot="sprite11"
    $ cecile.girl=True

    # Sophia Wisdom (Tutorial)
    $ sophia=reg("Sophia Wisdom", 15, rgs=True, pic="sharpshooter-female")
    $ sophia.ordem="R"
    $ sophia.pot="wisdom"
    $ sophia.girl=True





    $ no_one=Enemy("No one", 999999, 99, bar=9, rl=999) # aka. The Indestructble searched no one.


    $ load_factor+=1
    pause 0.1
    return



init python:
    def get_player_by_name(nameid, universe="_@_DEFAULT_@_"):
        if universe == "_@_DEFAULT_@_":
            universe=party
        elif universe == "INCLUDE_ENEMIES":
            IUI=[]
            for i in battling:
                IUI.append(i)
            for i in EnemyParty:
                IUI.append(i)
            universe=IUI

        for goy in universe:
            if goy.name == nameid:
                return goy
        return Enemy(_("SYSTEM ERROR - BUG, REPORT ME: GPBN"), 12001)

    def get_average_level(): # Return your party average highest level for late-game additions
        global party
        gsl_highest=0
        gsl_secondh=0

        for char in party:
            # I don't care for hero level
            if char == hero:
                if config.developer:
                    print "Hey, it is the hero. Let's IGNORE their awesome level!"
                continue
            if char.level >= gsl_highest:
                gsl_secondh=0
                gsl_secondh+=gsl_highest
                gsl_highest=0
                gsl_highest+=char.level

        return int((gsl_highest+gsl_secondh)/2)

    def resync_level(char): # Resynchronizes someone's level to meet your party average (for late-game additions)
        char.xp=0
        tmp_rsynclvl = get_average_level()
        if config.developer:
            print "Now resyncing %s level from %d to %d" % (char.name, char.level, tmp_rsynclvl)
        while char.level < tmp_rsynclvl:
            char.xp+=char.mxp
            char.s_lvlup()

    def get_average_luck(): # Return your party average luck.
        global party
        gal_val=0.0

        for char in party:
            gal_val+=(char.luk-1.0)

        return float(float(gal_val/len(party))+1.0)

    def ordem_to_text(src):
        classe=""

        if len(src) == 0:
            return "Neutral"

        for i in src:
            if i == 'W':
                classe+="Warrior"
            elif i == 'T':
                classe+="Tanker"
            elif i == 'R':
                classe+="Ranger"
            elif i == 'M':
                classe+="Mage"
            elif i == 'S':
                classe+="Sacred"
            if len(src) > 1: # Buggy
                classe+='\n'

        return classe
    # Show someone's portrait, but only if this someone is on party (saves up on script space usage)
    def show(whoami, effect=dissolve, at=[center], behind=[]):
        #print "Hello hello this is show for " + str(whoami.name)
        #print "We're trying to cast: " + str(whoami.pot.upper()) + " (a png image)"
        if whoami in party:
            persistent.testrart=False
            #print "We do have him in party"
            renpy.show(whoami.pot.upper() + ' full', at_list=at, behind=behind)
            renpy.with_statement(effect)
        else:
            for i in party:
                if i.name==whoami.name and not persistent.testrart:
                    print "\033[1;31mA BUG HAPPENED.\033[0m\n%s was not found within party, but a deep scan revealed that the TEST FAILED. We're using %s as a replacement.\nPlease note that theorically %s is not %s and strange bugs may or may not raise because that.\n\nPlease report the above bug, unless if you have reset database.\nIn that case, you deseve to see these silly messages." % (whoami.name, i.name, str(whoami), str(i))
                    persistent.testrart=True # Safety against infinite loops (We're recasting this function below!)
                    show(i, effect, at)
                    break

    # Hide someone's portrait (Useful when you don't know the image ID being shown for whatever reasons eg. automated portrait change)
    def hide(whoami, effect=dissolve):
        renpy.hide(whoami.pot.upper())
        renpy.with_statement(effect)

    def get_first_name(string):
        return str(string.split(' ')[0])
label OnJesusalvaJoin:
    "Jhonny Jesusaves haves two class. If it is your first time, we advise to try Tanker/Ranger, but this choice is yours to make."
    jump OnJesusalvaJoin1

label OnJesusalvaJoin1:
    # Determine class.
    window hide None
    call screen class_select
    window show None

    $classe=_return

    $ renpy.block_rollback()
    if classe == C_MAGE and not "M" in jhonny.ordem:
        $ jhonny.ordem+="M"
        $ jhonny.luk+=0.09
    elif classe == C_WARR and not "W" in jhonny.ordem:
        $ jhonny.ordem+="W"
        $ jhonny.str+=0.09
    elif classe == C_TANK and not "T" in jhonny.ordem:
        $ jhonny.ordem+="T"
        $ jhonny.vit+=0.09
    elif classe == C_RANG and not "R" in jhonny.ordem:
        $ jhonny.ordem+="R"
        $ jhonny.dex+=0.09
    else:
        "INVALID CLASS, TRY AGAIN"
        jump OnJesusalvaJoin1

    jump OnJesusalvaJoin2

label OnJesusalvaJoin2:
    call skupd(jhonny)
    "Determine Jhonny's auxiliar class."
    # Determine class.
    window hide None
    call screen class_select
    window show None

    $classe=_return

    $ renpy.block_rollback()
    if classe == C_MAGE and not "M" in jhonny.ordem:
        $ jhonny.ordem+="M"
        $ jhonny.luk+=0.01
    elif classe == C_WARR and not "W" in jhonny.ordem:
        $ jhonny.ordem+="W"
        $ jhonny.str+=0.01
    elif classe == C_TANK and not "T" in jhonny.ordem:
        $ jhonny.ordem+="T"
        $ jhonny.vit+=0.01
    elif classe == C_RANG and not "R" in jhonny.ordem:
        $ jhonny.ordem+="R"
        $ jhonny.dex+=0.01
    else:
        "INVALID CLASS, TRY AGAIN"
        jump OnJesusalvaJoin2

    call skupd(jhonny)
    return

# LABEL to add a member. Safe against double-adds
# WARNING this may not hold against saveload and/or journal reboots. BEWARE.
label add_member(mnamex):
    if mnamex in party:
        return
    $ party.append(mnamex)
    centered "{i}[mnamex.name]{/i} has joined your team!"
    if mnamex == jhonny:
        jump OnJesusalvaJoin
    call skupd(mnamex)
    return

# LABEL to add a TEMPORARY ally. Safe against double-adds
label add_ally(mnamex):
    if mnamex in battling:
        return
    if len(battling) < g_maxbt:
        $ battling.append(mnamex)
        centered "{i}[mnamex.name]{/i} will fight alongside you!"
        call skupd(mnamex) # HMMMMM
    else:
        "{font=f/Imperator.ttf}Warning{/font}" "You must free at least one party slot to continue."
        call party_screen
        call add_ally(mnamex)
    return

