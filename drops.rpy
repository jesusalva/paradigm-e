########################################################################################
#     This file is part of Castle.
#     Copyright (C) 2015  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# TODO: This file handles drop lists to avoid conflicts, like a Fire Eel dropping ice.

init 1:
  python:
    def drop_list(dl, adicional_drop=[GenericItem("."), 0.0]):
        if dl == 0: # Default
            droplist=[
[Teeth      , 0.15],
[Fur        , 0.20],
[Fang       , 0.20],
[Coal       , 0.05],
[IronScrap  , 0.01],
[Bone       , 0.05]]

        elif dl == 1: # Orc
            droplist=[
[Teeth      , 0.10],
[Fur        , 0.05],
[Gold       , 0.20],
[IronScrap  , 0.05],
[Bone       , 0.10]]

        elif dl == 2: # Human
            droplist=[
[Teeth      , 0.10],
[Gold       , 0.10],
[Glass      , 0.03],
[GlasShard  , 0.08],
[IronScrap  , 0.05],
[Bone       , 0.20]]

        elif dl == 3: # Monster
            droplist=[
[Teeth      , 0.05],
[Fang       , 0.05],
[Gold       , 0.05],
[Bone       , 0.05]]


        elif dl == 4: # Not sure...
            droplist=[
[Teeth      , 0.10],
[Fur        , 0.05],
[Gold       , 0.20],
[IronScrap  , 0.05],
[Bone       , 0.10],
[Bread      , 0.15],
[Glass      , 0.02]]


        elif dl == 5: # Bandit
            droplist=[
[Glass      , 0.02],
[Gold       , 0.25],
[IronScrap  , 0.10],
[GlasShard  , 0.10],
[Bone       , 0.15],
[Teeth      , 0.10]]


        elif dl == 6: # Left-overs
            droplist=[
[IronScrap  , 0.60],
[GlasShard  , 0.60],
[IronScrap  , 0.35],
[GlasShard  , 0.35],
[Bread      , 0.25],
[Bread      , 0.15]]

        elif dl == 7: # Titan Default
            droplist=[
[Teeth      , 0.15],
[Fur        , 0.20],
[Fang       , 0.20],
[Coal       , 0.15],
[IronScrap  , 0.20],
[IronScrap  , 0.05],
[HolyMetal  , 0.01],
[Copper     , 0.01],
[Gold       , 0.10],
[Bone       , 0.05]]


        elif dl == 8: # Mechanical
            droplist=[
[Steel      , 0.01],
[HolyMetal  , 0.02],
[Gold       , 0.05],
[Copper     , 0.05],
[Coal       , 0.10],
[Coal       , 0.10],
[IronScrap  , 0.15],
[IronScrap  , 0.15]]

        elif dl == 9: # Titan Mechanical
            droplist=[
[Steel      , 0.02],
[HolyMetal  , 0.05],
[Gold       , 0.10],
[Copper     , 0.15],
[Coal       , 0.20],
[Coal       , 0.20],
[IronScrap  , 0.30],
[IronScrap  , 0.45]]


        elif dl == 100: # Lumnia Default
            droplist=[
[Aluminium  , 0.15],
[Fur        , 0.20],
[Fang       , 0.20],
[Coal       , 0.05],
[IronScrap  , 0.01],
[Bone       , 0.05]]

        elif dl == 101: # Angry Teeth - Specific Drop List - 1xx.
            droplist=[
[Teeth      , 0.25],
[Fur        , 0.10],
[Fang       , 0.10]]

        elif dl == 102: # Titan Lumnia
            droplist=[
[Aluminium  , 0.75],
[Teeth      , 0.15],
[Fur        , 0.20],
[Fang       , 0.20],
[Coal       , 0.15],
[IronScrap  , 0.20],
[IronScrap  , 0.05],
[HolyMetal  , 0.01],
[Copper     , 0.01],
[Gold       , 0.10],
[Bone       , 0.05]]

        elif dl == 1001: # 1st floor boss - Specific Boss Drop List - 1xxx. Max luck bonus is 4%...
            droplist=[
                [MidNight_Cloack    , 0.45],
                [Elucidator         , 1.00],
                [HealingCRT         , 1.00],
                [SupPot             , 0.20],
                [Steel              , 0.25],
                [Fang               , 0.95],
                [Glass              , 0.80],
                [Glass              , 0.45],
                [IronScrap          , 1.00],
                [Gold               , 0.95],
                [Gold               , 0.60]]

        elif dl == 2001: # 1st floor boss - Additional Boss Drop List - 2xxx.
            droplist=[
                [Fang               , 0.60],
                [Fang               , 0.45],
                [Glass              , 0.25],
                [Glass              , 0.25],
                [IronScrap          , 0.30],
                [Gold               , 0.70],
                [Gold               , 0.20],
                [Bone               , 1.00],
                [Bread              , 0.81]]


        elif dl == 1002: # 2nd floor boss - Specific Boss Drop List - 1xxx. Max luck bonus is 4%...
            droplist=[
                [Tower_Shd          , 0.25],
                [SaberB             , 0.45],
                [HealingCRT         , 1.00],
                [ManaPot            , 1.00],
                [SupPot             , 0.20],
                [HolyMetal          , 0.95],
                [Glass              , 0.80],
                [Glass              , 0.45],
                [IronScrap          , 1.00],
                [Gold               , 0.95],
                [Gold               , 0.60]]

        elif dl == 2002: # 2nd floor boss - Additional Boss Drop List - 2xxx.
            droplist=[
                [ManaTreeBranch     , 0.40],
                [ManaTreeBranch     , 0.15],
                [Glass              , 0.25],
                [Glass              , 0.25],
                [IronScrap          , 0.30],
                [Gold               , 0.60],
                [Gold               , 0.20],
                [IronScrap          , 1.00],
                [ManaPot            , 0.81]]


        elif dl == 1003: # 3rd floor boss - Specific Boss Drop List - 1xxx. Max luck bonus is 4%...
            droplist=[
                [XDagger            , 0.40],
                [FxSaber            , 0.75],
                [HealingCRT         , 1.00],
                [TeleportCRT        , 0.20],
                [Glass              , 0.80],
                [Glass              , 0.45],
                [Glass              , 0.25],
                [Glass              , 0.25],
                [Steel              , 0.30],
                [HolyMetal          , 0.95],
                [ManaTreeBranch     , 0.40],
                [ManaTreeBranch     , 0.15],
                [IronScrap          , 1.00],
                [IronScrap          , 0.80],
                [IronScrap          , 0.30],
                [Gold               , 0.95],
                [Gold               , 0.60],
                [Gold               , 0.60],
                [Gold               , 0.20],
                [SupPot             , 0.20],
                [ManaPot            , 1.00],
                [ManaPot            , 0.81]]

        elif dl == 1005: # 5th floor boss - Specific Boss Drop List - 1xxx. Max luck bonus is 4%...
            droplist=[
                [WhiteLily          , 0.40],
                [SaberA             , 0.20],
                [Sacred_Cross       , 0.05],
                [HealingCRT         , 1.00],
                [HealingCRT         , 0.25],
                [TeleportCRT        , 0.20],
                [Coal               , 1.00],
                [Coal               , 0.40],
                [Glass              , 0.80],
                [Glass              , 0.45],
                [Glass              , 0.25],
                [Glass              , 0.25],
                [Steel              , 0.30],
                [Steel              , 0.10],
                [HolyMetal          , 0.95],
                [HolyMetal          , 0.15],
                [ManaTreeBranch     , 0.40],
                [ManaTreeBranch     , 0.15],
                [IronScrap          , 1.00],
                [IronScrap          , 0.80],
                [IronScrap          , 0.30],
                [Gold               , 0.95],
                [Gold               , 0.60],
                [Gold               , 0.60],
                [Gold               , 0.20],
                [SupPot             , 0.20],
                [SacPot             , 0.20],
                [ManaPot            , 1.00],
                [ManaPot            , 0.81],
                [SuperManaPot       , 0.20]]

        elif dl == 2005: # 5th floor boss - Additional Boss Drop List - 2xxx.
            droplist=[
                [Sacred_Cross       , 0.05],
                [HealingCRT         , 0.15],
                [Coal               , 1.00],
                [Coal               , 0.40],
                [Glass              , 0.80],
                [Glass              , 0.45],
                [Glass              , 0.25],
                [Steel              , 0.30],
                [Steel              , 0.10],
                [HolyMetal          , 0.95],
                [HolyMetal          , 0.15],
                [ManaTreeBranch     , 0.40],
                [ManaTreeBranch     , 0.15],
                [IronScrap          , 1.00],
                [IronScrap          , 0.80],
                [IronScrap          , 0.30],
                [Gold               , 0.95],
                [Gold               , 0.60],
                [Gold               , 0.60],
                [Gold               , 0.20],
                [SupPot             , 0.30],
                [SupPot             , 0.15],
                [SacPot             , 0.20],
                [ManaPot            , 0.31],
                [SuperManaPot       , 0.10]]

        elif dl == 3001: # Quest drops. Major Q1, ADMIN SIDE QUEST -- BANDIT CAMP RANSACK 01
            droplist=[
[Gold        , 0.6000],
[Gold        , 0.4000],
[Gold        , 0.2000],
[Glass       , 0.5020],
[Glass       , 0.3000],
[IronScrap   , 0.7020],
[IronScrap   , 0.3000],
[IronScrap   , 0.0700],
[Glass       , 0.1000],
[Gold        , 0.0500],
[Sword       , 0.0050],
[Buckler     , 0.0020],
[Bread       , 0.5010],
[Sandwich    , 0.3020],
[MinorPot    , 0.0504],
[Bronze_Shd  , 0.0001],
[Iron_Armor  , 0.0032],
[HealingBlade, 0.0001],
[BigPot      , 0.0024],
[MegPot      , 0.0011],
[HealingCRT  , 0.0005],
[TeleportCRT , 0.0005],
[MedPot      , 0.0100]]

        elif dl == 3002: # Secret Boss: 1oldtymer
            droplist=[
[MidNight_Cloack,0.45],
[Tower_Shd   , 0.2500],
[SaberB      , 0.4500],
[Elucidator  , 0.2500],
[HealingCRT  , 1.0000],
[Copper      , 0.3100],
[Aluminium   , 0.2500],
[Coal        , 0.4100],
[Gold        , 0.6000],
[Gold        , 0.4000],
[Gold        , 0.2000],
[Glass       , 0.5020],
[Glass       , 0.3000],
[IronScrap   , 0.7020],
[IronScrap   , 0.3000],
[IronScrap   , 0.0700],
[Glass       , 0.1000],
[Gold        , 0.0500],
[Sword       , 0.0050],
[Buckler     , 0.0020],
[Bread       , 0.5010],
[Sandwich    , 0.3020],
[MinorPot    , 0.0504],
[Bronze_Shd  , 0.0001],
[Iron_Armor  , 0.0032],
[HealingBlade, 0.0001],
[BigPot      , 0.0024],
[MegPot      , 0.0011],
[HealingCRT  , 0.0005],
[TeleportCRT , 0.0005],
[MedPot      , 0.0100]]

        elif dl == 3003: # Quest drops. Major Q3
            droplist=[
[Copper      , 0.3100],
[Gold        , 0.6000],
[Gold        , 0.4000],
[Gold        , 0.2000],
[Glass       , 0.5020],
[Glass       , 0.3000],
[IronScrap   , 0.7020],
[IronScrap   , 0.3000],
[IronScrap   , 0.0700],
[Glass       , 0.1000],
[Gold        , 0.0500],
[ESword      , 0.0050],
[GSword      , 0.0150],
[Shield      , 0.0020],
[ManaPot     , 0.5010],
[Sandwich    , 0.3020],
[MinorPot    , 0.0504],
[Bronze_Shd  , 0.0001],
[Iron_Armor  , 0.0032],
[HealingBlade, 0.0001],
[BigPot      , 0.0024],
[MegPot      , 0.0011],
[HealingCRT  , 0.0005],
[TeleportCRT , 0.0005],
[SuperManaPot, 0.0310],
[SupPot      , 0.0100]]

        else:
            droplist=[] # No drop. (ie. dl = -1.)

        droplist.append(adicional_drop)


        # Append healing items, based on your level. It should be influenced by enemy level, instead!
        if hero.level <= 2:#350
            droplist.append([Sandwich,0.10+(get_average_level()/100.0)])
        elif hero.level <= 3:#500
            droplist.append([MinorPot,0.10+(get_average_level()/100.0)])
        elif hero.level <= 4:#650
            droplist.append([MinorPot,0.10+(get_average_level()/100.0)])
            droplist.append([MedPot,0.05+(get_average_level()/100.0)])
        elif hero.level <= 5:#800
            droplist.append([MedPot,0.09+(get_average_level()/100.0)])
            droplist.append([BigPot,0.05+(get_average_level()/100.0)])
        elif hero.level <= 6:#950
            droplist.append([BigPot,0.09+(get_average_level()/100.0)])
            droplist.append([MegPot,0.01+(get_average_level()/100.0)])
        elif hero.level <= 7:#1100
            droplist.append([BigPot,0.09+(get_average_level()/105.0)])
            droplist.append([MegPot,0.05+(get_average_level()/105.0)])
        elif hero.level <= 9:#1400
            droplist.append([MegPot,0.08+(get_average_level()/107.0)])
            droplist.append([SupPot,0.01+(get_average_level()/107.0)])
        elif hero.level <= 10:#1550
            droplist.append([MegPot,0.08+(get_average_level()/110.0)])
            droplist.append([SupPot,0.04+(get_average_level()/110.0)])
        elif hero.level <= 12:#1850
            droplist.append([SupPot,0.07+(get_average_level()/110.0)])
            droplist.append([SacPot,0.01])
            droplist.append([MegPot,(get_average_level()/110.0)])
        else:
            droplist.append([SupPot,0.08+(get_average_level()/110.0)])
            droplist.append([MegPot,0.01+(get_average_level()/110.0)])
            droplist.append([SacPot,0.02])

        droplist.append([ManaPot,(get_average_level()/100.0)])


        for dropper in droplist:
            dropper[1]*=(1.0+(get_average_luck()/10.0) ) # Change every drop rate to attend the difficulty bonuses and party luck.
            # Luck is 10%, so 100 luck results in 1/10 or 10% bonus, and 10 pts in 1%.
            # Maybe we should sum instead?

        return droplist




    ###################################################################

init 1:
  python:
    def drop_items(dl, ad=[GenericItem("."), 0.0], odd=0.4):
        # VARIABLES: dl=list number, ad=Adicional drop, odd=chances of dropping, droplist=which list use, dropped=what you already gained, newdrop=Amount of dropped items, drop=possible drop item, rnd=ctnd=random value.
        # TODO: This must be rewritten, so you can drop items at top.
        if dl != 133731415: # Leet PI
            droplist=drop_list(dl, ad)
            dropped=[]

            newdrop =0
            # Let's sort the items.
            rnd=renpy.random.random()
            for drop in droplist:
                    if rnd <= drop[1]:
                        if newdrop == 0:
                            newdrop+=1
                            dropped.append(drop[0])
                        else:
                            ctnd=renpy.random.random()#chance to new drop
                            if (ctnd <= (odd/newdrop)):
                                newdrop+=1
                                dropped.append(drop[0])
        else:
          #print "133731415 code actived"
          dropped=[]
          for x in EnemyParty:
            droplist=drop_list(x.droplvl, ad)

            # Let's sort the items. With simplifications.
            for drop in droplist:
                if renpy.random.random() <= drop[1]:
                    dropped.append(drop[0])

        return dropped

    ################################################################
    def stockdrop(droparray):
        # This function stores the drops on inventary.
        for i in droparray:
            hero.items.append(i)

    def writedrop(droparray):
        # This function transform the drops on strings.
        droparray.sort(key=lambda student: student.name) # Don't ask. I have no idea how this work.
        dropped_items=""
        for item in droparray:
            dropped_items+=str(item.name)
            dropped_items+="\n"
        return dropped_items


