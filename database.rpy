########################################################################################
#     This file is part of Castle.
#     Copyright (C) 2015  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# THIS IS A BRANCH FROM MECHANICS.RPY
# GAME DATABASES. Setups all defaults and provides auto-enemy-generator REG func.

init python:
  def reg(name, power_level, armor=False, rgs=False, mage=False, aispec=["", 0.0], pic="brute", dl=0):
    # Magnitute Javali: HP 100 LV 1 RL 5 DL 0 | 10-30dmg 64% acc | no defs
    # Magnitute Power Level: 5
    aiconfig=aispec

    # MAGNUS are the magnitute controls, to make things 25% higher or 25% lower TODO DOES THIS WORK?
    magnus=0.75+((renpy.random.random()/4.0)*2)
    magnus2=0.75+((renpy.random.random()/4.0)*2)
    magnus3=0.75+((renpy.random.random()/4.0)*2)
    if config.developer:
        print "[REG] The three seeds of randomness are: %.2f - %.2f - %.2f" % (magnus, magnus2, magnus3)
    # It'll range from 0.75 to 1.25

    # 20 times HP with modifier. PL is RL, and level uses Journal understanding. (PL 100: Lv 10) (PL 91: Lv 10) (PL 87: Lv 9)
    my_enemy=Enemy(name, int(power_level*magnus*20), int((power_level/10)+1), rl=power_level, dl=dl)
    my_enemy.pic=pic

    # Repass any scripted AI configuration
    my_enemy.aispec=aiconfig[0]
    my_enemy.aieffc=aiconfig[1]

    # The weapon is a tad weirder, though.
    # Damage is 2-6 times the PL, with a random magnus multiplier.
    # Acc is a random magnus except the one used on HP, minus 30% (so ranges in 45~95 %)
    my_enemy.weapon=Weapon(name+"'s weapon", 
    int(power_level*1.5*renpy.random.choice([magnus, magnus2, magnus3])), # Using basic value as 40: 45~75 dmg (avg. 60) (prev. 80)
    int(power_level*3.5*renpy.random.choice([magnus, magnus2, magnus3])), # Using basic value as 40: 105~175 dmg (avg. 140) (prev. 200)
    (renpy.random.choice([magnus2, magnus3])-0.29) )

    # Readjust enemy acc so it ranges from 70~96%. This way, misses are less frequent. :|
    if my_enemy.weapon.acc < 0.70:
        my_enemy.weapon.acc = 0.70

    if armor:
        # RGE supports some defense. Up to PL, it absorbs up to almost 50% (min. 0.0001%)
        my_enemy.armor=Armor(name+"'s armor", power_level,
        (renpy.random.choice([magnus, magnus2, magnus3])-0.7499) )

    if mage:
        # This flag means to add a Luck stat, with up to 51% skillcasting
        my_enemy.luk     =1.01+(renpy.random.random()/2.0)

    if rgs:
        # RGS - Random Generated Stats. They're based on LEVEL. 
        tmp_rge_lvl     = int((power_level/10)+1)
        my_enemy.str    = 1.0 + ((tmp_rge_lvl/100.0) * renpy.random.choice([magnus2, magnus3]))
        my_enemy.dex    = 1.0 + ((tmp_rge_lvl/100.0) * renpy.random.choice([magnus, magnus3]))
        my_enemy.vit    = 1.0 + ((tmp_rge_lvl/100.0) * renpy.random.choice([magnus, magnus2]))
        # VIT does nothing without armor, so we create one. (Ranging from 0 to 25%)
        if not armor:
            my_enemy.armor=Armor(name+"'s dummy armor", power_level,
            ((renpy.random.choice([magnus, magnus2, magnus3])-0.7499)/2.0) )

    return my_enemy

# Miscellaneous setup stuff for dev_beta
label setup_weapons:

    # Neutral Swords (cheap)
    $ Paradigm      =Weapon("Paradigm"           ,200, 350, 0.75, 0.50, "paralyze heal poison silence drain instakill I") # ***UR***
    $ HealingBlade2 =Weapon("The Self Healing II", 65,  85, 0.75, 0.21, "heal II") # L12
    $ HealingBlade  =Weapon("The Self Healing I" , 45,  55, 0.75, 0.20, "heal I") # L07
    $ FreezingADM   =Weapon("The Chilling Blade" , 50,  80, 0.75, 0.40, "paralyze II") # L15
    $ FreezingGM    =Weapon("The Chilling Blade" , 50,  75, 0.75, 0.40, "paralyze I") # L12
    $ Freezing      =Weapon("The Chilling Blade" , 40,  75, 0.75, 0.20, "paralyze I") # L10
    $ WhiteKing     =Weapon("White King"         ,160, 180, 0.75) # L23
    $ WhiteQueen    =Weapon("White Queen"        ,140, 160, 0.75) # L21
    $ WhitePrince   =Weapon("White Prince"       ,120, 140, 0.75) # L19
    $ WhiteLily     =Weapon("White Lily"         , 90, 120, 0.75) # L17
    $ Elucidator    =Weapon("Elucidator"         , 70, 120, 0.75) # L15 ***SR***
    $ Darkiller     =Weapon("Dark Killer"        , 70,  90, 0.75) # L12
    $ PDarkiller    =Weapon("Dark Killer Proto"  , 60,  80, 0.75) # L10
    $ BDarkiller    =Weapon("Baby Dark Killer"   , 50,  70, 0.75) # L07
    $ ESword        =Weapon("Excellent Sword"    , 40,  60, 0.75) # L05
    $ GSword        =Weapon("Good Sword"         , 30,  50, 0.75) # L03
    $ Sword         =Weapon("Standard Sword"     , 20,  40, 0.75) # L01

    # Daggers (High precision, Low damage, cheap. Max acc: 90%)
    $ XDagger       =Weapon("Dagger X"           , 60, 110, 0.90) # L15 ***SR***
    $ MDagger       =Weapon("Master Dagger"      , 65,  85, 0.90) # L12
    $ EDagger       =Weapon("Excellent Dagger"   , 55,  75, 0.89) # L10
    $ GDagger       =Weapon("Good Dagger"        , 45,  65, 0.88) # L07
    $ SDagger       =Weapon("Sharp Dagger"       , 35,  55, 0.87) # L05
    $ DaggeR        =Weapon("Dagger"             , 25,  45, 0.86) # L03
    $ Dagger        =Weapon("Blind Dagger"       , 15,  36, 0.85) # L01

    # Sabers (Good precision and damage, expensive)
    $ FxSaber       =Weapon("Flanket Saber"      , 65, 115, 0.80) # L16 ***SR***
    $ SaberA        =Weapon("Saber class A"      , 90, 119, 0.80) # L16
    $ SaberB        =Weapon("Saber class B"      , 80,  99, 0.80) # L14
    $ SaberC        =Weapon("Saber class C"      , 70,  89, 0.80) # L12
    $ SaberD        =Weapon("Saber class D"      , 60,  79, 0.80) # L10
    $ SaberE        =Weapon("Saber class E"      , 50,  69, 0.80) # L07
    $ Sabern        =Weapon("Basic Saber"        , 40,  60, 0.80) # L05
    $ Saberb        =Weapon("Pratice Saber"      , 25,  45, 0.80) # L02

    # Bastard Swords (high damage, low precision, expensive)
    $ FourKill      =Weapon("Fourth Killer"         ,100, 140, 0.65) # L16
    $ TwoBlood      =Weapon("Blood Slasher"         , 80, 120, 0.65) # L12
    $ TwoHands      =Weapon("Bastard Sword"         , 60, 100, 0.65) # L10
    $ TwoHandsB     =Weapon("Basic Bastard Sword"   , 40,  80, 0.65) # L05
    $ TwoHandsP     =Weapon("Pratice Bastard Sword" , 20,  50, 0.65) # L02

    # Armors (max 40%)
    $ Starter_Armor     =Armor(_("Starter Armor")      ,  15, 0.10) # absorbs 10% of damage util a maximum of  15 damage.
    $ Normal_Armor      =Armor(_("Regular Armor")      ,  40, 0.12) # absorbs 12% of damage util a maximum of  40 damage.
    $ Reinforced_Armor  =Armor(_("Reinforced Armor")   ,  80, 0.15) # absorbs 15% of damage util a maximum of  80 damage.
    $ Leather_Armor     =Armor(_("Leather Armor")      ,  60, 0.20) # absorbs 20% of damage util a maximum of  60 damage.
    $ Iron_Armor        =Armor(_("Iron Armor")         ,  80, 0.20, pic="silver armor") # absorbs 20% of damage util a maximum of  80 damage.
    $ Steel_Armor       =Armor(_("Steel Armor")        , 100, 0.25, pic="silver armor") # absorbs 25% of damage util a maximum of 100 damage.
    $ MidNight_Cloack   =Armor(_("Midnight Cloack")    , 120, 0.20, pic="cloack")       # absorbs 20% of damage util a maximum of 120 damage. RARE ITEM.

    $ ADM_Armor01       =Armor(_("Custom Armor I")     ,  60, 0.13) # absorbs 13% of damage util a maximum of  60 damage.
    $ ADM_Armor02       =Armor(_("Custom Armor II")    ,  80, 0.15) # absorbs 15% of damage util a maximum of  80 damage.
    $ ADM_Armor03       =Armor(_("Custom Armor III")   , 100, 0.18) # absorbs 16% of damage util a maximum of 100 damage.
    $ ADM_Armor04       =Armor(_("Custom Armor IV")    , 120, 0.21) # absorbs 21% of damage util a maximum of 120 damage.
    $ ADM_Armor05       =Armor(_("Custom Armor V")     , 150, 0.24) # absorbs 24% of damage util a maximum of 150 damage.
    $ ADM_Armor06       =Armor(_("Custom Armor VI")    , 200, 0.40) # absorbs 40% of damage util a maximum of 200 damage.
    $ Paradox           =Armor(_("Paradox")            , 400, 0.40, pic="cloack") # absorbs 40% of damage util a maximum of 400 damage.

    # Shields (max 60%)
    $ Buckler       =Armor(_("Buckler")         , 40, 0.15, pic="buckler")      # abaorbs 15% of damage util  40.
    $ Shield        =Armor(_("Basic Shield")    , 80, 0.20, pic="shield")       # absorbs 20% of damage util  80.
    $ Std_Shd       =Armor(_("Standard Shield") ,100, 0.21, pic="shield")       # absorbs 21% of damage util 100.
    $ Bronze_Shd    =Armor(_("Bronze Shield")   ,120, 0.24, pic="shield gold")  # absorbs 24% of damage util 120.
    $ Tower_Shd     =Armor(_("Tower Shield")    ,100, 0.35, pic="shield tower") # absorbs 35% of damage util 100.
    $ Sacred_Cross  =Armor(_("Sacred Cross")    ,400, 0.60, pic="shield tower") # absorbs 60% of damage util 400.

    # Healing Items
    $ Bread         =HealItem(_("Bread")               ,  25)                             # heals  25 damage.
    $ Sandwich      =HealItem(_("Sandwich")            ,  50)                             # heals  50 damage. Restores 10 MP.
    $ MinorPot      =HealItem(_("Minor Healing Potion"), 100,  0, pic="green potion")     # heals 100 damage.
    $ MedPot        =HealItem(_("Med. Healing Potion") , 250,  0, pic="green potion")     # heals 250 damage.
    $ BigPot        =HealItem(_("Big Healing Potion")  , 500,  0, pic="red potion")       # heals 500 damage.
    $ MegPot        =HealItem(_("Mega Healing Potion") ,1000,  0, pic="red potion")       # heals1000 damage.
    $ SupPot        =HealItem(_("Super Healing Potion"),1500, 25, pic="full potion")      # heals1500 damage. Restores 25 MP.
    $ SacPot       =HealItem(_("Sacred Healing Potion"),3000, 25, pic="full potion")      # heals3000 damage. Restores 50 MP.
    $ ManaPot       =HealItem(_("Mana Potion")         ,   0, 50, pic="mp potion")        # heals   0 damage. Restores 50 MP.
    $ SuperManaPot  =HealItem(_("Super Mana Potion")   ,   0,100, pic="mp potion")        # heals   0 damage. Restores 100 MP.

    # Crystaline Devices
    $ TeleportCRT   =Crystaline(_("Teletransport C."), "teletransport", behavior="jump", pic="teleport")
    $ HealingCRT    =Crystaline(_("Healing C."),       "heal_crystal", pic="heal crystal" )

    # Drop Items
    # aluminum, copper, nickel, mild steel, stainless steel, and magnesium. Mild steel is the best choice, and magnesium generally performs poorly as a drop forging material.
    $ BugDrop       =GenericItem("Bug, report me")          # A bug
    $ Teeth         =GenericItem("Teeth",10)                # A teeth.
    $ Fur           =GenericItem("Fur",10)                  # Some fur.
    $ Fang          =GenericItem("Fangs",10)                # Generic Fangs from some animal
    $ Bone          =GenericItem("Bone",15)                 # Bones are usually dropped by humans...
    $ Gold          =GenericItem("Gold Coin",15)            # A very valuable gold coin.
    $ IronScrap     =GenericItem("Iron Scrap",12)           # It was part of some weapon/armor long, long ago.
    $ Glass         =GenericItem("Glass",25)                # How it didn't broke is unknown.
    $ GlasShard     =GenericItem("Glass Shard",15)          # A fragment of glass. Much less valuable.
    $ ManaTreeBranch=GenericItem("Mana Tree Branch",100)    # A powerful piece of wood, used for powerful craftings which needs wood.
    $ HolyMetal     =GenericItem("Holy Metal",100)          # Rare metal, used for decent forging.
    $ Coal          =GenericItem("Coal",5)                  # Coal used by the Forge
    $ Aluminium     =GenericItem("Aluminium",20)            # Aluminium is a weak forging material found in lumnia
    $ Copper        =GenericItem("Copper",50)               # Copper is an average forging material. Used to make Bronze materials.
    $ Steel         =GenericItem("Steel",250)               # Makes excellent weapons. Very rare, only found on worst enemies.

    # Setup Skills

    $ load_factor+=1
    pause 0.1
    return


label setup_mobs:
    $ l1_boss=Enemy(_("Foster Rex Forster")     , 1200, bar=3, rl=100, dl=1001)
    $ l2_boss=Enemy(_("Acharon, the Ice Dragon"), 6400, bar=3, rl=400, dl=1001)    # Acharon -> "Last", in hebrew.
    $ l2_b0ss=Enemy(_("Paul, the Landlord")     , 2400, bar=3, rl=140, dl=1002)
    $ l3_boss=Enemy(_("Silent Assassin")        , 3600, bar=3, rl=300, dl=1003)
    $ l4_boss=Enemy(_("Fire Gak")               ,12500, bar=4, rl=250)
    $ l5_boss=Enemy(_("Acharon")                ,18000, bar=5, rl=300)
#    $ l5_boss=Enemy(_("Abnormal")              ,18000, bar=5, rl=300)
    $ l6_boss=Enemy(_("Death Bringer")         ,24000, bar=5, rl=300)
    $ l7_boss=Enemy(_("Alf Elf Ilf")           ,29000, bar=5, rl=300)  # Alf - Title; Elf - Race; Ilf - Name
    $ l8_boss=Enemy(_("Hack3r.d0t")            ,33000, bar=5, rl=300)
    $ l9_boss=Enemy(_("Etheral")               ,35000, bar=5, rl=300)

    $ l10_boss=Enemy("Dark Lord"            ,25000, bar=5, rl=300)
    $ l11_boss=Enemy("Feared Reaper"        ,25000, bar=5, rl=300)
    $ l12_boss=Enemy("Fatal Schyte"         ,25000, bar=5, rl=300)
    $ l13_boss=Enemy("Luck Stealer"         ,25000, bar=5, rl=300)
    $ l14_boss=Enemy("Happiness ender"      ,25000, bar=5, rl=300)
    $ l15_boss=Enemy("Dream Hunter"         ,25000, bar=5, rl=300)
    $ l16_boss=Enemy("Bald Bunny"           ,25000, bar=5, rl=300)
    $ l17_boss=Enemy("Life Drainer"         ,25000, bar=5, rl=300)
    $ l18_boss=Enemy("Emperor Jaw"          ,25000, bar=5, rl=300)
    $ l19_boss=Enemy("Dark Rogue"           ,25000, bar=5, rl=300)
    $ l20_boss=Enemy("Black Crab"           ,25000, bar=5, rl=300)
    $ l21_boss=Enemy("Muted Death"          ,25000, bar=5, rl=300)
    $ l22_boss=Enemy("The Avenger"          ,25000, bar=5, rl=300)
    $ l23_boss=Enemy("Soul Eater"           ,25000, bar=5, rl=300)
    $ l24_boss=Enemy("The Spectral Killer"  ,25000, bar=5, rl=300)
    $ l25_boss=Enemy("Apocalypse"           ,25000, bar=5, rl=300)

    $ l1_subdungeon_boss=Enemy("Darkness Colector"      , 1200,    bar=3, rl=40)
    $ l1_subdungeon_mob1=Enemy("Mutant C-Rat"           ,  150, 1, bar=2, rl=5 , dl=0)
    $ l1_subdungeon_mob2=Enemy("Minor Shadow Hunter"    ,  250, 2, bar=2, rl=10, dl=2)
    $ l1_subdungeon_mob3=Enemy("Minor Darkness Hunter"  ,  450, 3, bar=2, rl=15, dl=2)


    $ l1_dungeon01  =Enemy(_("Bat")            , 150, 1, rl=  5, dl=0, bar=2)
    $ l1_dungeon01.pic="bloodbat"
    $ l1_dungeon02  =Enemy(_("Raging Dwarf")  , 250, 1, rl= 15, dl=0, bar=2)
    $ l1_dungeon02.pic="battlerager"
    $ l1_dungeon03  =Enemy(_("Minor Troll")    , 350, 2, rl= 20, dl=0, bar=2)
    $ l1_dungeon03.pic="troll"

    # Boss company
    $ l1_fiends01  =Enemy(_("Lightbringer, Limited") , 300, 1, rl=  5, dl=2001, bar=2)
    $ l1_fiends02  =Enemy(_("Lightvanisher, SA")     , 300, 1, rl=  5, dl=3001, bar=2)
    $ l2_fiends01  =Enemy(_("Tax Collector")         , 200, 2, rl= 15, dl=2002, bar=2)

    $ l1_mob_01=Enemy(_("Fire Ant")        ,  50, 1, rl=  2, dl=0)
    $ l1_mob_01.pic="ant"
    $ l1_mob_02=Enemy(_("Giant Rat")       , 100, 1, rl=  5, dl=0)
    $ l1_mob_02.pic="giant-rat"
    $ l1_mob_03=Enemy(_("Evil Pig")        , 200, 1, rl=  7, dl=0)
    $ l1_mob_03.pic="evil_pig"
    $ l1_mob_04=Enemy(_("Giant Spider")    , 250, 2, rl= 12, dl=0)
    $ l1_mob_04.pic="spiderbaby"
    $ l1_mob_05=Enemy(_("Dire Wolf")       , 300, 2, rl= 16, dl=1)
    $ l1_mob_05.pic="direwolf"
    $ l1_mob_06=Enemy(_("Ice Crab")        , 400, 2, rl= 20, dl=101)
    $ l1_mob_06.pic="icecrab"



    $ tutorial_mob=Enemy(_("Tutorial Dummy")  ,  5, 0, rl=  0)
    $ tutorial_mob.pic="quintain"

    $ load_factor+=1
    pause 0.1
    return

label setup_mobmob:
    $ l1_mob_01.weapon=RatWeapon
    $ l1_mob_02.weapon=JavaliWeapon
    $ l1_mob_03.weapon=SpiderWeapon
    $ l1_mob_04.weapon=WolfWeapon
    $ l1_mob_05.weapon=TeethWeapon
    $ l1_mob_06.weapon=MGoblinWeapon
    #$ l1_mob_07.weapon=GoblinWeapon
    #$ l1_mob_08.weapon=MOrcWeapon
    #$ l1_mob_09.weapon=OrcWeapon

    $ l1_boss.weapon  =L1BossWeapon
    $ l1_boss.offhand =L1Shield
    $ l1_boss.armor   =L1Clotch
    $ l1_boss.pic     ="knight-dismounted"
    $ l1_boss.luk     =1.17

    $ l2_boss.weapon  =L2BossWeapon
    $ l2_boss.offhand =L2Shield
    $ l2_boss.armor   =L2Clotch
    $ l2_boss.pic     ="ice-dragon"
    $ l2_boss.luk     =1.60

    $ l2_b0ss.weapon  =L2BossWeapon
    $ l2_b0ss.offhand =L2Sh1eld
    $ l2_b0ss.armor   =L2Clotch
    $ l2_b0ss.pic     ="royal-warrior"
    $ l2_b0ss.luk     =1.40

    $ l3_boss.weapon  =L3BossWeapon
    $ l3_boss.offhand =L3Shield
    $ l3_boss.armor   =L3Clotch
    $ l3_boss.pic     ="exterminator"
    $ l3_boss.luk     =1.30

    $ l1_fiends01.weapon  =L1FendWeapon
    $ l1_fiends01.offhand =L1Shield
    $ l1_fiends02.weapon  =L1FendWeapon
    $ l1_fiends02.offhand =L1Shield
    $ l1_fiends01.pic     ="silverwarrior"
    $ l1_fiends02.pic     ="assarcher"

    $ l2_fiends01.weapon  =L2FendWeapon
    $ l2_fiends01.offhand =L1Shield
    $ l2_fiends01.pic     ="caveman"

    $ l1_dungeon01.weapon=BatWeapon
    $ l1_dungeon01.armor =R01Clotch
    $ l1_dungeon02.weapon=AntWeapon
    $ l1_dungeon02.armor =R02Clotch
    $ l1_dungeon03.weapon=TrollWeapon
    $ l1_dungeon03.armor =R04Clotch

    $ l1_subdungeon_boss.weapon=L1SubBossWeapon
    $ l1_subdungeon_mob1.weapon=CRatWeapon
    $ l1_subdungeon_mob2.weapon=ShadowHWeapon
    $ l1_subdungeon_mob3.weapon=DarknessHWeapon

    $ tutorial_mob.weapon=TutWeapon

    $ load_factor+=1
    pause 0.1
    return


label setup_mobitem:
    $ RatWeapon        =Weapon("BUG",  5,  10, 0.70)
    $ JavaliWeapon     =Weapon("BUG", 10,  30, 0.64)
    $ SpiderWeapon     =Weapon("BUG", 15,  25, 0.69)
    $ WolfWeapon       =Weapon("BUG", 30,  45, 0.60)
    $ TeethWeapon      =Weapon("BUG", 35,  42, 0.62)
    $ MGoblinWeapon    =Weapon("BUG", 40,  50, 0.65)
    $ GoblinWeapon     =Weapon("BUG", 45,  60, 0.60)
    $ MOrcWeapon       =Weapon("BUG", 50,  64, 0.57)
    $ OrcWeapon        =Weapon("BUG", 52,  81, 0.54, 0.01, "heal IIIII")    # Probably never cast, but you will hate it. 20 HP regen.
    $ L1BossWeapon     =Weapon("BUG", 55,  81, 0.70) # Boss is centered in dealing damage, and Fiends on immobilizing your party. Uses Magic.
    $ L1FendWeapon     =Weapon("BUG", 15,  30, 0.90, 0.35, "paralyze I") # First boss loyal servants are plainly nasty.
    $ L2BossWeapon     =Weapon("BUG", 50, 100, 0.75) # Second boss is a skill-caster. It's attack is not THAT impressive. (Still strong)
    # TODO: L2BossWeapon (Code duplicate?)
    $ L2FendWeapon     =Weapon("BUG", 10,  20, 1.00, 0.05, "instakill") # Their hope is the instakill. Ouch.
    $ L3BossWeapon     =Weapon("BUG",120, 160, 0.75, 0.01, "instakill") # Third Boss fights all by himself.

    $ OrcBowWeapon      =Weapon("BUG", 55,  65, 0.70) 
    $ OrcCBowWeapon     =Weapon("BUG", 62,  70, 0.75)
    $ BOrcWeapon        =Weapon("BUG", 60,  80, 0.64)
    $ GOrcWeapon        =Weapon("BUG", 70,  70, 0.65)
    $ GoblinFgWeapon    =Weapon("BUG", 61,  77, 0.67)
    $ OrcMLWeapon       =Weapon("BUG", 75,  79, 0.60, 0.05, "paralyze I")
    $ OrcLWeapon        =Weapon("BUG", 82,  91, 0.55, 0.10, "paralyze I")
    $ HomeLessWeapon    =Weapon("BUG", 85, 106, 0.60, 0.15, "paralyze I")
    $ L2BossWeapon      =Weapon("BUG",105, 160, 0.82,  0.30, "paralyze I") # This is Castle default isn't it?

    $ BatWeapon        =Weapon("BUG",  15,  30, 0.60, 0.20, "heal I")           #Semiboss have a permanent 20% chance of skill.
    $ AntWeapon        =Weapon("BUG",  25,  30, 0.65, 0.20, "heal II")          #The two firsts will regenerate, as the normal.
    $ TrollWeapon      =Weapon("Porrete",  50,  80, 0.40, 0.20, "paralyze I")   #The last one have a stunning attack and will dizzy.

    $ CRatWeapon       =Weapon("BUG", 15,  30, 0.70, 0.60, "heal I")      # C-rats are annoying due almost always be regenarating.
    $ ShadowHWeapon    =Weapon("BUG", 25,  40, 0.70, 0.24, "heal III")    # Heal less often, but heals a lot. Take it quickly.
    $ DarknessHWeapon  =Weapon("BUG", 35,  50, 0.70, 0.05, "paralyze I")  # Because a turn less makes ALL the difference.
    $ L1SubBossWeapon  =Weapon("BUG", 50,  70, 0.50, 0.10, "paralyze II") # Not only the annoying effect.

    $ L1Shield         =Armor("BUG" , 20, 0.50) # First boss shield is very effective, but have low max values.
    $ L1Clotch         =Armor("BUG" , 60, 0.10) # First boss clotching is not very efficient, but have good max value.
    $ L2Shield         =Armor("BUG" , 40, 0.45) # Second boss shield is effective, and with reasonable max values.
    $ L2Clotch         =Armor("BUG" , 70, 0.15) # Second boss clotching is not THAT efficient, but have good max value.
    $ L2Sh1eld         =Armor("BUG" , 40, 0.25) # S3c0nd b055 shield is less effective than the dragon's
    $ L3Shield         =Armor("BUG" , 10, 0.05) # Third Boss cannot carry an effective shield but do what can do...
    $ L3Clotch         =Armor("BUG" , 50, 0.20) # Third boss clotching is all his defense (which is not much)

    $ R01Clotch        =Armor("BUG" , 10, 0.1) # Class R??Clotch haves random armors for mobs.
    $ R02Clotch        =Armor("BUG" , 20, 0.1) 
    $ R03Clotch        =Armor("BUG" , 10, 0.2) 
    $ R04Clotch        =Armor("BUG" , 20, 0.2) 

    $ TozzetiWeapon    =Weapon("Accounting Prince",100, 100, 1.00,  0.30, "paralyze poison silence drain I") # No skill casting. Relies on weapon
    $ MarcosWeapon     =Weapon("Salty Blade"      ,150, 200, 0.90,  0.25, "poison drain II") # Primary attacker, solely with a PA skill.
    $ ForsterWeapon    =Weapon("Payment Paper"    ,120, 160, 0.70,  0.10, "silence I") # He is the ultimate skill caster, offensive magic
    $ PauloWeapon      =Weapon("Coolness"         , 90, 110, 0.70,  0.15, "heal I") # He is a skill caster, but relies on defensive magic
    $ ThiagoWeapon     =Weapon("Lost Test"        , 90, 110, 0.60,  0.05, "paralyze I") # The weakest member, Summoning magic (Marx)

    $ TutWeapon        =Weapon("BUG",  1,  3, 1.00) # Tutorial use.

    $ load_factor+=1
    pause 0.1
    return

label setup_skill:
    # DEPRECATED: blablah-ON: Skills you know on format Skill()
    $ skilist=[]

    # DEPRECATED: Skills are not pre-initialized because... just because!
    # skilist should be [Skill(), Skill()] and learn function should be the append. Oh well.
    $ append_sk("m1",
                "Magic Arrow",
                "Indispensable for magic users, the basic Magic Arrow is a reliable way of inflicting a punch. \n\nDamage: 75-100\nAcc: 80%\nCost: 25 MP",
                'A', 25, float=0.80, array=[75, 100], halo="firemagic")

    $ append_sk("m2",
                "Fireball",
                "If you want to play volley with your enemies, use a FireBall! Diversion guaranteed! \n\nDamage: 100-200\nAcc: 75%\nCost: 50 MP",
                'A', 50, float=0.75, array=[100, 200], halo="flameburst")

    $ append_sk("m3",
                "Ultima CRUSH",
                "Exterminate. \n\nDamage: 150-400\nAcc: 70%\nCost: 80 MP",
                'A', 80, float=0.70, array=[150, 400], halo="flamedrown")

    $ append_sk("t1",
                "Heal",
                "In order to withstand the most severe blows, a tanker is fully trained to transform his mana in life in the most efficient fashion. \n\nHealing: 50-100 HP\nCost: 20 MP",
                'H', 20, array=[50, 100], halo="lightbeam")

    $ append_sk("t2",
                "Buff",
                "When facing a tough fight, all bonuses are important.\n\nDefense and Attack Up: 30%\nDuration: 3 hits\nCost: 40 MP",
                'B', 40, float=0.3, int=3, halo="teleport")

    $ append_sk("t3",
                "Debuff",
                "Against a strong foe? Ever wondered in reducing their defense and attack for an easier time?\n\nDefense and Attack Down: 30%\nDuration: 2 hits\nCost: 40 MP",
                'B', 40, float=-0.3, int=2, halo="teleport")

    $ append_sk("w1", 
                "Heavy Slash", 
                "Strikes a heavy blow, descending from heavens, against your foes. \n\nDamage: 60-80\nAcc: 80%\nCost: 20 MP",
                'A', 20, float=0.80, array=[60, 80], halo="aerowheel")

    $ append_sk("w2", 
                "Light Slash", 
                "Pathetically weak attack, but never misses. \n\nDamage: 30-45\nAcc: 100%\nCost: 20 MP",
                'A', 20, float=1.00, array=[30, 45], halo="aerowheel")

    $ append_sk("w3", 
                "Stab", 
                "Stab with all your might. You will fell good afterward. The enemy? Not so much. \n\nDamage: Weapon damage + 20~30\nAcc: 75%\nCost: 35 MP",
                'PA', 35, int=-1, float=0.75, array=[20, 30], halo="aerowheel") # old 120-140, correct 105-140, later to 110-120. Replaced to PA

    $ append_sk("r1", 
                "Paralizy Dart", 
                "Stun your foes with a dart. Not so powerful as it looks. \n\nDamage: 30-50\nAcc: 70%\n\nEffect Duration: 1 turn\nCost: 25 MP",
                'R', 25, float=0.7, array=[30, 50], str="paralyze I")

    $ append_sk("r2", 
                "Poison Dart", 
                "An attack which poisons the target. \n\nDamage: 50-100\nAcc: 70%\n\nEffect Duration: 1 turn\nCost: 25 MP",
                'R', 25, float=0.7, array=[30, 50], str="poison I")

    $ append_sk("r3", 
                "Draining Dart", 
                "An attack, which drains 20% from damage. \n\nDamage: 50-100\nAcc: 80%\n\nEffect Duration: 1 turn\nCost: 35 MP",
                'R', 35, float=0.8, array=[50, 100], str="drain II")

    $ append_sk("s1", 
                "Precise Shoot Sanctum", 
                "Sanctum: Attack which never misses. \n\nDamage: 120-140\nAcc: 100%\nCost: 100 MP",
                'A', 100, float=1.00, array=[120, 140])

    $ append_sk("s2", 
                "Explosion Sanctum", 
                "Sanctum: Attack all enemies, never misses. \n\nDamage: 40-60\nAcc: 100%\nCost: 100 MP",
                'E', 100, float=1.00, array=[40, 60], halo="flameburst")

    $ append_sk("s3", 
                "Protector Sanctum", 
                "Sanctum: Perfect protection for all allies. Major damage capacity. \n\nDefense and Attack Up: 100%\nDuration: 1 hits\nCost: 100 MP",
                'MB', 100, float=1.00, int=1)

    $ append_sk("s4", 
                "Be Quiet!", 
                "Silences the enemy, preventing skill-casting. \n\nDamage: 45-70\nAcc: 70%\n\nEffect Duration: 1 turn\nCost: 50 MP",
                'R', 50, float=0.7, array=[45, 70], str="silence I")

    $ append_sk("s5", 
                "Blue Mist", 
                "Attack someone and those close to the target. \n\nDamage: 50-70\nAcc: 80%\nCost: 25 MP",
                'AoE', 25, float=0.80, array=[50, 70], halo="firewheel")

    $ append_sk("en0", 
                "Power Attack", # Physical Attack?
                "Attack! \n\nDamage: +10\nIgnore defenses\nBase Acc: same as weapon\nCost: 100 MP",
                'PA', 100, int=10, halo="aerowheel")

    $ append_sk("en1", 
                "Ice Breath", 
                "A freezing breath. Brrr! \n\nDamage: 140-160\nParalyze: 1 turn\nAcc: 80%\nCost: 100 MP",
                'ICEBREATH', 100, float=0.80, int=1, array=[140,160], halo="icebubble")

    $ append_sk("en2", 
                "Droids Are My Friends", 
                "Summon worker droids to aid you.\n\nAmount: 1\nStats: 60 RGS\nSuccess Chance: 100%\nCost: 150 MP",
                'SUMMON', 150, str="worker", int=60, float=1.0, halo="recruit")

    $ append_sk("en3", 
                "Power Slash",
                "A dangerous power attack! \n\nDamage: +30\nIgnore defenses\nBase Acc: same as weapon\nCost: 40 MP",
                'PA', 40, int=30, halo="aerowheel")

    $ append_sk("en4", 
                "Poisoned Dagger",
                "Poison by itself does 5% damage each turn. It can kill.\n\nDamage: 30-50\nAcc: 70%\n\nEffect Duration: 2 turns\nCost: 50 MP",
                'R', 50, float=0.7, array=[30, 50], str="poison I", halo="aerowheel")

    $ append_sk("en5", 
                "Stun Potion",
                "Paralyzes the enemy with fashion.\n\nAcc: 90%\n\nEffect Duration: 1 turn\nCost: 50 MP",
                'R', 50, float=0.7, array=[0, 0], str="paralyze I", halo="bluebeam")

    $ append_sk("en6", 
                "Heal Potion",
                "Perfect when you or a friendly need to heal in the battle!\n\nHealing: 50-150 HP\nCost: 30 MP",
                'H', 30, array=[50, 150], halo="lightbeam")

    $ append_sk("en7", 
                "Throw Bomb", 
                "Enemy Sanctum: Attack all enemies with high probability. \n\nDamage: 60-90\nAcc: 90%\nCost: 100 MP",
                'E', 100, float=0.90, array=[60, 90], halo="flamedrown")

    $ append_sk("en8", 
                "Marx Summon", 
                "Invokes a Marx to brainwash the team. \n\nAmount: 1\nStats: 30 RGS\nSuccess Chance: 90%\nCost: 100 MP",
                'SUMMON', 100, str="teti", float=0.90, int=30, halo="recruit")

    $ append_sk("en9", 
                "Weber Summon", 
                "Invokes a Max Weber to brainwash the team. \n\nAmount: 1\nStats: 50 RGS\nSuccess Chance: 80%\nCost: 130 MP",
                'SUMMON', 130, str="stalwart", float=0.80, int=50, halo="recruit")

    $ append_sk("en10", 
                "Warrior Summon", 
                "Invokes a warrior to exterminate the team. \n\nAmount: 1\nStats: 80 RGS\nSuccess Chance: 70%\nCost: 200 MP",
                'SUMMON', 200, str="silverwarrior", float=0.70, int=80, halo="recruit")

    $ append_sk("en11",
                "Greater Debuff",
                "Too strong enemies? BLAST THEM AWAY.\n\nDefense and Attack Down: 50%\nDuration: 3 hits\nCost: 80 MP",
                'B', 80, float=-0.5, int=3, halo="teleport")

    $ append_sk("en12",
                "Area Explosion", 
                "Attack someone and those close to the target in a fiery explosion. \n\nDamage: 70-100\nAcc: 90%\nCost: 75 MP",
                'AoE', 75, float=0.90, array=[70, 100], halo="firewheel")

    $ append_sk("en13",
                "Droids Are My Allies", 
                "Summon worker golems to aid you.\n\nAmount: 1\nStats: 120 RGS\nSuccess Chance: 55%\nCost: 300 MP",
                'SUMMON', 300, str="golem", int=120, float=0.55, halo="recruit")

    $ append_sk("en14",
                "Summon Water Sprite", 
                "Summon a water sprite to aid you.\n\nAmount: 1\nStats: 45 RGS\nSuccess Chance: 85%\nCost: 110 MP",
                'SUMMON', 110, str="tidal", int=45, float=0.85, halo="recruit")

    $ append_sk("en15",
                "Summon Water Avatar", 
                "Summon a water Avatar to aid you.\n\nAmount: 1\nStats: 85 RGS\nSuccess Chance: 80%\nCost: 210 MP",
                'SUMMON', 210, str="undine", int=85, float=0.80, halo="recruit")

    $ append_sk("en16",
                "Summon Fire Sprite", 
                "Summon a fire sprite to aid you.\n\nAmount: 1\nStats: 50 RGS\nSuccess Chance: 80%\nCost: 110 MP",
                'SUMMON', 110, str="firewisp", int=50, float=0.80, halo="recruit")

    $ append_sk("en17",
                "Summon Fire Avatar", 
                "Summon a fire Avatar to aid you.\n\nAmount: 1\nStats: 90 RGS\nSuccess Chance: 75%\nCost: 210 MP",
                'SUMMON', 210, str="fireghost", int=90, float=0.75, halo="recruit")

    $ append_sk("en18",
                "Summon Water Dragon", 
                "Summon a water Dragon to aid you. Powerful!\n\nAmount: 1\nStats: 100 RGS\nSuccess Chance: 100%\nCost: 500 MP",
                'SUMMON', 500, str="water-avatar", int=100, float=1.00, halo="recruit")

    $ append_sk("tray", 
                "Sanctum!", 
                "Some powerful skill only the true hero may use. Damage is equivalent your current weapon max damage with a bonus of 20.\n\nAcc: 90%\nCost: 10 MP",
                'PA', 10, int=20, float=0.9, halo="implosion")

    $ load_factor+=1
    pause 0.1
    return

