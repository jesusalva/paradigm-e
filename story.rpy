########################################################################################
#     This file is part of Project E.
#     Copyright (C) 2015  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Story control
image Flag = Frame("gfx/bg/Flag.jpg",0,0)
image DKF = Frame("gfx/bg/dark_fortress.png",0,0)
image Throne Entrance = Frame("gfx/bg/KingRoom.jpg",0,0)
image Throne Room = Frame("gfx/bg/KingRoom.jpg",0,0) # I don't want the door and the guards
image Greenhills = Frame("gfx/bg/Greenhills.jpg",0,0)
image Bridge2 = Frame("gfx/bg/bridge2.jpg",0,0)
image Night Past = Frame("gfx/bg/pastnight.jpg",0,0)
image Shipwreck = Frame("gfx/bg/SeaStorm.png",0,0)
image Farmland Night = Frame("gfx/bg/farmland_night.jpg",0,0)

image Fountain = Frame("gfx/bg/fountain.jpg",0,0) # PD by Jon Sullivan
image Fountain Afternoon = Frame("gfx/bg/fountain_afternoon.jpg",0,0) # see above
image Fountain Night = Frame("gfx/bg/fountain_night.jpg",0,0) # see above
image forge = Frame("gfx/bg/forge.jpg",0,0)

# Scene Controls
label scene_ctrl_castle:
    if 'CASTLE' in gstory_ctrl:
        if 'PROLOGUE' in gstory_ctrl:
            jump scene2
        e "I don't think it would be wise to do so. This is a bug, by the way.\n\nI will pray with the following words:"
        # jk
        $ bug('flag CASTLE was used incorrectly.')
        $ gstory_ctrl.remove("CASTLE")
        jump dev_beta
    else:
        $ ui.frame(xalign=.95, yalign=.05, xsize=.2, ysize=.2)
        $ ui.text(_("Game Completion: %.2f %%" % (float(float(renpy.count_seen_dialogue_blocks()) / renpy.count_dialogue_blocks())*100)), font="f/FreeMono.ttf")
        menu:
            "The Imperial Castle of Linarius - Main Hall"
            "Adjust difficulty":
                call set_difficulty
            "Insert bonus code":
                call claim_code
            "Leave.":
                jump dev_beta
        jump scene_ctrl_castle














label scene_ctrl_forge:

    $ show_hpbar=False
    scene forge with dissolve

    jump scene_forge2


label scene_ctrl_tavern:
    if 'T_FIRST' in gstory_ctrl:
        jump tv_viniverme_in
    # BS_ flags are handled in tavern.rpy
    jump scene_tavern














































# Scenes
label scene1:
    scene black with dissolve
    "..."
    "... ..."
    $ prologue=True
    $ gstory_ctrl.append('CASTLE')
    $ gstory_ctrl.append('PROLOGUE')
    $ ql.update_quest("01 Departing from Linarius", "Proceed to Imperial Castle", "Everything is calm. I cannot hear birds, nor the breeze. Something might be about to happen.\nI should travel to the Imperial Castle to see how things are going there.\n\nThis can be done by Exploring City Streets.")
    return



label scene2:
    "Guard" "Halt!"
    $ jhonnyname="Prince Of Linarius"
    $ italusname="Guardian of Linarius"
    # Italus & Mamedis - Beer, Money, Woman
    # Italus - I don't care, some people will have to die!
    show JHONNY full at left
    show ITALUS full at right
    with dissolve
    ejhonny "Hello, PCT! We were waiting you!"
    eitalus "Indeed, I already stocked the beer!"
    show PCT full at center behind JHONNY,ITALUS with dissolve
    "You'll be playing as [yname] from now on. During rare cases (like this one), the story focus may change to some other member, \"for an improved experience\".\nPlease note that PCT is the one who writes quest logs."
    "[yname], The Hero" "Are you ready? Then we should set off. Gustavus Mamedis should be expecting us at Lumnia City."
    epct " Also, knowing The Great Shadow Mage, it's probably urgent and we should hurry, I think."
    "[yname], The Hero" "It's not THAT far, we can complete the travel in a day, but returning here will be tiringsome! Can't you do something about that, Jhonny Jesusaves?"
    $ jhonnyname="Jhonny, Prince Of Linarius"
    ejhonny "Do not worry, [yname] The Hero. Cleidir and I have been testing a new teletransport system. With it, we can jump from a beacon to another. I've set a beacon here on the Castle, already."
    "[yname], The Hero" "Beacons? What are you talking about, after all?"
    epct "Basically, this is the first place we travelled, so, this is Beacon 1 - Linarius. Every time we travel and reach a new place, we'll set a new Beacon. So we should have soon enough a Beacon 2 - Lumnia."
    $ italusname="Italus, Guardian of Linarius"
    eitalus "Do not worry about the details. Worry about the beer! Actually, how can you stand idle when we're going to Lumnia? {i}They have beaches{/i}! You know what I mean, right?!"
    epct "Sorry, I'm not Vinicius, the Redhead. I don't have interest in such things, Italus."
    ejhonny "[yname], once again, we'll count on your guidance."
    e "Alright. In this case, to the beach! Err, to Lumnia City, I mean. For a, uhm, important meeting with Gustavus and the Great Shadow Mage. Yeah. That."
    hide PCT
    hide ITALUS
    hide JHONNY
    with dissolve

    call add_member(pctpct)
    call add_member(italus)

    show JHONNY full with dissolve
    ejhonny "Also, during this travel we'll be under your Zone Of Influence. This means that not only you can choose what skills of mine shall be deemed necessary, but we're all depending on your growth to unlock new skills and etc."
    ejhonny "Not to forget, you can once again have up to 9 friends on the frontline, and the first set is the first to act. This time though, there are no restrictions, and you can even manage the party during the fight!"
    ejhonny "Just be warned that if you die, then it's all over. I'll pick up the fight, but the only reason we are able to survive fatal blows is because your sanctum influence."
    show ITALUS full at right behind JHONNY with dissolve
    eitalus "Basically, you die, and everything is over. The others will never die, and even if they get knocked out, you just need a normal healing to get them up again. Now let's go! For money, beer and woman! And justice!"
    hide ITALUS with dissolve
    pause 0.2
    hide JHONNY with dissolve

    call add_member(jhonny)

    $ prologue=False
    $ gstory_ctrl.remove('CASTLE')
    $ gstory_ctrl.remove('PCT_PROLOGUE')
    $ ql.update_quest("01 Departing from Linarius", "Travel to Lumnia City", "Now that we have a team, we should travel to Lumnia City, in order to meet the Great Shadow Mage or something. This can be accomplished by selecting \"Travel\" on the city menu.\n\nJhonny warned me that it is game over if the hero dies, so I should my friends to fight with me on the party menu.")
    jump dev_beta

label scene10:
    $ show(jhonny, at=[left])
    ejhonny "We should take a quick break."
    $hide(jhonny)
    e "I agree, we've been fighting non-stop."
    if yasmin in party:
        $ show(yasmin, at=[center])
        eyasmin "There seems to be clearing here, it should be safe to rest."
        $ hide(yasmin)
    else:
        ejhonny "There seems to be clearing here, it should be safe to rest."

    $ show(italus, at=[center])
    eitalus "[yname], please, cast a protection Sanctum, and let's sleep for about... three minutes."
    $ hide(italus)
    call rest_menu
    e "We're rested now. Let's continue!"

    return










################### BAR STORY
label bs_vermed_1:
    $ show(vermed)
    "Guard" "I'm sorry, Mr. [vermed.name], but you're not allowed to drink."
    evermes "But how come? I absolutely need to drink!"
    "Guard" "No you don't. Also, these were the King's order. I'm sorry. You cannot enter this bar."
    evermes "Hah, just you wait! I have suppliers in the black market!"
    "Guard" "As long that you don't bother me."
    evermes "Hmpf."
    $ hide(vermed)
    return

label bs_italus_1:
    $ show(italus)
    eitalus "Dargh, I shouldn't have drank that much!"
    e "...I really hope you can hold for more than that, though..."
    $ italus.dislike.append('beer')
    $ hide(italus)
    return

# If yasmin in party
label bs_yasmin_1:
    $ show(yasmin, at=[left])
    $ show(jhonny, at=[right])
    if yasmin in party:
        evermes "You should drink with us, Yasmin!"
        eyasmin "I'll pass."
        $ hide(yasmin)
    else:
        evermes "Dang, I never manage to convince Yasmin to drink with us..."
    evermes "You too, Jhonny! C'mon! Let's drink!"
    ejhonny "E-Eh? No, thanks!"
    $ show(jhonny, move, at=[offscreenleft])
    $ hide(jhonny, None)
    evermes "Pfft. Worms." # Internal joke (TODO)?
    return

################### TAVERN STORY
label tv_viniverme_in:
    $ gstory_ctrl.remove('T_FIRST')
    $ vermesname="Vinicius, the Redhead"

    # If you are not eligible
    if vermed in party:
        jump scene_ctrl_tavern

    scene Restaurant with Dissolve(1.0) # err, bar you meant

    $ show_money=True
    $ tmp_tverme=get_average_level()
    menu:
            "GREAT DEAL!" "ONLY THIS ONCE TIME, acquire Vinicius, the RedHead with 88%% off!\nHe'll also come at level [tmp_tverme], instead of usual level 1!\n\n{size=16}This is an one-time-only offer.{/size}"
            "{b}GREAT DEAL{/b}\nVinicius the Redhead (Warrior) for ONLY 850 credits":
                if gp < 850:
                    e "We're too poor to do so."
                else:
                    $gp-=850
                    call add_member(vermed)
                    $ resync_level(vermed)
                    $show(vermed)
                    evermes "I'm feeling lazy today, but hey, IS THAT GOLD? Then, let's do our best."
                    e "..."
                    ejhonny "Frankly. He didn't changed a thing."
                    e "...It's surprising that he didn't mentioned any ladies..."
                    $hide(vermed)

            "I'm not interested":
                jump scene_ctrl_tavern

    jump scene_ctrl_tavern

## TV_ labels control tavern recruitment, and BS_ labels are bar stories (random)
## -----------------------------------------------------------------------------------

label tv_yasmin_p_leaf:
    $ yasminname="Yasmin, the Sniper"
    $show(yasmin)
    eyasmin "Try anything funny, and I'll struck an arrow on your head."
    ejhonny "Since when were you so hostile?"
    e "Who knows. It was a long time since we last seen each other."
    $hide(yasmin)
    return

label tv_viniverme:
    $ vermesname="Vinicius, the Redhead"
    $show(vermed)
    evermes "I'm feeling lazy today, but hey, IS THAT GOLD? Then, let's do our best."
    e "..."
    ejhonny "Frankly. He didn't changed a thing."
    e "...It's surprising that he didn't mentioned any ladies..."
    $hide(vermed)
    return

label tv_gill_vans:
    $ gilvanname="Gill Vans"
    $show(gilvan)
    egilvan "..."
    e "Eerly quiet."
    egilvan "Hello, how are you?"
    e "Too simpatic. Ah, nevermind. Welcome aboard, Gill Vans."
    $hide(gilvan)
    return

label tv_ingrid:
    $ ingridname="Ingrid"
    $show(ingrid)
    eingrid "Hello folks!"
    ejhonny "Hello, [ingrid.name]. Welcome to the team."
    $hide(ingrid)
    return

label tv_anna_gold:
    $ anacmaname="Anna Gold"
    $show(anacma)
    eanacma "Hello there. Let's do our best."
    evermes "Have I already said your hair reminds me of GOLD?"
    $hide(anacma)
    return

label tv_:
    return









################### COMBAT STORY
label CS_B2_DJ:
    $ renpy.pop_call() # This is for jumping, not for calling
    e "Oh noes! We can't do it without Jesusalva! Acharon will kill us all! We're doomed!"
    $ show_bossb=False
    jump defeat

label CS_B2_S2:
    show JHONNY full at left with dissolve
    show ITALUS full at right with dissolve
    eitalus "We can't win! It's an Ice Dragon!"
    ejhonny "Indeed, it's pretty strong. I don't think we can save the ship."
    eitalus "Do you have any ideas, Jhonny Jesusaves?"
    ejhonny "For now, focus on survival. I'm thinking in something."
    "IMPORTANT" "If Jhonny Jesusaves dies on this fight, it'll be GAME OVER!"
    hide ITALUS with dissolve
    hide JHONNY with dissolve
    $ gstory_ctrl.append("COR_3_@_CS_B2_S3")
    return

label CS_B2_S3:
    show JHONNY full at left with dissolve
    ejhonny "I have an idea. Can you guys cover me for a turn?"
    $ jhonny.frozen+=1
    "Jhonny Jesusaves won't fight this turn. It's advised to use your tankers to heal and buff him."
    hide JHONNY with dissolve
    $ gstory_ctrl.append("COR_4_@_CS_B2_S4")
    return


label CS_B2_S4:
    show PCT full at right with dissolve
    show JHONNY full at left with dissolve
    ejhonny "Cleidir, do you remember the basic theory about Specialized Sanctums?"
    epct    "Yes. Sanctums can be bent, but only to a certain extent. Why do you ask so?"
    ejhonny "I just determined, the Hero is capable to bend his Sanctum to provide a 100%% defense bonus for a while."
    ejhonny "It's the Protector Sanctum. All preparatives are now made. If this ship sinks, the dragon won't give pursuit."
    $ learn_sk("s3", "Protector Sanctum", hero)
    $ g_maxbt=6
    centered "{font=f/Imperator.ttf}{b}Maximum battle size increased to 6!{/b}{/font}"
    epct    "So, all we need to do is to lose this fight?"
    ejhonny "Exactly. The ship will sink and Acharon, the Ice Dragon will assume we're all dead. At the same time, [yname] shall use the Protector Sanctum. I'll use mine too, so I can't say what will happen next, except for one thing: We'll survive."
    "You can now lose this fight. You can {i}auto{/i} the battle for a faster process." # Or remove everyone from party!
    hide PCT with dissolve
    hide JHONNY with dissolve
    $ gstory_ctrl.append("UponDefeat_CS_B2_END")
    $ gstory_ctrl.remove("COD_"+jhonny.name+"_@_CS_B2_DJ")
    return

# Used on a fake-death fight
label CS_B2_END:
    $ gstory_ctrl.remove("COMBAT_OVERRIDE")
    $ autofight=False
    $ atk_off() # Doublesure
    $ EnemyParty=[] # Clear enemies list
    # Default behavior restore.
    if not canflee:
        $ canflee=True

    $ fighting=False
    $ show_enbar=False
    $ show_bossb=False

    # Hide background
    $ renpy.hide(combat_bg)
    $ renpy.with_statement(dissolve)

    # Resume standard dungeon
    #$ renpy.jump("dev_beta__dungeon_l" + str(g_cur_level))

    # Jump to next Scene
    jump dev_beta

# Internal note about CS_ label
# CS Combat Story
# B is Boss Battle ID (or something like that)
# D is Death
# S is for turn
# M is for madness


label CS_B3_M:
    $ l2_boss.aispec="WEAKEST"
    $l2_boss.add_dmg+=1.00
    $l2_boss.add_dmg_dur+=3
    $ renpy.block_rollback()
    "{font=f/Imperator.ttf}{color=#F00}WARNING{/color}{/font}" "Now this boss is mad! It'll be dangerous!!" # Maybe Fishmonger font? I wanted SFs
    return

label CS_B03_S1:
    $ show(jhonny, at=[left])
    ejhonny "{font=f/Imperator.ttf}TAKE CARE! This assassin is not only a master of poison, but also of stealth!\nIf you paralyze him, he'll become untargettable!{/font}"
    $ hide(jhonny)
    return

label CS_B03_F:
    $ l3_boss.hidden+=l3_boss.frozen+1
    "[l3_boss.name] conceals its presence, and cannot be target anymore!"
    return

label CS_B03_M:
    $l3_boss.add_dmg+=0.40
    $l3_boss.add_dmg_dur+=4
    $l3_boss.add_def_rt+=0.20
    $l3_boss.add_def_dur+=4
    $l3_boss.mp=0+l3_boss.maxmp
    $l3_boss.luk+=0.05
    $l3_boss.skill.append(retrieve_skill_from_db("Throw Bomb"))
    $l3_boss.hp+=int(l3_boss.maxhp/6) # recovers 50% from second HP bar: More though than looks!
    # Cool down any skill like poison or silence
    $cooldown(l3_boss)
    $ renpy.block_rollback()
    "{font=f/Imperator.ttf}{color=#F00}WARNING{/color}{/font}" "[l3_boss.name] is now mad! It'll be dangerous!!"
    return

